(ns ^:figwheel-always ssoup-om-client.core
  (:require
    [cljs.core.async :as async :refer [<! chan close! put!]]
    [cljs.core.async.impl.protocols :refer [Channel]]
    [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]
    [goog.dom :as gdom]
    [om.next :as om :refer-macros [defui]]
    [om.dom :as dom]
    [ssoup-clj-core.core :as s]
    [ssoup-clj-core.utils :as utils]
    [ssoup-clj-core.uuid :as uuid]
    [ssoup-clj-client.core :as sc]
    [ssoup-clj-client.ajax :as ajax]
    [ssoup-clj-client.modules :as sm])
  (:require-macros [cljs.core.async.macros :refer [go]]
                   [ssoup-clj-core.core :refer [ssoup-let ssoup-exec <?]]))

(enable-console-print!)

; -------------------------------------------------------
; OM UI state
; -------------------------------------------------------

(def app-state (atom {}))

(defmulti read (fn [env key params] key))

(def reconciler
  (om/reconciler
    {:state app-state
     :parser (om/parser {:read read})}))

; register remote ajax ssoup engine
(s/register-ssoup-engine!
  {:exec ajax/exec})

(defui Container
  static om/IQuery
  (query [this]
    )

  Object
  (render [this]
    ()))

(defn container [container-id]
  (let [container-ui (get @container-ratoms container-id)]
    (if container-ratom
      [wrap-ratom container-ratom]
      (let [container-ratom (reagent/atom nil)]
        (register-container-ratom container-id container-ratom)
        [wrap-ratom container-ratom]))))

(defn render-root-container []
  (om/add-root! reconciler
                (container :ssoup/root-container) (gdom/getElement "app")))

(defrecord OMEngine
  []
  s/IEngine
  (init [this]
    (s/set-author-scs-rules! (sm/compute-scs-rules))
    (render-root-container))
  sc/IUIEngine
  (template-registered [this ctx template-id template-body]
    (reagent/render-to-string (template-body ctx {})))      ; called so that any container defined in the template definition is instantiated immediatly, without waiting for the next rerender cycle
  (set-container-content! [this container-id content]
    (let [ratom (get @container-ratoms container-id)]
      (if ratom
        (reset! ratom content)
        (warn "Container ratom" container-id "not found")))))

(s/set-engine! (->OMEngine))

; -------------------------------------------------------
; Show facade
; -------------------------------------------------------
(defn show
  ([ctx obj]
   (show ctx nil obj))
  ([ctx additional-classes obj]
   (let [container-id (keyword (str (uuid/make-random)))]
     (go (let [classes #{:ssoup/show}
               classes (if additional-classes
                         (clojure.set/union classes additional-classes)
                         classes)]
           (try (s/throw-err (<! (s/exec-async {:classes classes
                                                :ctx     {}
                                                :args    {:object obj}
                                                :options {:container container-id}})))
                (catch js/Error e
                  (sc/set-container-content! (s/get-engine) container-id
                                             [:p.error {:title (str e)} "An error occurred."])))))
     (fn []
       (container container-id)))))
