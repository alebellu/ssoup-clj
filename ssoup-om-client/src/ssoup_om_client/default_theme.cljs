(ns ^:figwheel-always ssoup-om-client.default-theme
  (:require
    [cljs.core.async :as async :refer [<! chan close! put!]]
    [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]
    [goog.dom :as gdom]
    [om.next :as om :refer-macros [defui]]
    [om.dom :as dom]
    [ssoup-clj-client.core :as sc]
    [ssoup-clj-client.session :as session]
    [ssoup-clj-client.auth :as auth]
    [ssoup-clj-client.i18n :refer [msg]]
    [ssoup-clj-core.core :as s]
    [ssoup-om-client.core :refer [show container]])
  (:require-macros [cljs.core.async.macros :refer [go]]))

; from https://github.com/yogthos/yuggoth

(defn- https? []
  (= "https:" (.-protocol js/location)))

(defn- input-value [input]
  (-> input .-target .-value))

(defn- set-value! [target]
  (fn [source] (reset! target (input-value source))))

(defn- text-input [target & [opts]]
  [:input (merge
            {:type      "text"
             :on-change (set-value! target)
             :value     @target}
            opts)])

(defn- string-viewer [_ str options]
  [:span str])

(defn- keyword-viewer [_ k options]
  [:span ":" (namespace k) "/" (name k)])

(defn- number-viewer [_ num options]
  [:span num])

(defn- map-viewer [ctx obj options]
  [:ul (for [[k v] obj]
         ^{:key k} [:li [:span [:b (name k)] ": "] [:span [show ctx v]]])])

(defn- compact-map-viewer [ctx obj options]
  [:table
   [:tr (for [[k v] obj]
          ^{:key k} [:td [:span [:b (name k)] ": "] [:span [show ctx v]]])]])

(defn- array-viewer [ctx array options]
  [:ul (for [item array]
         ^{:key item} [:li [show ctx item]])])

(defn dropdown [ctx obj-type options]
  (let [items (atom nil)]
    (go (let [vals (<! (s/list-objects ctx obj-type))]
          (reset! items vals)))
    [:select
     (when @items
       (for [item @items]
         [:option {:value item} item]))]))

; the array-type must somehow contain the item type
(defn dropdown-multi [ctx array-type options])

(defn init-theme [ctx options]
  (debug "Registering theme ssoup default theme")

  (sc/register-theme! (s/get-engine) ctx :ssoup/default-theme
                      {:login-form (fn [success]
                                     (let [user (atom nil)
                                           pass (atom nil)
                                           error (atom nil)]
                                       [:fieldset.login-form
                                        [:legend (msg "Sign in")]
                                        [:span (msg :user)]
                                        [text-input user]
                                        [:span (msg :password)]
                                        [text-input pass {:type "password"}]
                                        [:button.login-button {:on-click #(session/remove! :login)} (msg :reset)]
                                        [:button.login-button {:on-click #(auth/login! @user @pass success error)} (msg :login)]
                                        (if-let [error @error]
                                          [:div.error error])]))
                       :viewers
                                   [[:ssoup-default-theme/string-viewer nil :xsd/string
                                     string-viewer]
                                    [:ssoup-default-theme/keyword-viewer nil :ssoup/keyword keyword-viewer]
                                    [:ssoup-default-theme/number-viewer nil :ssoup/number number-viewer]
                                    [:ssoup-default-theme/map-viewer nil :ssoup/map map-viewer]
                                    [:ssoup-default-theme/compact-map-viewer #{:ssoup/compact} :ssoup/map compact-map-viewer]
                                    [:ssoup-default-theme/array-viewer nil :ssoup/array array-viewer]]
                       :editors    []
                       :selectors
                                   [[:ssoup-default-theme/object-selector nil :ssoup/any-object dropdown]
                                    [:ssoup-default-theme/array-selector nil :ssoup/array dropdown-multi]]}))

(derive :ssoup/default-theme :ssoup/default-flavor)

(s/register! {:fname     :default-theme/init-theme
              :handler   (fn [ctx args options]
                           (init-theme ctx options))
              :flavor    :ssoup/default-theme
              :user-role :ssoup/any-user
              :classes   #{:ssoup/init-theme}
              :arg-types {}})
