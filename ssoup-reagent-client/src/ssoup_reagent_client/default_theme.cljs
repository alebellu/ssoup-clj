(ns ^:figwheel-always ssoup-reagent-client.default-theme
  (:require
    [cljs.core.async :as async :refer [<! chan close! put!]]
    [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]
    [reagent.core :as reagent :refer [atom]]
    [re-frame.core :refer [reg-sub reg-event-db subscribe dispatch path trim-v after]]
    [ssoup-clj-core.types :refer [derive*]]
    [ssoup-clj-core.utils :refer [retval str-pprint]]
    [ssoup-clj-client.core :as sc :refer [get-container show ask edit]]
    [ssoup-clj-client.auth :as auth]
    [ssoup-clj-client.i18n :refer [msg]]
    [ssoup-clj-core.core :as s]
    [ssoup-reagent-client.core]
    [ssoup-clj-core.uuid :as uuid])
  (:require-macros [cljs.core.async.macros :refer [go]]))

;------------------------------------
; VIEWERS
;------------------------------------

(defn- string-viewer [_ str options]
  [:span str])

(defn- keyword-viewer [_ k options]
  [:span ":" (namespace k) "/" (name k)])

(defn- number-viewer [_ num options]
  [:span num])

(defn- boolean-viewer [_ b options]
  [:span b])

(defn- resource-viewer [ctx obj options]
  [:ul (for [[k v] obj]
         ^{:key k} [:li [:span [:b (name k)] ": "] [:span [show ctx v]]])])

(defn- compact-resource-viewer [ctx obj options]
  [:table
   [:tr (for [[k v] obj]
          ^{:key k} [:td [:span [:b (name k)] ": "] [:span [show ctx v]]])]])

(defn- resource-viewer-schema-based [ctx obj options]
  (let [schema (:ssoup/schema options)]
    [:table
     [:tbody
      (let [types (keys schema)
            props (map (fn [type] (-> schema (get type) :ssoup/schema :ssoup/properties)) types)
            props (distinct (apply concat props))]
        (for [prop props]
          (let [p (:id prop)
                l (:rdfs/label prop)
                r (:rdfs/range prop)
                v (get obj p)]
            ^{:key p}
            [:tr
             [:td [:span [:b l] ": "]]
             [:td [show ctx nil v {:ssoup/item-type r}]]])))]]))

(defn- array-viewer [ctx array options]
  [:ol (for [item array]
         ^{:key item} [:li [show ctx item]])])

(defn- set-viewer [ctx s options]
  (if-let [item-schema (:ssoup/item-schema options)]
    ; if the item type and schema is known then show the set as a table
    [:table
     [:thead
      [:tr
       [:th [:b "Name"]]
       (map-indexed (fn [i prop]
                      (let [p (:id prop)
                            l (:rdfs/label prop)]
                        ^{:key i} [:th [:b l]]))
                    (:ssoup/properties item-schema))]]
     [:tbody
      (map-indexed (fn [i obj]
                     ^{:key i}
                     [:tr
                      (let [name (get obj :rdfs/label)
                            description (get obj :rdfs/comment)]
                        ^{:key :rdfs/label}
                        [:td {:title description} name])
                      (for [prop (:ssoup/properties item-schema)]
                        (let [p (:id prop)
                              v (get obj p)]
                          ^{:key p}
                          [:td [show ctx v]]))
                      (when-let [actions (:ssoup/actions options)]
                        (for [{action-id      :id
                               action-name    :name
                               action-handler :handler} actions]
                          ^{:key (str "action-" action-id)}
                          [:td [:a {:href     "#"
                                    :on-click #(action-handler obj
                                                               {:container-obj obj})}
                                action-name]]))])
                   s)]]
    ; otherwise show the set as a bulleted list
    [:ul (for [item s]
           ^{:key item} [:li [show ctx item]])]))

(defn- link-viewer [_ link options]
  (let [href (:ssoup/href link)
        desc (:rdfs/label link)
        desc (or desc href)]
    [:a {:href href} desc]))

(defn- folder-viewer [_ folder options]
  [:div
   (when (:ssoup/parent folder)
     [:a {:href (str "dir?p=\"" (:ssoup/parent folder) "\"")} "[Up]"])
   [:span "   "] [:b (:rdfs/label folder)]
   [:ul
    (for [f (:ssoup/files folder)]
      ^{:key f} [:li
                 (if (= (:rdf/type f) :ssoup/folder)
                   [:a {:href (str "dir?p=\"" (:rdfs/label folder) "/" (:rdfs/label f) "\"")}
                    "[" (:rdfs/label f) "]"]
                   [:a {:target "_blank" :href (str "public"
                                                    (:rdfs/label folder) "/"
                                                    (:rdfs/label f))}
                    (:rdfs/label f)])])]])

;------------------------------------
; EDITORS
;------------------------------------

; from https://github.com/yogthos/yuggoth

(defn- input-value [input]
  (-> input .-target .-value))

(defn- set-value! [target-atom target-ratom]
  (fn [source]
    (let [v (input-value source)]
      (reset! target-atom v)
      (reset! target-ratom v))))

(defn- text-input [target-atom & [opts]]
  (let [str-value (atom @target-atom)]
    (fn []
      [:input (merge
                {:type      "text"
                 :on-change (set-value! target-atom str-value)
                 :value     @str-value}
                opts)])))

(defn- checkbox-input [target-atom & [opts]]
  (let [boolean-value (atom @target-atom)]
    (fn []
      [:input (merge
                {:type      "checkbox"
                 :on-change (set-value! target-atom boolean-value)}
                (if @boolean-value {:checked :checked} {})
                opts)])))

(defn- string-editor [ctx str options]
  [:div
   [:input {:type      "text"
            :on-change #(let [on-change (:on-change options)]
                          (when on-change
                            (on-change (-> % .-target .-value))))}]])

(defn- email-editor [ctx email options]
  [:span "@" [string-editor ctx email options]])

(defn- keyword-editor [_ k options]
  (let [ns-value (atom (namespace @k))
        k-value (atom (name @k))]
    [:span ":" (text-input ns-value) "/" (text-input k-value)]))

(defn- number-editor [_ num options]
  [:span (text-input num)])

(defn- boolean-editor [_ b options]
  [:span [checkbox-input b]])

(defn- resource-editor [ctx obj options]
  [:ul (for [[k v] @obj]
         ^{:key k} [:li [:span [:b (name k)] ": "] [:span [ask ctx v]]])])

(defn- resource-editor-schema-based [ctx obj options]
  (let [schema (:ssoup/schema options)
        on-change (:on-change options)
        prop-editor (fn [p l r v]
                      ^{:key p}
                      [:tr
                       [:td [:b l] ": "]
                       [:td [ask ctx r v #{:ssoup/ask-with-editor}
                             {:ssoup/reuse-existing-ask-context true
                              :on-change                        (fn [value]
                                                                  (swap! obj assoc p value)
                                                                  (when on-change
                                                                    (on-change @obj)))
                              :ssoup/item-type                  r}]]])]
    [:table
     [:tbody
      (prop-editor :rdfs/label "Name" :xsd/string (get @obj :rdfs/label))
      (let [types (keys schema)
            props (map (fn [type] (-> schema (get type) :ssoup/schema :ssoup/properties)) types)
            props (distinct (apply concat props))]
        (for [prop props]
          (let [p (:id prop)
                l (:rdfs/label prop)
                r (:rdfs/range prop)
                v (get @obj p)]
            (prop-editor p l r v))))]]))

(defn- array-editor [ctx array options]
  [:ol (for [item @array]
         ^{:key item} [:li [ask ctx (:rdf/type item) item]])])

(defn- set-editor [ctx s options]
  ; this outer fn is needed because ssoup is calling set-editor
  ; as a function, bypassing reagent
  (fn []
    (let [container-id (:ssoup/container-id ctx)
          editor-state (subscribe [:widget-state container-id])
          selected-item (subscribe [:selected-item container-id])
          item-type (:ssoup/item-type options)
          item-schema (:ssoup/item-schema options)
          on-change (:on-change options)
          on-answer (:on-answer options)]
      (fn []
        (cond
          (or (= @editor-state :add-item)
              (= @editor-state :edit-item))
          [:div
           (when-let [item-type-name (:rdfs/label item-schema)]
             [:p
              (when (= @editor-state :add-item)
                [:b (str "Add new " item-type-name)])
              (when (= @editor-state :edit-item)
                [:b (str "Edit " item-type-name " " (:rdfs/label @selected-item))])])
           (let [init-value (when (= @editor-state :edit-item) @selected-item)]
             [ask ctx item-type init-value #{:ssoup/ask-with-editor}
              {:on-answer              (fn [ctx value]
                                         (let [value (if (and (map? value) (not (:id value)))
                                                       (assoc value :id (uuid/make-random-keyword :ssoup))
                                                       value)]
                                           (when (= @editor-state :edit-item)
                                             (reset! s (disj @s @selected-item)))
                                           (reset! s (conj @s value))
                                           (when on-change
                                             (on-change @s)))
                                         (dispatch [:show container-id]))
               :on-cancel              #(dispatch [:show container-id])
               :create-new-ask-context true}])]

          (= @editor-state :delete-item)
          [:div
           (when-let [item-type-name (:rdfs/label item-schema)]
             [:div
              [:p [:b (str "Are you sure you want to delete " item-type-name " " (:rdfs/label @selected-item))]]
              [:button {:on-click #(dispatch [:show container-id])} "Cancel"]
              [:button {:on-click (fn []
                                    (dispatch [:confirm-delete-set-item container-id s @selected-item ctx options]))} "Delete"]])]

          :else
          [:div
           [show ctx nil @s {:ssoup/item-type item-type
                             :ssoup/actions   [{:id      :edit-object
                                                :name    "Edit"
                                                :handler (fn [obj options]
                                                           (dispatch [:edit-item container-id obj]))}
                                               {:id      :delete-object
                                                :name    "Delete"
                                                :handler (fn [obj options]
                                                           (dispatch [:delete-item container-id obj]))}]}]
           [:a {:style   {:color "#0000dd"}
                :onClick #(dispatch [:add-item container-id])} "Add item"]])))))

;------------------------------------
; SELECTORS
;------------------------------------

(defn- dropdown [ctx obj-type options]
  (let [items (atom nil)]
    ; (go (let [vals (<! (s/list-objects ctx obj-type))]
    ;      (reset! items vals)
    [:select
     (when @items
       (for [item @items]
         [:option {:value item} item]))]))

; the array-type must somehow contain the item type
(defn- dropdown-multi [ctx array-type options])

;------------------------------------
; NOTIFICATIONS
;------------------------------------

(defn- global-notifications [ctx]
  (let [notifications (subscribe [:global-notifications])]
    (fn []
      [:fieldset {:style {:border "1px gray solid"}}
       [:legend {:style {:border "1px gray solid"
                         :margin-left "1em"
                         :padding "0.2em 0.8em"}}  "Notification area"]
       [:ul.notifications
        (for [notification @notifications]
          (let [{notification-id :id
                 severity :ssoup/severity
                 message  :ssoup/message} notification]
            ^{:key notification-id}
            [(keyword (str "li.notification." (name severity))) {:style (when (= :error severity) {:color "red"})} message]))]])))

;------------------------------------
; AUTH FORM
;------------------------------------

(defn- login-form [ctx]
  (let [engine (:engine ctx)
        user (atom nil)
        pass (atom nil)
        reset (fn [] (reset! user) (reset! pass))
        success (fn [user-details]
                  (dispatch [:logged-in user-details]))
        error (atom nil)]
    [:div
     [:fieldset.login-form
      [:legend (msg "Sign in")]
      [:span (msg :user)]
      [text-input user]
      [:span (msg :password)]
      [text-input pass {:type "password"}]
      [:button.reset-button {:on-click reset} (msg :reset)]
      [:button.login-button {:on-click #(auth/login! engine
                                                     @user @pass
                                                     success
                                                     error)} (msg :login)]
      (if-let [error @error]
        [:div.error error])]
     [:div
      [:button.signup-button {:on-click #(dispatch [:signup-form])}
       (msg :signup)]]]))

(defn- signup-form [ctx user-type]
  (let [rdf-ctx (-> ctx :engine :default-context)
        user-details {:rdf/context rdf-ctx
                      :rdf/type user-type}]
    [edit ctx user-type user-details
     {:ssoup/data-tags #{:ssoup/auth}
      :on-cancel       #(dispatch [:login-form])
      :on-answer       #(dispatch [:login-form])}]))

(defn- logout-form [ctx]
  (let [engine (:engine ctx)
        success (fn []
                  (dispatch [:logged-out]))
        error (atom nil)]
    [:div
     [:button.logout-button {:on-click #(auth/logout! engine success error)}
      (msg :logout)]]))

(defn- auth-template [ctx]
  (let [engine (:engine ctx)
        user-type (get-in engine [:options :ssoup/user-type])
        current-user (subscribe [:current-user])
        section (subscribe [:section])]
    (fn []
      (if @current-user
        [logout-form ctx]
        (if (= @section :signup-form)
          [signup-form ctx user-type]
          [login-form ctx])))))

;------------------------------------
; TEMPLATES
;------------------------------------

(defn main-template [ctx options]
  (let [engine (:engine ctx)
        current-user (subscribe [:current-user])]
    (fn []
      [:div
       [:div [auth-template ctx]]
       [:hr]
       [:br]
       [:div
        (if @current-user
          [:div {:key :private-content-container}
           (get-container engine :ssoup-default-theme/private-content-container)]
          [:div {:key :public-content-container}
           (get-container engine :ssoup-default-theme/public-content-container)])
        [:br]
        [:hr]
        [:div [global-notifications]]]]))
  #_(let [{ui-engine :engine
           container :return-value}
          (container* ui-engine :ssoup-default-theme/main-container)]
      (retval ui-engine ui-session
              [:div container])))

;------------------------------------
; THEME
;------------------------------------

(defn init-theme [ui-engine ui-session ctx options]
  (debug "Registering theme ssoup default theme")

  (sc/register-theme* ui-engine ui-session ctx :ssoup/default-theme
                      {:library-id :ssoup/base-ui-services
                       :viewers    [[:ssoup-default-theme/string-viewer nil :xsd/string string-viewer]
                                    [:ssoup-default-theme/keyword-viewer nil :ssoup/keyword keyword-viewer]
                                    [:ssoup-default-theme/number-viewer nil :xsd/decimal number-viewer]
                                    [:ssoup-default-theme/number-viewer nil :xsd/double number-viewer]
                                    [:ssoup-default-theme/number-viewer nil :xsd/float number-viewer]
                                    [:ssoup-default-theme/boolean-viewer nil :xsd/boolean boolean-viewer]
                                    [:ssoup-default-theme/resource-viewer nil :rdfs/Resource resource-viewer-schema-based]
                                    #_[:ssoup-default-theme/compact-resource-viewer
                                       #{:ssoup/compact} :rdfs/Resource
                                       compact-resource-viewer]
                                    [:ssoup-default-theme/link-viewer nil :ssoup/link link-viewer]
                                    [:ssoup-default-theme/array-viewer nil :ssoup/array array-viewer]
                                    [:ssoup-default-theme/set-viewer nil :ssoup/set set-viewer]
                                    [:ssoup-default-theme/folder-viewer nil :ssoup/folder folder-viewer]]

                       :editors    [[:ssoup-default-theme/string-editor nil :xsd/string
                                     string-editor]
                                    [:ssoup-default-theme/keyword-editor nil :ssoup/keyword keyword-editor]
                                    [:ssoup-default-theme/number-editor nil :xsd/decimal number-editor]
                                    [:ssoup-default-theme/number-editor nil :xsd/double number-editor]
                                    [:ssoup-default-theme/number-editor nil :xsd/float number-editor]
                                    [:ssoup-default-theme/boolean-editor nil :xsd/boolean boolean-editor]
                                    [:ssoup-default-theme/resource-editor nil :rdfs/Resource resource-editor-schema-based]
                                    [:ssoup-default-theme/array-editor nil :ssoup/array array-editor]
                                    [:ssoup-default-theme/set-editor nil :ssoup/set set-editor]
                                    [:ssoup-default-theme/email-editor nil :vcard/Email email-editor]]

                       :selectors  [[:ssoup-default-theme/object-selector nil :rdfs/Resource dropdown]
                                    [:ssoup-default-theme/array-selector nil :ssoup/array dropdown-multi]]

                       :containers [[:ssoup-default-theme/public-content-container]
                                    [:ssoup-default-theme/private-content-container]]

                       :templates  [[:ssoup-default-theme/main-template main-template]]}))

(defn init-default-theme* [ui-engine]
  ; registering subscribers
  (reg-sub :section
           (fn [db _]
             (:section db)))

  (reg-sub :widget-state
           (fn [db [_ container-id]]
             (get-in db [:widget-state container-id])))

  (reg-sub :selected-item
           (fn [db [_ container-id]]
             (get-in db [:selected-item container-id])))

  ; registering handlers
  (reg-event-db :login-form
                (fn [db [_]]
                  (assoc db :section :login-form)))

  (reg-event-db :signup-form
                (fn [db [_]]
                  (assoc db :section :signup-form)))

  (reg-event-db :add-item
                (fn [db [_ container-id]]
                  (assoc-in db [:widget-state container-id] :add-item)))

  (reg-event-db :edit-item
                (fn [db [_ container-id item-to-edit]]
                  (-> db
                      (assoc-in [:selected-item container-id] item-to-edit)
                      (assoc-in [:widget-state container-id] :edit-item))))

  (reg-event-db :delete-item
                (fn [db [_ container-id item-to-delete]]
                  (-> db
                      (assoc-in [:selected-item container-id] item-to-delete)
                      (assoc-in [:widget-state container-id] :delete-item))))

  (reg-event-db :confirm-delete-set-item
                (fn [db [_ container-id s item-to-delete ctx options]]
                  (reset! s (disj @s item-to-delete))
                  (when-let [on-change (:on-change options)]
                    (on-change @s))
                  (-> db
                      (assoc-in [:selected-item container-id] nil)
                      (assoc-in [:widget-state container-id] :show))))

  (reg-event-db :show
                (fn [db [_ container-id]]
                  (assoc-in db [:widget-state container-id] :show)))

  (-> ui-engine
      (derive* :ssoup/default-theme :ssoup/default-flavor)
      (s/register-service* {:library-id :ssoup/base-ui-services
                            :fname      :ssoup-default-theme/init-theme
                            :handler    (fn [ui-engine ui-session ctx args options]
                                          (init-theme ui-engine ui-session ctx options))
                            :flavor     :ssoup/default-theme
                            :user-role  :ssoup/any-user
                            :classes    #{:ssoup/init-theme}
                            :arg-types  {}})))
