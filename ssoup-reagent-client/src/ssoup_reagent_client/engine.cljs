(ns ^:figwheel-always ssoup-reagent-client.engine
  (:require
    [clojure.core.async :as async :refer [<! chan >! put! close! pipeline]]
    [ssoup-clj-core.core :as s]
    [ssoup-clj-client.engine :as sce]
    [ssoup-clj-client.modules :as sm]
    [ssoup-reagent-client.core :as rc]
    [re-frame.core :refer [reg-sub reg-event-db subscribe dispatch path trim-v after]]
    [taoensso.timbre :as timbre :refer [log trace debug info warn error fatal]]
    [ssoup-clj-core.uuid :as uuid])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(derive :ssoup/reagent-engine :ssoup/ui-engine)

(defn register-reframe-subs []
  (reg-sub :current-user
           (fn [db _]
             (:current-user db)))
  (reg-sub :global-notifications
           (fn [db _]
             (:global-notifications db))))

(defn register-reframe-handlers []
  (reg-event-db :logged-in
                (fn [db [_ user-details]]
                  (dispatch [:add-global-notification
                             {:id :session-status
                              :ssoup/severity :info
                              :ssoup/message (str "User " (:name user-details) " logged in")}])
                  (assoc db :current-user user-details)))

  (reg-event-db :logged-out
                (fn [db [_]]
                  (dispatch [:add-global-notification {:id :session-status
                                                       :ssoup/severity :info
                                                       :ssoup/message "Session terminated"}])
                  (assoc db :current-user nil)))

  (reg-event-db :add-global-notification
                (fn [db [_ notification]]
                  (let [notification-id (:id notification)
                        notifications (or (:global-notifications db) [])
                        notifications (if notification-id
                                        (filter #(not= (:id %) notification-id) notifications)
                                        notifications)
                        notification (if notification-id
                                       notification
                                       (assoc notification :id (uuid/make-random-str)))]
                    (assoc db :global-notifications (conj notifications notification))))))

(defn init-reframe [reagent-engine]
  (register-reframe-subs)
  (register-reframe-handlers)
  #_(when-let [current-user (s/get-current-user)]
    (dispatch [:logged-in current-user]))
  reagent-engine)

(defn init-engine-fn [reagent-engine]
  (info "Initializing Reagent Ssoup engine")
  (go (let [reagent-engine (init-reframe reagent-engine)
            reagent-engine (<! (sce/init-engine-fn* reagent-engine))
            reagent-engine (assoc reagent-engine :author-scs-rules (sm/compute-scs-rules))
            reagent-engine (rc/render-root-container* reagent-engine)]
        reagent-engine)))

(defmethod s/on-notification :ssoup/reagent-engine [reagent-engine notification]
  (dispatch [:add-global-notification notification]))

(defmethod s/init-engine* :ssoup/reagent-engine [reagent-engine]
  (init-engine-fn reagent-engine))

(defn new-reagent-engine [name default-context options]
  (let [engine (sce/new-ui-engine name default-context options)]
    (-> engine
        (assoc :type :ssoup/reagent-engine))))
