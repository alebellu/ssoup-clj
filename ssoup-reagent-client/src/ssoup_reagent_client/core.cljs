(ns ^:figwheel-always ssoup-reagent-client.core
  (:require
    [cljs.core.async :as async :refer [<! chan close! put!]]
    [cljs.core.async.impl.protocols :refer [Channel]]
    [reagent.core :as reagent]
    [re-frame.core :refer [reg-sub reg-event-db subscribe dispatch path trim-v after]]
    [ssoup-clj-core.core :as s]
    [ssoup-clj-core.utils :refer [retval gretval as-set str-pprint]]
    [ssoup-clj-core.types :refer [get-type ssoup-cast dispatch-on-type box unbox]]
    [ssoup-clj-core.uuid :as uuid]
    [ssoup-clj-core.state :as state]
    [ssoup-rdf.core :as rdf]
    [ssoup-clj-client.core :as sc]
    [taoensso.timbre :as timbre :refer [log trace debug info warn error fatal]]
    [ssoup-clj-core.datasources :as ds])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(enable-console-print!)

; -------------------------------------------------------
; IWithAuthHandler
; -------------------------------------------------------

(defmethod sc/ensure-auth* :ssoup/reagent-engine [reagent-engine]
  (let [current-user (subscribe [:current-user])]
    (if @current-user
      @current-user
      (let [auth-chan (chan)]
        (add-watch current-user :auth-watcher
                   (fn [key ref old-state new-state]
                     (when new-state
                       (put! auth-chan new-state))))
        auth-chan))))

; -------------------------------------------------------
; Reagent UI state
; -------------------------------------------------------

(defn wrap-ratom
  "Reagent atoms must be dereferenced
   by Reagent during the render phase ! In dynamic contexts
   use wrap-ratom to ensure it will be the case.
   IMPORTANT: call wrap-ratom with brackets and not parenthesis,
   so that Reagent will reevaluate it at every render:
     [wrap-ratom my-ratom]"
  [ratom]
  (fn []
    @ratom))

; -------------------------------------------------------
; IWithContainers
; -------------------------------------------------------

(defn register-container-ratom* [reagent-engine container-id ratom]
  (debug "Registering container with id " container-id)
  (assoc-in reagent-engine [:container-ratoms container-id] ratom))

(defmethod sc/register-container* :ssoup/reagent-engine
  [reagent-engine container-id]
  (let [container-ratom (reagent/atom nil)]
    (register-container-ratom* reagent-engine container-id container-ratom)))

(defmethod sc/get-container :ssoup/reagent-engine
  [reagent-engine container-id]
  (let [container-ratoms (get reagent-engine :container-ratoms)
        container-ratom (get container-ratoms container-id)]
    (if container-ratom
      [wrap-ratom container-ratom]
      [:div "Container with id " (str container-id) "could not be found"])))

(defn container* [reagent-engine container-id]
  (let [container-ratoms (get reagent-engine :container-ratoms)
        container-ratom (get container-ratoms container-id)]
    (if container-ratom
      (retval reagent-engine nil [wrap-ratom container-ratom])
      (let [container-ratom (reagent/atom nil)]
        (retval (register-container-ratom* reagent-engine container-id container-ratom)
                nil
                [wrap-ratom container-ratom])))))

(defn container!
  "This function is supposed to be called from reagent markup.
   It will operate on the default engine in the pool and modify its state"
  [container-id]
  (let [engine (state/get-engine)
        {engine    :engine
         container :return-value}
        (container* engine container-id)]
    (state/set-engine-state! engine)
    container))

(defn render-root-container* [reagent-engine]
  (let [{reagent-engine :engine
         container      :return-value}
        (container* reagent-engine :ssoup/root-container)]
    (reagent/render container (. js/document (getElementById "app")))
    reagent-engine))

(defn template-registered* [reagent-engine ctx template-id template-body]
  ; called so that any container defined in the template definition is instantiated immediatly,
  ; without waiting for the next rerender cycle
  (reagent/render-to-string (template-body (assoc ctx :engine reagent-engine) {})))

(defmethod sc/set-container-content :ssoup/reagent-engine
  [reagent-engine reagent-session container-id content]
  (let [ratom (get-in reagent-engine [:container-ratoms container-id])
        content (if (fn? content)
                  (content {:engine  reagent-engine
                            :session reagent-session}
                           {})
                  content)]
    (if ratom
      (reset! ratom content)
      (warn "Container ratom" container-id "not found"))
    ; always return nil
    nil))

; -------------------------------------------------------
; IUIRuntime
; -------------------------------------------------------

(defmethod sc/call-viewer :ssoup/reagent-engine [ui-engine ui-session viewer ctx
                                                 obj
                                                 options]
  (go
    (let [ctx (assoc ctx :engine ui-engine :session ui-session)
          id (or (:id obj) (str (uuid/make-random)))
          fndef (:fndef ctx)
          service-arg-types (:arg-types fndef)
          obj (unbox obj)
          obj-types (get-type ui-engine obj)
          obj-types (as-set obj-types)
          schema (gretval ui-engine
                          (<! (s/get-types-schema ui-engine ui-session ctx
                                                  #{:ssoup/schema} obj-types
                                                  {})))
          item-type (:ssoup/item-type options)
          {item-schema :ssoup/schema}
          (when item-type
            (<! (s/get-type-schema ui-engine ui-session ctx
                                       #{:ssoup/schema}
                                       item-type
                                       {})))
          viewer-ui (apply viewer [ctx obj (merge options
                                                  {:rdf/context  rdf-ctx
                                                   :ssoup/schema schema
                                                   :ssoup/item-schema item-schema})])
          ; attach a key to the viewer to make sure that react rerenders it
          viewer-ui (with-meta viewer-ui {:key id})]
      (when-let [container-id (:ssoup/container options)]
        (sc/set-container-content ui-engine ui-session container-id viewer-ui)))))

#_(defmethod sc/call-editor :ssoup/reagent-engine [ui-engine ui-session editor ctx
                                                   obj
                                                   options]
    (let [ctx (assoc ctx :engine ui-engine :session ui-session)
          id (or (:id obj) (str (uuid/make-random)))
          obj-type (get-type ui-engine obj)
          obj (unbox obj)
          obj (if (map? obj) (assoc obj :id id) obj)
          value (atom obj)]
      (go (let [schema (gretval ui-engine
                                (<! (s/get-type-schema ui-engine ui-session ctx
                                                       #{:ssoup/schema} obj-type
                                                       {})))
                editor-ui (apply editor [ctx value {:ssoup/schema schema}])
                ; attach a key to the editor to make sure that react rerenders it
                editor-ui (with-meta editor-ui {:key id})]
            (when-let [container-id (:ssoup/container options)]
              (sc/set-container-content ui-engine ui-session container-id editor-ui))))))

(defmethod sc/error-message :ssoup/reagent-engine
  [ui-engine ui-session message exception]
  [:p.error {:title (str exception)} message])

; -------------------------------------------------------
; IWithAskContexts
; -------------------------------------------------------
(defmethod sc/new-ask-context-ui :ssoup/reagent-engine [ui-engine ui-session
                                                        asker-ui
                                                        asker-name
                                                        send-answer
                                                        cancel-handler
                                                        options]
  (fn []
    (if (:ssoup/immediate options)
      [:div {:key asker-name} asker-ui]
      [:div
       [:div {:key asker-name} asker-ui]
       [:div
        [:button {:on-click send-answer} "Ok"]
        [:button {:on-click cancel-handler} "Cancel"]]])))

; -------------------------------------------------------
; IWithSessions
; -------------------------------------------------------

(defn new-reagent-session-fn* [ui-engine additional-attributes options]
  (sc/new-ui-session-fn* ui-engine (merge {:type             :ssoup/reagent-session
                                           :container-ratoms {}}
                                          additional-attributes)
                         options))

(defmethod s/new-session* :ssoup/reagent-engine [engine additional-attributes options]
  (new-reagent-session-fn* engine additional-attributes options))

; -------------------------------------------------------
; IWithUser
; -------------------------------------------------------

(defn set-current-user-fn* [engine session user]
  (let [ret (s/set-current-user-fn* engine session user)]
    (dispatch [:logged-in user])
    ret))

(defmethod s/set-current-user* :ssoup/reagent-engine [engine session user]
  (set-current-user-fn* engine session user))
