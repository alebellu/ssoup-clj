(ns ssoup-clj-server.views
  (:require [geheimtur.util.auth :refer [get-identity]]
            [ring.util.response :as ring-resp]
            [hiccup.page :as h]
            [hiccup.element :as e]))

(def head
  [:head
   [:title "SSOUP"]])

(defn body [user & content]
  [:body
   [:div
     content]])

(defn error-page
  [context]
  (ring-resp/response
    (h/html5 head (body (:user context)
                        [:div {:class "col-lg-8 col-lg-offset-2"}
                         [:h2 (:title context)]
                         [:p (:message context)]]))))

(defn unauthorized
  [request]
  (ring-resp/response
    (h/html5 head (body nil
                        [:div {:class "col-lg-8 col-lg-offset-2"}
                         [:h2 "Unauthorized"]
                         [:p "It looks like there was a problem authenticating you, sir. Please try again."]]))))

(defn home-page
  [request]
  {:status  200
   :session {} ; immediatly create a session object
   :body (h/html5 head (body (get-identity request)
                        [:h1 "SSOUP"]))})
