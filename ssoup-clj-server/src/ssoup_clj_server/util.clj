(ns ssoup-clj-server.util
  (:gen-class)
  (:import (java.net HttpCookie URLDecoder)))

(defn lazy-contains? [col key]
  (some #{key} col))

(defn keys-to-keywords [e]
  "Transform the input sequence to an hashmap where keys are transformed to keywords"
  (apply hash-map
         (flatten
           (for [[k v] e]
             [(keyword k) v]))))

(defn get-env [var-name]
  (System/getenv var-name))

(defn get-cookie-value [cookies cookie-name]
  (let [v (first
            (filter
              (fn [^HttpCookie cookie]
                (= cookie-name (.getName cookie)))
              cookies))]
    (when v
      (URLDecoder/decode (.getValue v) "UTF-8"))))
