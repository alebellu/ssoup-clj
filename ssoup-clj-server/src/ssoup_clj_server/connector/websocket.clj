(ns ssoup-clj-server.connector.websocket
  (:gen-class)
  (:require [clojure.core.async :as async :refer [put! <! <!! >! go]]
            [clojure.core.async.impl.protocols :refer [Channel]]
            [cognitect.transit :as t]
            [io.pedestal.http.jetty.websockets :as ws]
            [ssoup-clj-core.uuid :as uuid]
            [ssoup-clj-core.utils :refer [str-pprint]]
            [ssoup-clj-core.connector :as connector]
            [ssoup-clj-server.connector.common :as common]
            [ssoup-clj-server.util :as util]
            [ring.middleware.session.store :as sessions]
            [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)])
  (:import [org.eclipse.jetty.websocket.api Session WebSocketConnectionListener WebSocketListener]
           (java.io ByteArrayOutputStream ByteArrayInputStream)))

; WebSockets related code derived from :
; https://github.com/pedestal/pedestal/blob/master/samples/jetty-web-sockets/src/jetty_web_sockets/service.clj#L11

(def ws-clients (atom {}))
(def ws-send-channels (atom {}))
(def sid->cid (atom {}))
(def ws-session->cid (atom {}))

(defn send-message [client-id msg headers])

(defn get-sid [^Session ws-session]
  (let [request (.getUpgradeRequest ws-session)
        cookies (.getCookies request)
        sid (util/get-cookie-value cookies "SID")]
    sid))

(defn ws-client-connected [websocket-connector ctx ^Session ws-session send-ch]
  ; do nothing: wait for the open-request message before accepting the connection
  ; this is needed because the first connection message does not contain the cookie sid
  ; temporarly store the send channel with key ws-session
  (swap! ws-send-channels assoc ws-session send-ch))

(defn open-request [websocket-connector ctx ^Session ws-session cid-from-request]
  (let [send-ch (get @ws-send-channels ws-session)]
    (when-not send-ch
      (throw (ex-info "A open request was received but the connection send channel was lost" {})))
    (let [sid (get-sid ws-session)
          cid-from-sid (when sid (get @sid->cid sid))
          cid (or cid-from-request cid-from-sid (uuid/make-random-str))]
      (when sid
        (if (and cid-from-request cid-from-sid (not= cid-from-request cid-from-sid))
          (throw (ex-info "Provided cid does not match recorded cid" {})))
        (swap! sid->cid assoc sid cid))
      (swap! ws-session->cid assoc ws-session cid)
      (let [existing-client (contains? @ws-clients cid)
            http-session (when sid (sessions/read-session (:session-store websocket-connector) sid))]
        (if existing-client
          (debug "Existing ws client re-connected. cid " cid)
          (debug "New ws client connected. cid " cid))
        (swap! ws-clients assoc cid {:cid          cid
                                     :ws-session   ws-session
                                     :send-channel send-ch
                                     :http-session http-session})
        ; we don't need to store the send channel separately anymore
        (swap! ws-send-channels dissoc ws-session)
        ; Send the cid: the client is supposed to pass the cid back to the server on all subsequent requests
        (debug "send open ack " (send-message cid :ssoup/open-ack "" {:cid cid}))
        (when existing-client
          ; if its a reconnection from a known client, send all queued and yet unsent messages
          (connector/resend-enqueued-messages websocket-connector ctx cid))))))

(defn connection-closed [ws-connector ws-session]
  (when-let [cid (get @ws-session->cid ws-session)]
    (swap! ws-clients assoc-in [cid :send-channel] nil))
  (swap! ws-session->cid dissoc ws-session))

(defn send-message [cid msg-type msg-body headers]
  (if-let [{^Session ws-session :ws-session
            send-ch :send-channel} (get @ws-clients cid)]
    (if (and ws-session (.isOpen ws-session) send-ch)
      (let [out (ByteArrayOutputStream. 2000)
            tw (t/writer out :json)
            headers (assoc headers :type msg-type)
            _ (t/write tw {:header headers
                           :body   msg-body})
            payload (.toString out)]
        (put! send-ch payload)
        (trace "Sending message " msg-type " with content " msg-body " to client: " cid)
        true)
      ; connection with the client dropped, return false
      false)
    ; unknown client, return false
    false))

(defn on-text [websocket-connector ctx ws-session ^String msg]
  (let [in (ByteArrayInputStream. (.getBytes msg))
        tr (t/reader in :json)
        message (t/read tr)
        headers (:header message)
        msg-type (:type headers)
        msg-body (:body message)
        cid (:cid headers)
        {http-session :http-session} (get @ws-clients cid)
        ctx (merge headers
                   {:client-id    cid
                    :http-session http-session})]
    (if (= :ssoup/open-request msg-type)
      (open-request websocket-connector ctx ws-session cid)
      (connector/dispatch-message websocket-connector ctx msg-type msg-body))))

;; This is just for demo purposes
(defn send-and-close! []
  (let [[cid {^Session ws-session :ws-session
              send-ch             :send-channel}] (first @ws-clients)]
    (async/put! send-ch "A message from the server")
    ;; And now let's close it down...
    (async/close! send-ch)
    ;; And now clean up
    (swap! ws-clients dissoc cid)))

;; Also for demo purposes...
(defn send-message-to-all!
  [message]
  (doseq [[cid {^Session ws-session :ws-session
                channel             :send-channel}] @ws-clients]
    ;; The Pedestal Websocket API performs all defensive checks before sending,
    ;;  like `.isOpen`, but this example shows you can make calls directly on
    ;;  on the Session object if you need to
    (when (.isOpen ws-session)
      (async/put! channel message))))

(defrecord WebSocketListenerAdapter [ws-map
                                     session]
  WebSocketConnectionListener
  (onWebSocketConnect [this ws-session]
    (reset! session ws-session)
    (when-let [f (:on-connect ws-map)]
      (f ws-session)))
  (onWebSocketClose [this status-code reason]
    (when-let [f (:on-close ws-map)]
      (f (deref (:session this)) status-code reason))
    (reset! session nil))
  (onWebSocketError [this cause]
    (when-let [f (:on-error ws-map)]
      (f (deref (:session this)) cause)))

  WebSocketListener
  (onWebSocketText [this msg]
    (when-let [f (:on-text ws-map)]
      (f (deref (:session this)) msg)))
  (onWebSocketBinary [this payload offset length]
    (when-let [f (:on-binary ws-map)]
      (f (deref (:session this)) payload offset length))))

(defn make-ws-listener
  "Given a map representing WebSocket actions
  (:on-connect, :on-close, :on-error, :on-text, :on-binary),
  return a WebSocketConnectionListener.
  Values for the map are functions with the same arity as the interface."
  [ws-map]
  (->WebSocketListenerAdapter ws-map (atom nil)))

(defn ws-listener [req response ws-map]
  (make-ws-listener ws-map))

(defn- make-context-configurator [ws-connector ws-path ws-map]
  (fn [ctx]
    (let [ws-paths {ws-path (merge {:on-connect (ws/start-ws-connection (partial ws-client-connected
                                                                                 ws-connector
                                                                                 ctx))
                                    :on-text    (fn [ws-session message]
                                                  (let [message (on-text ws-connector ctx ws-session message)]
                                                    (when-let [on-message (:on-message ws-map)]
                                                      (on-message ws-connector ws-session message))))
                                    :on-binary  (fn [ws-session payload offset length] (info "Binary Message!" payload))
                                    :on-error   (fn [ws-session t]
                                                  (when-not (.isOpen ws-session)
                                                    (connection-closed ws-connector ws-session)
                                                    (error "WS Error happened: " t)
                                                    (.printStackTrace t)))
                                    :on-close   (fn [ws-session num-code reason-text]
                                                  (connection-closed ws-connector ws-session)
                                                  (info "WS Closed: " num-code ": " (str-pprint reason-text)))}
                                   ws-map)}]
      (ws/add-ws-endpoints ctx ws-paths {:listener-fn ws-listener}))))

; -------------------------------------------------------
; IConnector
; -------------------------------------------------------
(derive :ssoup/websocket-connector-server :ssoup/connector-server)

(defmethod connector/init :ssoup/websocket-connector-server
  [connector ctx]
  (go))

(defmethod connector/tear-down :ssoup/websocket-connector-server
  [connector ctx])

(defmethod connector/login :ssoup/websocket-connector-server
  [connector ctx user password options]
  (warn "Authentication via websocket not yet supported"))

(defmethod connector/logout :ssoup/websocket-connector-server
  [connector ctx options]
  (warn "Authentication via websocket not yet supported"))

(defmethod connector/exec :ssoup/websocket-connector-server
  [connector ctx message options]
  (let [client-id (:client-id ctx)
        http-session (:http-session ctx)
        ssoup-session-id (-> http-session :identity :ssoup/ssoup-session-id)
        call-id (:call-id ctx)]
    (try (let [return-value (common/exec message ssoup-session-id client-id)]
           (connector/send-or-enqueue-message
             connector ctx client-id
             :ssoup/return-value-message return-value {:status  :ok
                                                       :call-id call-id}))
         (catch Exception e
           (error "An exception occurred while serving websocket request: " e)
           (.printStackTrace e)
           (connector/send-or-enqueue-message
             connector ctx client-id
             :ssoup/exec-error nil {:call-id    call-id
                                    :status     :error
                                    :error-text (.getMessage e)})))))

(defmethod connector/topic-on-message :ssoup/websocket-connector-server [connector ctx topic-id message]
  (let [client-id (:client-id ctx)]
    (send-message client-id :ssoup/topic-on-message message {:topic-id topic-id})))

(defmethod connector/send-message :ssoup/websocket-connector-server
  [connector ctx destination msg-type msg-body headers]
  (go
    (send-message destination msg-type msg-body headers)))

(defn new-websocket-connector-server [websocket-url session-store options]
  (let [connector-server (common/new-connector-server)
        websocket-connector (merge connector-server
                                   {:type          :ssoup/websocket-connector-server
                                    :session-store session-store})]
    (merge websocket-connector
           {:context-configurator (make-context-configurator websocket-connector websocket-url options)})))
