(ns ssoup-clj-server.connector.http
  (:gen-class)
  (:require [clojure.core.async :as async :refer [put! <! <!! >! go]]
            [clojure.core.async.impl.protocols :refer [Channel]]
            [cognitect.transit :as t]
            [io.pedestal.http :as bootstrap]
            [io.pedestal.http.route :as route :refer [expand-routes]]
            [io.pedestal.http.body-params :as body-params]
            [io.pedestal.http.ring-middlewares :as middlewares]
            [io.pedestal.interceptor.helpers :as h :refer [on-response]]
            [geheimtur.interceptor :refer [interactive guard http-basic]]
            [geheimtur.impl.form-based :refer [default-login-handler default-logout-handler]]
            [geheimtur.impl.oauth2 :refer [authenticate-handler callback-handler]]
            [geheimtur.util.auth :as auth :refer [authenticate]]
            [geheimtur.util.response :as response]
            [ssoup-clj-core.uuid :as uuid]
            [ssoup-clj-core.connector :as connector]
            [ssoup-clj-server.connector.common :as common]
            [ssoup-clj-server.views :as views]
            [ssoup-clj-server.util :as util]
            [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)])
  (:import (org.eclipse.jetty.server Request)))

(def oauth-providers
  ; see https://github.com/propan/geheimtur-demo/blob/master/src/geheimtur_demo/service.clj
  {})

(defn ssoup-routes [http-connector public-routes check-auth]
  (let [exec-fn (fn [request]
                  (let [ctx {:request request}]
                    (connector/exec http-connector)))
        terse-routes
        [[(apply vector
                 "/" {:get (h/handler ::home-page views/home-page)}
                 ^:interceptors [(body-params/body-params)
                                 bootstrap/html-body]
                 (:public-pages public-routes))
          (apply vector
                 "/"
                 ^:interceptors [(body-params/body-params)
                                 bootstrap/transit-json-body
                                 (middlewares/content-type)]
                 ["/user"
                  ["/current" {:get (h/handler ::get-current-user
                                               (fn [request]
                                                 (let [; Ring middleware will add a :session key to the request
                                                       http-session (:session request)]
                                                   (if-let [user (:identity http-session)]
                                                     {:status 200
                                                      :body   user}
                                                     {:status 404}))))}]]
                 ["/login" {:post (h/handler ::login
                                             (fn [request]
                                               (let [credentials (:transit-params request)]
                                                 (when-let [identity (and credentials
                                                                          (check-auth
                                                                            (:username credentials)
                                                                            (:password credentials)))]
                                                   {:status  200
                                                    :session {:identity identity}}))))}]
                 ; better to use POST for logout too:
                 ; http://stackoverflow.com/questions/3521290/logout-get-or-post
                 ["/logout" {:post (h/handler ::logout
                                              (fn [request]
                                                {:status  200
                                                 :body    "ok"
                                                 :session {::identity nil}}))}]
                 ["/oauth.login" {:get (authenticate-handler oauth-providers)}]
                 ["/oauth.callback" {:get (callback-handler oauth-providers)}]
                 ["/exec" {:post (h/handler ::exec exec-fn)}]
                 ["/unauthorized" {:get (h/handler ::unauthorized views/unauthorized)}]
                 (:public-routes public-routes))]]
        ; _ (pprint terse-routes)
        routes (expand-routes terse-routes)]
    routes))

; -------------------------------------------------------
; IConnectorServer
; -------------------------------------------------------
(derive :ssoup/http-connector-server :ssoup/connector-server)

(defmethod connector/init :ssoup/http-connector-server
  [connector ctx]
  (go))

(defmethod connector/tear-down :ssoup/http-connector-server
  [connector ctx])

(defmethod connector/login :ssoup/http-connector-server
  [connector ctx user password options]
  (warn "Authentication via websocket not yet supported"))

(defmethod connector/logout :ssoup/http-connector-server
  [connector ctx options]
  (warn "Authentication via websocket not yet supported"))

(defmethod connector/exec :ssoup/http-connector-server
  [connector ctx message options]
  (let [request (:request ctx)
        message (:transit-params request)]
    (try
      (let [; Ring middleware will add a :session key to the request
            http-session (:session request)
            ; _ (pprint request)
            ; http session -> ssoup engine session
            user (:identity http-session)
            session-id (:ssoup/session-id user)
            return-value (common/exec message session-id nil)]
        {:status  200
         :headers {}
         :body    return-value})
      (catch Exception e
        (error "An exception occurred while serving http request: " e)
        (.printStackTrace e)
        {:status 500
         :body   (.getMessage e)}))))

(defn new-http-connector-server [public-routes check-auth options]
  (let [http-connector {:type :ssoup/http-connector-server}]
    (merge http-connector
           {:routes (ssoup-routes http-connector public-routes check-auth)})))
