(ns ssoup-clj-server.connector.common
  (:gen-class)
  (:require [clojure.core.async :as async :refer [put! <! <!! >! go]]
            [clojure.core.async.impl.protocols :refer [Channel]]
            [ssoup-clj-core.core :as s]
            [ssoup-clj-core.state :as state]
            [ssoup-clj-core.connector :as connector]
            [ssoup-clj-core.pub-sub :as ps]
            [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)]))

(defn exec [message ssoup-session-id client-id]
  (debug "API Exec body: " message)
  (let [classes (into #{} (for [class (:classes message)] (keyword class)))
        flavor (keyword (:flavor message))
        args (:args message)
        options (:options message)
        engine (state/get-engine)
        session (s/get-session engine ssoup-session-id)
        {engine  :engine
         session :session} (if session
                             {:engine  engine
                              :session session}
                             (s/new-session* engine {}
                                             {:session-id ssoup-session-id}))
        session (if (nil? (:client-id session))
                  (assoc session :client-id client-id)
                  (if (= (:client-id session) client-id)
                    session
                    (assoc session :client-id client-id)    ; TODO: thats strange, but for now lets just update it
                    #_(throw (Exception. (str "Client id " client-id " does not match the session client id "
                                              (:client-id session))))))
        engine (s/set-session* engine session)
        resp (s/ssoup-exec-sync engine session classes flavor [] {} args options)
        engine (:engine resp)
        return-value (:return-value resp)]
    (when engine
      (state/set-engine-state! engine))
    (trace "API Exec out: " return-value)
    return-value))

; -------------------------------------------------------
; IConnector
; -------------------------------------------------------
(derive :ssoup/connector-server :ssoup/connector)

(defmethod connector/subscribe :ssoup/connector-server [connector ctx topic-id]
  (let [{engine    :engine
         client-id :client-id} ctx]
    (ps/subscribe engine client-id connector ctx topic-id
                  {:on-update (fn [msg]
                                (connector/send-message connector ctx client-id
                                                        :ssoup/topic-message
                                                        msg
                                                        {:status   :ok
                                                         :topic-id topic-id}))})))

(defmethod connector/subscribe-to-object-updates :ssoup/connector-server
  [connector ctx data-tags object-id data-pattern options]
  (let [engine (state/get-engine)
        {session   :session
         client-id :client-id} ctx]
    (ps/subscribe-to-object-updates engine session ctx data-tags object-id data-pattern
                                    {:on-object-update    (fn [object-update]
                                                            (connector/send-message connector ctx client-id
                                                                                    :ssoup/object-update-message
                                                                                    object-update
                                                                                    {:status       :ok
                                                                                     :data-tags    data-tags
                                                                                     :object-id    object-id
                                                                                     :data-pattern data-pattern}))
                                     :send-initial-update (:send-initial-update options)})))

(defmethod connector/subscribe-to-schema-updates :ssoup/connector-server
  [connector ctx data-tags options]
  (let [engine (state/get-engine)
        {session   :session
         client-id :client-id} ctx]
    (ps/subscribe-to-schema-updates engine session ctx data-tags
                                    {:on-schema-update    (fn [schema-update]
                                                            (connector/send-message connector ctx client-id
                                                                                              :ssoup/schema-update-message
                                                                                              schema-update
                                                                                              {:status    :ok
                                                                                               :data-tags data-tags}))
                                     :send-initial-update (:send-initial-update options)})))

(defmethod connector/dispatch-message :ssoup/connector-server
  [connector ctx msg-type msg-body]
  (debug "A client sent: " msg-type ", body " msg-body)
  (when (= :ssoup/exec-message msg-type)
    (connector/exec connector ctx msg-body {}))
  (when (= :ssoup/subscribe-message msg-type)
    (let [topic-id (:topic-id ctx)]
      (connector/subscribe connector ctx topic-id)))
  (when (= :ssoup/subscribe-to-object-updates-message msg-type)
    (let [data-tags (:ssoup/data-tags msg-body)
          object-id (:ssoup/object-id msg-body)
          data-pattern (:ssoup/data-pattern msg-body)
          send-initial-update (:send-initial-update msg-body)]
      (connector/subscribe-to-object-updates connector ctx data-tags object-id data-pattern
                                             {:send-initial-update send-initial-update})))
  (when (= :ssoup/subscribe-to-schema-updates-message msg-type)
    (let [data-tags (:ssoup/data-tags msg-body)
          send-initial-update (:send-initial-update msg-body)]
      (connector/subscribe-to-schema-updates connector ctx data-tags
                                             {:send-initial-update send-initial-update})))
  (when (= :ssoup/topic-message msg-type)
    (let [topic-id (:topic-id ctx)]
      (connector/topic-on-message connector ctx topic-id msg-body))))

(defn new-connector-server []
  (let [connector-server (connector/new-connector)]
    connector-server))
