(ns ssoup-clj-server.base-clj-services
  (:gen-class)
  (:require [ssoup-clj-core.core :as s :refer [ssoup-exec-sync]]
            [ssoup-clj-core.utils :refer [retval]]
            [ssoup-clj-server.util :refer [get-env]]
            [clojure.core.async :as async :refer [<! <!! go]]
            [clojure.core.async.impl.protocols :refer [Channel]]
            [clojure.pprint :refer [pprint]]
            [clojure.core.async :as async :refer [go <! <!! >!]]
            [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)]))

; -------------------------------------------------------
; Library functions implementation.
; -------------------------------------------------------
(defn- pre-process-path [path]
  (-> path
      (clojure.string/replace "\\" "/")
      (clojure.string/replace "//" "/")
      (clojure.string/replace "C:" "")
      (clojure.string/replace "c:" "")))

(defn- list-files [path base-folder]
  (let [rel-path (fn [path]
                   (let [path (pre-process-path path)
                         bf (pre-process-path base-folder)
                         rpath (if (.startsWith path bf)
                                 (subs path (count bf))
                                 nil)
                         rpath (if (and rpath (not (.startsWith rpath "/")))
                                 (str "/" rpath)
                                 rpath)]
                     rpath))
        full-path (str base-folder "/" path)
        d (clojure.java.io/file full-path)]
    {:rdf/type     :ssoup/folder
     :rdfs/label   (rel-path (.getCanonicalPath d))
     :ssoup/parent (when (-> d .getParentFile)
                     (rel-path (-> d .getParentFile .getPath)))
     :ssoup/files  (doall (for [f (.listFiles d)]
                            {:rdf/type   (if (.isDirectory f) :ssoup/folder :ssoup/file)
                             :rdfs/label (.getName f)}))}))

; -------------------------------------------------------
; Library registration.
; -------------------------------------------------------
(defn- init-library [])

(s/register-library!
  {:library-id      :ssoup/base-clj-services
   :engine-type     :ssoup/clj-engine
   :dependencies [:ssoup/base-services]
   :ssoup/services  [{:fname     :ssoup/list-files
                      :handler   (fn [engine session ctx args options]
                                   (let [path (:ssoup/path args)
                                         base-folder (get-env "BASE_FOLDER")]
                                     (retval engine session
                                             (list-files path base-folder))))
                      :flavor    :ssoup/default-flavor
                      :user-role :ssoup/any-user
                      :classes   #{:ssoup/list-files}
                      :arg-types {:ssoup/path :xsd/string}}]
   :init-library-fn init-library})
