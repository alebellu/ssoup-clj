(ns ssoup-clj-server.engine
  (:gen-class)
  (:require [clojure.core.async :as async :refer [go <! chan >! put! close! pipeline]]
            [ssoup-clj-core.engine :as se]
            [ssoup-clj-core.core :as s]
            [ssoup-clj-core.types :as types]
            [ssoup-clj-core.connector :as connector]
            [ssoup-clj-server.base-clj-services]
            [ssoup-clj-server.core]
            [ssoup-clj-server.connector.http :as http]
            [ssoup-clj-server.connector.websocket :as websocket]
            [ssoup-clj-core.utils :refer [retval]]
            [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)]))

(derive :ssoup/clj-engine :ssoup/engine)

(defn- init-type-system [engine]
  (let [h (get engine :types)]
    (assoc engine :types h)))

(defn init-engine-fn* [engine]
  (go (let [libraries (or (:ssoup/libraries engine) #{})
            libraries (conj libraries :ssoup/base-clj-services)
            ; all clj engines should expose the clj base libraries
            engine (assoc engine :ssoup/libraries libraries)
            engine (<! (se/init-engine-fn* engine))
            engine (init-type-system engine)
            connectors (:server-connectors engine)
            ctx {:engine engine}]
        (when connectors
          ; initialize all the regostered connectors
          (loop [connectors connectors]
            (when-not (empty? connectors)
              (let [[_ connector] (first connectors)]
                (<! (connector/init connector ctx))
                (recur (rest connectors))))))
        engine)))

(defmethod s/init-engine* :ssoup/clj-engine [engine]
  (init-engine-fn* engine))

(defn new-server-engine [name default-context connectors options]
  (let [base-engine (se/new-engine name default-context options)
        engine (assoc base-engine :type :ssoup/clj-engine)]
    ; register the server connectors
    (reduce
      (fn [engine connector]
        (s/register-server-connector* engine connector))
      engine connectors)))
