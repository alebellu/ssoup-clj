(ns ssoup-clj-server.core
  (:gen-class)
  (:use [hiccup.core])
  (:require [ssoup-clj-core.core :as s]
            [ssoup-clj-core.state :as  state]
            [ssoup-clj-core.pub-sub-impl] ; don't remove, needed to require multimethods implementation
            [ssoup-clj-server.connector.http :as http]
            [ssoup-clj-server.connector.websocket :as websocket]
            [clojure.core.async :as async :refer [<! <!! >! go]]
            [clojure.core.async.impl.protocols :refer [Channel]]
            [clojure.pprint :refer [pprint]]
            [ring.middleware.session.cookie :as cookie]
            [io.pedestal.http :as bootstrap]
            [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)]
            [ring.middleware.session.memory :as mem]))

; defprotocol ISessionFactory
(defmethod s/new-session* :ssoup/clj-engine [engine additional-attributes options]
  (s/new-session-fn* engine additional-attributes options))

(def session-cookie-name "SID")
(def session-store (cookie/cookie-store))                   ; session store

(defn server [port http-connector websocket-connector]
  (let [server (bootstrap/create-server
                 (-> {:env                          :dev
                      ::bootstrap/router            :linear-search
                      ::bootstrap/type              :jetty
                      ::bootstrap/port              port
                      ::bootstrap/join?             false
                      ::bootstrap/enable-session    {:cookie-name session-cookie-name
                                                     :store       session-store}
                      ::bootstrap/routes            (:routes http-connector)
                      ::bootstrap/secure-headers    nil
                      ::bootstrap/container-options {:context-configurator (:context-configurator websocket-connector)}}
                     (bootstrap/default-interceptors)))]
    {:start (fn [] (bootstrap/start server))
     :stop  (fn [] (bootstrap/stop server))}))
