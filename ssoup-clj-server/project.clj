(defproject ssoup-clj-server "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/core.cache "0.6.5"]
                 [org.clojure/core.memoize "0.5.8"]
                 [org.clojure/core.async "0.2.385"]
                 [com.taoensso/timbre "4.0.2"]
                 [ssoup-rdf "0.1.0-SNAPSHOT"]
                 ;[ring/ring "1.4.0"]
                 ;[ring/ring-json "0.4.0"]
                 ;[ring-transit "0.1.4"]
                 ;[compojure "1.4.0"]
                 [io.pedestal/pedestal.service       "0.5.2"]
                 [io.pedestal/pedestal.service-tools "0.5.2"]
                 [io.pedestal/pedestal.jetty         "0.5.2"]
                 [geheimtur "0.3.0"]
                 [enlive "1.1.5"]
                 [hiccup "1.0.5"]
                 ;[com.cemerick/friend "0.2.1"]
                 ;[org.marianoguerra/friend-json-workflow "0.2.1"]
                 [clj-time "0.9.0"]]
  :main ^:skip-aot ssoup-clj-server.core
  :source-paths ["src" "../ssoup-clj-core/src"]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :repositories [["releases" {:url "https://clojars.org/repo/"
							  :username :env/CLOJARS_USERNAME
                              :password :env/CLOJARS_PASSWORD}]])

