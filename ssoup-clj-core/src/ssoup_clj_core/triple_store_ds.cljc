(ns ^:figwheel-always ssoup-clj-core.triple-store-ds
  (:require
    [ssoup-clj-core.datasources :as ds]
    [ssoup-clj-core.pub-sub :as ps]
    [ssoup-rdf.core :as rdf]
    [clojure.core.async :as async :refer [<! chan >! put! close! pipeline]]
    #?(:clj
    [clojure.core.async :refer [go <!!]])
    [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)])
  #?(:cljs (:require-macros [cljs.core.async.macros :refer [go]])))

(derive :ssoup-ds/triple-store :ssoup/datasource)

(defn new-triple-store-datasource [ds-name tags triple-store get-object-options options]
  {:type               :ssoup-ds/triple-store
   :name               ds-name
   :data-tags          tags
   :publish-updates    (:publish-updates options)
   :cache              (atom {})                            ; clojure(script) objects cache
   :triple-store       triple-store
   :get-object-options get-object-options})

; -------------------------------------------------------
; IDataSource
; -------------------------------------------------------

(defmethod ds/init-ds :ssoup-ds/triple-store [ds ctx]
  ds)

(defmethod ds/get-model-rdf-context :ssoup-ds/triple-store [ds]
  (let [ts (:triple-store ds)]
    (rdf/get-model-rdf-context ts)))

(defmethod ds/get-object :ssoup-ds/triple-store [query-ds ctx object-id data-pattern options]
  (let [ts (:triple-store query-ds)
        rdf-ctx (rdf/get-model-rdf-context ts)
        options (merge (:get-object-options query-ds)
                       options)
        object (rdf/pull ts rdf-ctx object-id data-pattern options)
        object (if (keyword? object)
                 {:id          object-id
                  :rdf/context rdf-ctx}
                 (assoc object :rdf/context rdf-ctx))]
    (go object)))

(defmethod ds/list-objects-with-property :ssoup-ds/triple-store
  [ds ctx property-id property-value data-pattern options]
  (go
    (let [ts (:triple-store ds)
          rdf-ctx (rdf/get-model-rdf-context ts)
          object-ids (rdf/list-subjects-with-property ts rdf-ctx property-id
                                                      property-value)
          objects (loop [object-ids object-ids
                         objects []]
                    (if (empty? object-ids)
                      objects
                      (recur
                        (rest object-ids)
                        (conj objects
                              (<! (ds/get-object ds ctx (first object-ids) data-pattern options))))))]
      objects)))

(defmethod ds/save-object :ssoup-ds/triple-store [ds ctx rdf-ctx object options]
  (let [ts (:triple-store ds)
        rdf-ctx (or rdf-ctx (rdf/get-model-rdf-context ts))
        object-id (:id object)]
    (if-not object-id
      (if-let [graph (:graph object)]                     ; in case of a graph do save-object for each object in the graph
        (doall (map (fn [object-in-graph]
                      (let [object-rdf-context (:rdf/context object)
                            object-in-graph (if object-rdf-context
                                              (assoc object-in-graph :rdf/context object-rdf-context)
                                              object-in-graph)]
                        (ds/save-object ds ctx rdf-ctx object-in-graph options))) graph))
        (warn "Cannot save object: unknown id"))
      (let [data-format (or (:ssoup/data-format options) "JSON-LD")
            rdf-ctx (or (:rdf/context object) rdf-ctx)
            object (rdf/with-context object rdf-ctx)
            object-serialized (rdf/to-rdf rdf-ctx object data-format)
            data-pattern ["*" 1000]                         ; TODO: deduce data-pattern from data or schema.
            object-uri (rdf/to-rdf-expanded-iri rdf-ctx object-id)
            ;object-in-ts (ds/get-object ds ctx object-id data-pattern {})
            ]
        ; first remove the object from the triple store
        (rdf/delete-data-by-id ts rdf-ctx object-id)
        #_(when object-in-ts
            (let [object-in-ts-serialized (rdf/to-rdf rdf-ctx object-in-ts data-format)]
              (rdf/delete-data ts object-in-ts-serialized data-format)))
        ; then add the new version back
        (rdf/save-data ts object-uri object-serialized data-format)
        (when (:publish-updates ds)
          (let [engine (:engine ctx)]
            (ps/publish-object-update engine ctx (:data-tags ds) object-id data-pattern object)))))))

(defmethod ds/load-object-from-file :ssoup-ds/triple-store [ds ctx filename options]
  (let [ts (:triple-store ds)]
    (debug "Loading file " filename " into " (:name ds))
    (rdf/read-file ts filename (:ssoup/data-format options))))

(defmethod ds/get-schema :ssoup-ds/triple-store [schema-ds ctx options]
  (go
    (let [ts (:triple-store schema-ds)
          rdf-ctx (rdf/get-model-rdf-context ts)]
      {:rdf/context  rdf-ctx
       :ssoup/schema (rdf/get-schema ts rdf-ctx)})))

(defmethod ds/get-type-schema :ssoup-ds/triple-store [schema-ds ctx type options]
  (go
    (let [ts (:triple-store schema-ds)
          rdf-ctx (rdf/get-model-rdf-context ts)]
      {:rdf/context  rdf-ctx
       :ssoup/schema (rdf/get-declared-properties ts rdf-ctx type
                                                  (:direct options))})))

(defmethod ds/get-super-types :ssoup-ds/triple-store [schema-ds ctx type options]
  (go
    (let [ts (:triple-store schema-ds)
          rdf-ctx (rdf/get-model-rdf-context ts)
          st (rdf/get-super-types ts rdf-ctx type)]
      st)))
