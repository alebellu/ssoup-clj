(ns ssoup-clj-core.connector
  (:require
    [ssoup-clj-core.types :refer [dispatch-on-type]]
    [clojure.core.async :as async :refer [<! chan >! put! close! pipeline]]
    #?(:clj
    [clojure.core.async :refer [go <! <!!]])
    [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)])
  #?(:cljs (:require-macros [cljs.core.async.macros :refer [go]])))

; --------------------------------------------------------------------------------------
; The Connector protocol describes the interactions between connected SSOUP engines.
; Connectors are split in two parts : the connector client and the connector server.
; Connector clients and connector servers share most of the interface, but:
; - only connector clients know how send messages
; - only connector servers know how to handle received messages.
; --------------------------------------------------------------------------------------

; defprotocol IConnector
(defmulti init #_[connector ctx]
          "Initialize the connector." dispatch-on-type)
(defmulti tear-down #_[connector ctx]
          "Tear down the connector." dispatch-on-type)
(defmulti get-current-user #_[connector ctx options]
          "Returns the name of the current logged in user." dispatch-on-type)
(defmulti login #_[connector ctx user password options]
          "Logs the specified user in. Only one logged-in user per connection client is supported."
          dispatch-on-type)
(defmulti logout #_[connector ctx options]
          "Logs out the current user."
          dispatch-on-type)
(defmulti connection-open? #_[connector destination]
          "Checks whether the connection towards the given destination is open or not."
          dispatch-on-type)
(defmulti exec #_[connector ctx message options]
          "Executes a service on the connected engine." dispatch-on-type)
(defmulti subscribe #_[connector ctx topic-id]
          "Subscribes to a topic on the remote engine" dispatch-on-type)
(defmulti subscribe-to-object-updates #_[connector ctx data-tags object-id data-pattern options]
          "Subscribe to updates on the specified object for the given data pattern on the remote engine" dispatch-on-type)
(defmulti subscribe-to-schema-updates #_[connector ctx data-tags options]
          "Subscribe to updates on schema information on the remote engine" dispatch-on-type)
(defmulti topic-on-message #_[connector ctx topic-id message]
          "Called when a new message is coming relative to a subscribed topic"
          dispatch-on-type)
(defmulti on-object-update #_[connector ctx data-tags object-id data-pattern object]
          "Called when an object update is received"
          dispatch-on-type)
(defmulti on-schema-update #_[connector ctx data-tags schema-update]
          "Called when an update on the schema is received"
          dispatch-on-type)
(defmulti dispatch-message #_[connector-server ctx msg-type msg-body]
          "Dispatch a message to the proper handler method" dispatch-on-type)
(defmulti send-message #_[connector ctx destination msg-type msg-body headers]
          "Sends a message to the specified (or the default) destination.
           Does not wait for a response.
           Destination identifies where the message should be sent.
           Returns a channel which resolves to true if the message was sent (and received by the destination), false otherwise."
          dispatch-on-type)
(defmulti send-or-enqueue-message #_[connector ctx destination msg-type msg-body headers]
          "Sends a message or equeue it if the connection to the destination is currently not open" dispatch-on-type)
(defmulti resend-enqueued-messages #_[connector ctx destination]
          "Tries to re send all the previously enqueued messages.
           Messages are removed from the queue only when successfully sent."
          dispatch-on-type)
(defmulti send-message-and-wait-for-response #_[connector ctx destination msg-type msg-body headers]
          "Sends a message to the specified (or the default) destination.
           Waits for the response and returns it.
           If the connection is down, according to the connector policy, resends may be tried.
           Returns a channel where the response will be written to."
          dispatch-on-type)

; -------------------------------------------------------
; Some default implementions
; -------------------------------------------------------

(defn new-connector []
  {:message-queues #?(:clj (ref {}))
   #?(:cljs (atom {}))})

; TODO: put an upper bound to message queues and return false if the queue is full
(defmethod send-or-enqueue-message :ssoup/connector
  [connector ctx destination msg-type msg-body headers]
  (let [destination (or destination (:default-destination connector))]
    (go
      (debug "Trying to send message " msg-type " to destination " destination)
      (when-not (<! (send-message connector ctx destination msg-type msg-body headers))
        (let [msg {:msg-type msg-type
                   :msg-body msg-body
                   :headers  headers}]
          (debug "Message " msg-type " could not be sent. Enqueueing it.")
          #?(:clj
             (let [message-queues-ref (:message-queues connector)
                   ; atomically get and possibly initilize the message queue for the specific destination
                   message-queue-ref (dosync
                                       (let [message-queue-ref (get @message-queues-ref destination)]
                                         (if message-queue-ref
                                           message-queues-ref
                                           (let [message-queue-ref (ref [])]
                                             (alter message-queues-ref assoc destination message-queue-ref)
                                             message-queue-ref))))]
               ; atomically update the messages in the queue
               (dosync
                 (alter message-queue-ref conj msg))))
          #?(:cljs
             (let [message-queues-atom (:message-queues connector)
                   message-queue (or (get @message-queues-atom destination) [])
                   message-queue (conj message-queue msg)]
               (swap! message-queues-atom assoc destination message-queue))))))))

(defmethod resend-enqueued-messages :ssoup/connector
  [connector ctx destination]
  (let [destination (or destination (:default-destination connector))
        message-queues @(:message-queues connector)]
    (debug "Resender: resending messages to destination " destination)
    (when (contains? message-queues destination)
      (go
        (let [send-all (fn [message-queue]
                         (go
                           (loop [message-queue message-queue]
                             (when-let [{msg-type :msg-type
                                         msg-body :msg-body
                                         headers  :headers} (first message-queue)]
                               (debug "Resender: Sending message of type " msg-type)
                               (if (<! (send-message connector ctx destination msg-type msg-body headers))
                                 (recur (rest message-queue))
                                 ; as soon as one of the resends fails leave the current and subsequent messages in the queue
                                 message-queue)))))]
          #?(:clj
             (let [message-queue-ref (get message-queues destination)]
               ; atomically get the messages in the queue,try to resend them, and reset the queue
               ; to only contain the values of the messages unsent.
               (dosync
                 (let [message-queue @message-queue-ref
                       remaining-message-queue (<!! (send-all message-queue))]
                   (ref-set message-queue-ref (or remaining-message-queue []))))))
          #?(:cljs
             (let [message-queue (get message-queues destination)
                   remaining-message-queue (<! (send-all message-queue))]
               (swap! message-queues assoc destination (or remaining-message-queue [])))))))))
