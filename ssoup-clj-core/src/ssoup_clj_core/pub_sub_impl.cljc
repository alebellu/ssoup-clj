(ns ssoup-clj-core.pub-sub-impl
  (:require
    [clojure.core.async :as async :refer [sub pub <! chan close! >! put! mult tap]]
    #?(:clj
    [clojure.core.async :refer [go go-loop]])
    [ssoup-clj-core.types :refer [dispatch-on-type]]
    [ssoup-clj-core.core :as s]
    [ssoup-clj-core.pub-sub :as ps]
    [ssoup-clj-core.connector :as connector]
    [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)])
  #?(:cljs (:require-macros [cljs.core.async.macros :refer [go go-loop]])))

; --------------------------------------------------------------------------------------
; Here is provided an implementation of a system handle local publications and subscriptions and
; also keep track or remotely connected clients via connectors.
; --------------------------------------------------------------------------------------

; a publication channel for local pub-sub
(def pub-chan (chan))
(def publication (pub pub-chan (fn [msg]
                                 (-> msg :header :topic-id))))

; -------------------------------------------------------
; IPublishSubscribe
; -------------------------------------------------------

(defmethod ps/subscribe :ssoup/engine [engine session ctx topic-id options]
  (let [sub-chan (chan)
        multc (mult sub-chan)
        channel (:channel options)]
    (when channel
      (tap multc channel))
    (when-let [on-update (:on-update options)]
      (let [on-update-channel (chan)]
        (tap multc on-update-channel)
        (go-loop []
          (let [message (<! on-update-channel)]
            (on-update message)
            (recur)))))
    (debug "Subscribing to " topic-id)
    (sub publication topic-id sub-chan)))

(defmethod ps/publish :ssoup/engine [engine ctx topic-id data]
  (debug "Publishing data to " topic-id)
  (put! pub-chan {:header {:topic-id topic-id}
                  :body   data}))

; -------------------------------------------------------
; IPublishSubscribeObjects
; -------------------------------------------------------

(defn get-topic-id-for-object-updates
  [data-tags object-id data-pattern]
  (str data-tags ":" object-id ":" data-pattern))

(defmethod ps/subscribe-to-object-updates :ssoup/engine
  [engine session ctx data-tags object-id data-pattern options]
  (let [topic-id (get-topic-id-for-object-updates data-tags object-id data-pattern)
        send-initial-update (:send-initial-update options)]
    (ps/subscribe engine session ctx topic-id
                  {:on-update (fn [msg]
                                (let [object (:body msg)]
                                  (when-let [on-object-update (:on-object-update options)]
                                    (on-object-update object))
                                  (when-let [channel (:update-channel options)]
                                    (put! channel object))))})
    (when send-initial-update
      (go
        (let [object (<! (s/get-object engine session ctx data-tags object-id data-pattern options))]
          (ps/publish-object-update engine ctx data-tags object-id data-pattern object))))))

(defmethod ps/publish-object-update :ssoup/engine
  [engine ctx data-tags object-id data-pattern object]
  (let [topic-id (get-topic-id-for-object-updates data-tags object-id data-pattern)]
    (ps/publish engine ctx topic-id object)))

; -------------------------------------------------------
; IPublishSubscribeSchema
; -------------------------------------------------------

(defn get-topic-id-for-schema-updates
  [data-tags]
  (str data-tags ":schema"))

(defmethod ps/subscribe-to-schema-updates :ssoup/engine
  [engine session ctx data-tags options]
  (let [topic-id (get-topic-id-for-schema-updates data-tags)
        send-initial-update (:send-initial-update options)]
    (ps/subscribe engine session ctx topic-id
                  {:on-update (fn [msg]
                                (let [schema-update (:body msg)]
                                  (when-let [on-schema-update (:on-schema-update options)]
                                    (debug "on schema update")
                                    (on-schema-update schema-update))
                                  (when-let [channel (:update-channel options)]
                                    (put! channel schema-update))))})
    (when send-initial-update
      (go
        (let [schema (<! (s/get-schema engine session ctx data-tags options))]
          (ps/publish-schema-update engine ctx data-tags schema))))))

(defmethod ps/publish-object-update :ssoup/engine
  [engine ctx data-tags object-id data-pattern object]
  (let [topic-id (get-topic-id-for-object-updates data-tags object-id data-pattern)]
    (ps/publish engine ctx topic-id object)))

(defmethod ps/publish-schema-update :ssoup/engine
  [engine ctx data-tags schema-updates]
  (let [topic-id (get-topic-id-for-schema-updates data-tags)]
    (ps/publish engine ctx topic-id schema-updates)))
