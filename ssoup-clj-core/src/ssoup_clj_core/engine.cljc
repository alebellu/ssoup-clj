(ns ssoup-clj-core.engine
  (:require
    [clojure.core.async :as async :refer [<! chan >! put! close! pipeline]]
    [ssoup-clj-core.datasources :as ds]
    #?(:clj
    [clojure.core.async :refer [go <!!]])
    [ssoup-clj-core.core :as s]
    [ssoup-clj-core.types :as types]
    [ssoup-clj-core.base-services]
    [ssoup-clj-core.utils :refer [retval]]
    [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)])
  #?(:cljs (:require-macros [cljs.core.async.macros :refer [go]])))

(defn- init-type-system* [engine]
  (let [h (-> (get engine :types)
              (types/add-xsd-type-hierarchy)
              (derive :ssoup/keyword :rdfs/Literal)
              (derive :ssoup/array :rdfs/Resource)
              (derive :ssoup/call :rdfs/Resource)
              (derive :ssoup/script :rdfs/Resource)
              (derive :ssoup/from-node :rdfs/Resource)
              (derive :ssoup/from-user-attribute :rdfs/Resource)
              (derive :ssoup/from-script-argument :rdfs/Resource)
              (derive :ssoup/anonymous-user :ssoup/any-user)

              ; consider any rdf literal or resource also as a set
              ; (of one element of the same type) :
              ; o <==> #{o}
              ; this is useful for dispatching to methods that have parameters
              ; of type :ssoup/set : when fetching from a triple store
              ; a single object for a property could be converted to either a
              ; clojure keyword or a clojure set
              (derive :rdfs/Literal :ssoup/set)
              (derive :rdfs/Resource :ssoup/set))
        h-ref (:h-ref engine)]
    (reset! h-ref h)

    #_(s/init-get-candidates h-ref)

    (assoc engine :types h
                  :h-ref h-ref)))

(defn- register-library-services* [engine library-spec]
  (let [service-specs (:ssoup/services library-spec)
        size (count service-specs)]
    (go (loop [index 0
               engine engine]
          (if (< index size)
            (let [service-spec (get service-specs index)]
              (recur (inc index)
                     (<! (s/register-service* engine
                                              (assoc service-spec
                                                :library-id (:library-id library-spec))))))
            engine)))))

(defn- sort-libraries
  "Sort libraries in dependencies order"
  [libraries]
  (reverse
    (sort (comparator
            (fn [x y]
              (let [depy (or (:dependencies y) [])]
                (contains? depy x))))
          libraries)))

(defn- init-libraries* [engine]
  (let [engine-libraries @s/libraries
        library-ids (:ssoup/libraries engine)
        libraries (map (fn [library-id]
                         (get engine-libraries library-id))
                       library-ids)
        libraries (sort-libraries libraries)]
    (debug "Libraries initialization order " (doall (map :library-id libraries)))
    (go (loop [libraries libraries
               engine engine]
          (if (not-empty libraries)
            (let [library-spec (first libraries)
                  library-id (:library-id library-spec)
                  engine-type (:type engine)]
              (if (isa? engine-type (:engine-type library-spec))
                (let [init-library-fn (:init-library-fn library-spec)]
                  (info "Initializing library" library-id)
                  (when init-library-fn
                    (apply init-library-fn []))
                  (recur (rest libraries)
                         (<! (register-library-services* engine library-spec))))
                (do
                  (warn "Library" library-id "cannot be used with engine of type"
                        engine-type ". It requires an engine with type "
                        (:engine-type library-spec))
                  (recur (rest libraries) engine))))
            engine)))))

(defn init-engine-fn* [engine]
  (go (let [libraries (or (:ssoup/libraries engine) #{})
            libraries (conj libraries :ssoup/base-services)
            engine (-> engine
                       (assoc :ssoup/libraries libraries)   ; all engines should expose the base libraries
                       (init-type-system*))
            engine (<! (init-libraries* engine))]
        engine)))

(defmethod s/init-engine* :ssoup/engine [engine]
  (init-engine-fn* engine))

(defn tear-down-fn* [engine]
  (when-let [dss (:dss engine)]
    (map #(ds/tear-down %) dss))
  (dissoc engine :dss))

(defmethod s/tear-down* :ssoup/engine [engine]
  (tear-down-fn* engine))

(defn new-engine [name default-context options]
  {:type             :ssoup/engine
   :name             name
   :default-context  default-context
   :h-ref            (atom nil)
   :remote-engines   []
   :datasources      {}
   :types            (make-hierarchy)
   :scripts          {}
   :author-scs-rules {}
   :classes-keys     {}
   :sessions         {}
   :permission-cache (atom {})
   :options          options})
