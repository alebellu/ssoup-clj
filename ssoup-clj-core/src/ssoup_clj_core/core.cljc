(ns ^:figwheel-always ssoup-clj-core.core
  (:require
    [ssoup-clj-core.datasources :as ds]
    [ssoup-clj-core.connector :as connector]
    [ssoup-rdf.core :as rdf]
    [ssoup-clj-core.types :refer [ssoup-cast get-arg-type-keys get-all-arg-type-keys-combinations get-type
                                  atomic? dispatch-on-type derive*]]
    [ssoup-clj-core.utils :refer [as-set channel? if-err-throw exception retval gretval
                                  str-pprint]]
    #?(:clj
    [ssoup-clj-core.utils :refer [assert-args]])
    [clojure.math.combinatorics :as combo]
    [clojure.core.async :as async :refer [<! chan >! put! close! pipeline pipeline-async]]
    #?(:clj
    [clojure.core.async :refer [go <!!]])
    [clojure.core.async.impl.protocols :refer [Channel]]
    #?(:clj
    [clj-time.core :as t])
    #?(:cljs [cljs-time.core :as t])
    [clojure.set]
    [ssoup-clj-core.uuid :as uuid]
    [taoensso.timbre :as timbre :refer [log trace debug info warn error fatal]]
    [ssoup-clj-core.utils :as utils])
  #?(:cljs (:require-macros [cljs.core.async.macros :refer [go]])))

; -------------------------------------------------------
; Anonymous user
; -------------------------------------------------------
(def anon-user {:roles [:ssoup/anonymous-user]})

; -------------------------------------------------------
; Engine protocols
; -------------------------------------------------------

; defprotocol IEngine
(defmulti init-engine* #_[engine]
          "Initialize the engine" dispatch-on-type)
(defmulti tear-down* #_[engine]
          "Tear down the engine" dispatch-on-type)

; defprotocol IWithDataSources
(defmulti register-data-source* #_[engine ds]
          "Register a datasource" dispatch-on-type)
(defmulti get-object #_[engine session ctx data-tags object-id data-pattern options]
          "Retrieve the object with the given id from one or more data sources."
          dispatch-on-type)
(defmulti get-object-from-datasources #_[engine session ctx query-dss object-id data-pattern options]
          "Retrieve the object with the given id from one or more data sources."
          dispatch-on-type)
(defmulti list-objects-with-property
          #_[engine session ctx data-tags property-id property-value data-pattern
             options]
          "Retrieve the list of objects with the given value for the property specified
           from one or more data sources."
          dispatch-on-type)
(defmulti list-objects-with-property-from-datasources
          #_[engine ctx query-dss property-id property-value data-pattern options]
          "Retrieve the list of objects with the given value for the property specified
          from one or more data sources."
          dispatch-on-type)
(defmulti get-schema #_[engine session ctx data-tags options]
          "Retrieve the schema info from one or more data sources."
          dispatch-on-type)
(defmulti get-schema-from-datasources #_[engine ctx schema-dss options]
          "Retrieve the schema info from one or more data sources."
          dispatch-on-type)
(defmulti get-type-schema #_[engine session ctx data-tags type options]
          "Retrieve the schema of the given type from one or more data sources."
          dispatch-on-type)
(defmulti get-type-schema-from-datasources #_[engine ctx schema-dss object-id options]
          "Retrieve the schema of the given type from one or more data sources."
          dispatch-on-type)
(defmulti get-types-schema #_[engine session ctx data-tags types options]
          "Retrieve the schema of the given types from one or more data sources."
          dispatch-on-type)
(defmulti get-super-types #_[engine session ctx data-tags type options]
          "Retrieve the super types of the given type from one or more data sources."
          dispatch-on-type)
(defmulti get-super-types-from-datasources #_[engine ctx schema-dss object-id options]
          "Retrieve the super types of the given type from one or more data sources."
          dispatch-on-type)
(defmulti save-object #_[engine session ctx data-tags object options]
          "Save the given object to the data sources with the specified tags."
          dispatch-on-type)
(defmulti sync-datasource #_[engine session ctx ds-name objects-to-update objects-to-fetch options]
          "Synchronize the given datasource with the data provided and returns the currect version
           of the requested objects."
          dispatch-on-type)

; defprotocol IWithRemoteEngines
(defmulti register-remote-engine* #_[engine remote-engine]
          "Registers a remote engine" dispatch-on-type)
(defmulti get-remote-engine-by-id #_[engine remote-engine-id]
          "Returns a remote engine given the remote engine id" dispatch-on-type)

; defprotocol IWithServerConnectors
(defmulti register-server-connector* #_[engine connector]
          "Registers a server connector" dispatch-on-type)
(defmulti get-server-connector-by-id #_[engine connector-id]
          "Returns a  server connector given its id" dispatch-on-type)

; defprotocol IWithServices
(defmulti register-service* #_[engine fndef]
          "Registers a new service.
          - (:classes fndef):
          these are the classes used to characterise the service and they are used
          as a discriminator during service call: the called service will have at least all the
          requested classes.
          To do so when registering a service, it is registered for the given classes as well as
          for all the supersets. There should be no hierarchy between the classes keys, the
          recursive registration takes care of the hierarchy: the problem when adding a hierarchy
          is that a call to a more specific service could result in the dispatch to a more generic
          one, which should not happen.
          AS AN ALTERNATIVE WE COULD HAVE:
          + defined a isa relashionship of class keys, reversed with respect to the natural
          isa: on dispatch services with more specific classes can be called, but services with
          more generic classes cannot, which is the opposite logic than for parameter types
          + ignore the isa rel in classes if there is one (because it would have the wrong
          direction) -> use additional classes instead of isa to obtain the same effect.
          The problem with this alternative solution is that in case of multiple services for the
          same dispatch value we have no opportunity di disambiguate, all decisions are in the
          hands of the clojure multifn dispatch mechanism."
          dispatch-on-type)

; defprotocol IWithPermissions
(defmulti register-permission* #_[engine permission]
          "Adds an entry in the permissions map.
          Example of permission map:
          fn-name ->
          [{ssoup/fname f1
            ssoup/users #{user01 user02}
            ssoup/roles #{role01 role02}
            ssoup/args-mapping-fn get-types
            ssoup/mapped-args {arg1:t1,arg2:t2,arg3:t3,arg4:t4}}]"
          dispatch-on-type)
(defmulti get-permissions #_[engine object-id]
          "Returns the permissions registered in the engine for a given object id."
          dispatch-on-type)
(defmulti library-use-permitted? #_[engine user library-id]
          "Checks whether the use of a library by the given user is allowed or not.
          If use is allowed, then any service in the library can be called by the given user."
          dispatch-on-type)
(defmulti service-call-permitted? #_[engine user fndef args]
          "Checks whether a call to a service with some specified arguments
          by the given user is allowed or not."
          dispatch-on-type)

; defprotocol IWithScripts
(defmulti register-script* #_[engine options]
          "Registers a new script" dispatch-on-type)
(defmulti get-script #_[this script-id]
          "Returns the scrivpt with the given id" dispatch-on-type)

; defprotocol IServiceRuntime
(defmulti find-best-candidate #_[engine user classes flavor args scs-rules]
          "Find ( and possibly cache ) the best candidate service that:
          - the specified user is allowed to execute
          - has the given class set (or sub-classes)
          - has the given flavor (or a sub-flavor)
          - is compatible with the types of the arguments specified
          Preference among remaining candidates is given according to the scs rules specified."
          dispatch-on-type)
(defmulti exec-async #_[engine session options]
          "Executes a ssoup service asynchronously.
           An array of arguments or a channel resolving to an array of arguments is expected as input.
           Single arguments can be actual values or channels.
           Returns a channel." dispatch-on-type)
(defmulti exec-async-remote #_[remote-engine-id session options]
          "Executes a ssoup service asynchronously on a remote engine.
           Returns a channel." dispatch-on-type)

; defprotocol IScriptRuntime
(defmulti exec-script #_[engine session ctx script script-arguments options]
          "Executes a script" dispatch-on-type)
(defmulti set-var* #_[engine session var value]
          "Sets the value of a variable" dispatch-on-type)

; defprotocol ISessionFactory
(defmulti new-session* #_[engine data options]
          "Creates a new session" dispatch-on-type)

; defprotocol IWithSessions
(defmulti get-session #_[engine session-id]
          "Returns the session with the given id" dispatch-on-type)
(defmulti set-session* #_[engine session]
          "Register a session object in the engine" dispatch-on-type)

; -------------------------------------------------------
; Session related protocols
; -------------------------------------------------------

; defprotocol ISession
(defmulti init-session #_[engine session]
          "Initialize the session" dispatch-on-type)

; defprotocol IWithUser
(defmulti set-current-user* #_[engine session rrepl]
          "Sets the current user" dispatch-on-type)
(defmulti get-current-user #_[session]
          "Returns the current user" dispatch-on-type)

; defprotocol IWithFlavor
(defmulti set-current-flavor #_[engine session flavor]
          "Sets the current flavor" dispatch-on-type)
(defmulti get-current-flavor #_[engine session]
          "Returns the current flavor")

; defprotocol IWithAskContexts
(defmulti new-ask-context #_[engine session options]
          "Adds a new ask context with the given options to the session"
          dispatch-on-type)
(defmulti get-ask-context #_[engine session ask-ctx-id]
          "Gets the ask context with the given id" dispatch-on-type)
(defmulti delete-ask-context #_[engine session ask-ctx]
          "Deletes the given ask context from the session" dispatch-on-type)

; defprotocol IWithNotifications
(defmulti on-notification #_[engine notification]
          "Called when a new notification arrives from a running or connected entity."
          dispatch-on-type)

; -------------------------------------------------------
; Service libraries related functions and protocols.
; -------------------------------------------------------
(def libraries (atom {}))                                   ; global atom storing all available libraries specs

(defn register-library! [library-spec]
  "A library included in the classpath or in the CLJS codebase
  should register its presence automatically by calling register-library!
  in the global scope.
  library-spec should be a map with at least the following keys:
  :library-id should be a unique identifier for the library.
  :engine-type the type of engine for which the library is designed to work with.
  :init-library-fn should be a function that does
  any needed initializations and return a seq of service definitions
  for the services implemented by the library."
  (let [library-id (:library-id library-spec)]
    (swap! libraries assoc library-id library-spec)
    (info "Library" library-id "specs registered.")))

#_(defn init-get-candidates [h-ref])

; forward private declarations
(defn- get-classes-key* [engine classes generate-if-not-found])
(def get-candidates #_[engine session classes flavor args])

; -------------------------------------------------------
; IWithDatasources
; -------------------------------------------------------
(defmethod register-data-source* :ssoup/engine [engine ds]
  (debug "Registering datasource " (:name ds))
  (assoc-in engine [:dss (:name ds)] ds))

(defmethod get-object-from-datasources :ssoup/engine
  [engine session ctx query-dss object-id data-pattern options]
  "Try one datasource at a time and stop at the first one returning some data"
  (go
    (loop [query-dss query-dss]
      (when-let [query-ds (first query-dss)]
        (if-let [object (<! (ds/get-object query-ds {:engine engine :session session}
                                           object-id data-pattern options))]
          object
          (recur (rest query-dss)))))))

(defmethod get-object :ssoup/engine [engine session ctx data-tags object-id
                                     data-pattern options]
  (let [dss (:dss engine)
        query-dss (filter (fn [ds] (not-empty (clojure.set/intersection (:data-tags ds) data-tags))) (vals dss))
        data-chan (if-not (empty? query-dss)
                    (get-object-from-datasources engine session ctx
                                                 query-dss
                                                 object-id
                                                 data-pattern
                                                 options)
                    (go))]
    data-chan))

(defmethod list-objects-with-property-from-datasources :ssoup/engine
  [engine ctx query-dss property-id property-value data-pattern options]
  "Try one datasource at a time and stop at the first one returning some data"
  (go
    (loop [query-dss query-dss]
      (when-let [query-ds (first query-dss)]
        (if-let [objects (<! (ds/list-objects-with-property query-ds ctx property-id property-value data-pattern options))]
          objects
          (recur (rest query-dss)))))))

(defmethod list-objects-with-property :ssoup/engine
  [engine session ctx data-tags property-id property-value data-pattern options]
  (let [dss (:dss engine)
        query-dss (filter (fn [ds] (not-empty (clojure.set/intersection (:data-tags ds) data-tags))) (vals dss))
        data-chan (if-not (empty? query-dss)
                    (list-objects-with-property-from-datasources
                      engine ctx
                      query-dss
                      property-id
                      property-value
                      data-pattern
                      options)
                    (go))]
    data-chan))

(defmethod get-schema-from-datasources :ssoup/engine
  [engine ctx schema-dss options]
  "Try one datasource at a time and stop at the first one returning some data"
  (go
    (loop [schema-dss schema-dss]
      (if-not (empty? schema-dss)
        (let [schema-ds (first schema-dss)
              type-schema (<! (ds/get-schema schema-ds ctx options))]
          (or type-schema (recur (rest schema-dss))))
        {}))))

(defn- get-schema* [engine session ctx data-tags options]
  (go
    (let [dss (:dss engine)
          query-dss (filter (fn [ds] (not-empty (clojure.set/intersection (:data-tags ds) data-tags))) (vals dss))
          schema (when-not (empty? query-dss)
                   (<! (get-schema-from-datasources engine ctx
                                                    query-dss
                                                    options)))]
      schema)))

(defmethod get-schema :ssoup/engine [engine session ctx data-tags options]
  (get-schema* engine session ctx data-tags options))

(defmethod get-type-schema-from-datasources :ssoup/engine
  [engine ctx schema-dss type options]
  "Try one datasource at a time and stop at the first one returning some data"
  (go
    (loop [schema-dss schema-dss]
      (if-not (empty? schema-dss)
        (let [schema-ds (first schema-dss)
              type-schema (<! (ds/get-type-schema schema-ds ctx type options))]
          (or type-schema (recur (rest schema-dss))))
        []))))

(defn- get-type-schema* [engine session ctx data-tags type options]
  (go
    (if (atomic? engine type)
      []                                                    ; xsd atomic types are no further decomposable
      (let [dss (:dss engine)
            query-dss (filter (fn [ds] (not-empty (clojure.set/intersection (:data-tags ds) data-tags))) (vals dss))
            data (when-not (empty? query-dss)
                   (<! (get-type-schema-from-datasources engine ctx
                                                         query-dss
                                                         type
                                                         options)))]
        data))))

(defmethod get-type-schema :ssoup/engine [engine session ctx data-tags type options]
  (get-type-schema* engine session ctx data-tags type options))

(defn- get-types-schema* [engine session ctx data-tags types options]
  (go
    (loop [types types
           schemas {}]
      (if (empty? types)
        schemas
        (let [type (first types)
              schema (gretval engine (<! (get-type-schema engine session ctx data-tags type options)))]
          (recur (rest types)
                 (assoc schemas type schema)))))))

(defmethod get-types-schema :ssoup/engine [engine session ctx data-tags types options]
  (get-types-schema* engine session ctx data-tags types options))

(defmethod get-super-types-from-datasources :ssoup/engine
  [engine ctx schema-dss type options]
  "Try one datasource at a time and stop at the first one returning some data"
  (go
    (loop [schema-dss schema-dss]
      (if-not (empty? schema-dss)
        (let [schema-ds (first schema-dss)
              super-types (<! (ds/get-super-types schema-ds ctx type options))]
          (or super-types (recur (rest schema-dss))))
        []))))

(defn- get-super-types* [engine session ctx data-tags type options]
  (go
    (if (atomic? engine type)
      []                                                    ; do not look for supertypes of xsd atomic types
      (let [dss (:dss engine)
            query-dss (filter (fn [ds] (not-empty (clojure.set/intersection (:data-tags ds) data-tags))) (vals dss))
            data (when-not (empty? query-dss)
                   (<! (get-super-types-from-datasources engine ctx
                                                         query-dss
                                                         type
                                                         options)))]
        data))))

(defmethod get-super-types :ssoup/engine [engine session ctx data-tags
                                          type options]
  (get-super-types* engine session ctx data-tags type options))

(defmethod save-object :ssoup/engine [engine session ctx data-tags object options]
  (let [dss (:dss engine)
        dest-dss (filter (fn [ds] (not-empty (clojure.set/intersection (:data-tags ds)
                                                                       data-tags))) (vals dss))
        rdf-ctx (:rdf/context options)]
    (if-not (empty? dest-dss)
      (go                                                   ; try to call the hook before-save
        (let [object (or (gretval engine
                                  (<! (exec-async engine session
                                                  {:ssoup/user    (:user session)
                                                   :ssoup/classes #{:ssoup/before-save}
                                                   :ssoup/flavor  (:ssoup/flavor options)
                                                   :ssoup/args    {:ssoup/object object}})))
                         object)]
          (doall (map (fn [ds]
                        (ds/save-object ds ctx rdf-ctx object options))
                      dest-dss)))))))

(defmethod sync-datasource :ssoup/engine [engine session ctx datasource-name objects-to-update objects-to-fetch options]
  (let [dss (:dss engine)
        dest-ds (get dss datasource-name)
        rdf-ctx (:rdf/context options)]
    (if dest-ds
      (ds/sync-ds dest-ds {:engine engine :session session}
                  objects-to-update
                  objects-to-fetch
                  options))))

(defn- fix-type-hierarchy* [engine session type-keys]
  "Ensure that the type keywords extend the super types keywords or rdfs:Resource in case
  they should"
  (go
    (loop [engine engine
           type-keys type-keys]
      (if (not-empty type-keys)
        (let [t (first type-keys)
              h (get engine :types)]
          (if (and (keyword? t)
                   (not (atomic? engine t))
                   (not= t :rdfs/Resource)
                   (not= t :rdfs/Literal)
                   (not= t :ssoup/set)
                   (not= t :ssoup/link)
                   (not= t :ssoup/folder)
                   (not (parents h t)))
            (let [super-types (<! (get-super-types engine session nil #{:ssoup/schema} t {}))
                  engine (if (not-empty super-types)
                           (let [engine (<! (fix-type-hierarchy* engine session
                                                                 super-types))
                                 engine (reduce (fn [engine super-type]
                                                  (derive* engine t super-type))
                                                engine super-types)]
                             engine)
                           (derive* engine t :rdfs/Resource))]
              (recur engine (rest type-keys)))
            (recur engine (rest type-keys))))
        engine))))

; -------------------------------------------------------
; IWithRemoteEngines
; -------------------------------------------------------

(defmethod register-remote-engine* :ssoup/engine
  [engine
   {remote-engine-id :id
    :as              remote-engine-def}]
  (-> engine
      (assoc :remote-engines (conj (:remote-engines engine) remote-engine-def))
      (assoc-in [:remote-engines-by-id remote-engine-id] remote-engine-def)))

(defmethod get-remote-engine-by-id :ssoup/engine
  [engine remote-engine-id]
  (get-in engine [:remote-engines-by-id remote-engine-id]))

; -------------------------------------------------------
; IWithServerConnectors
; -------------------------------------------------------

(defmethod register-server-connector* :ssoup/engine
  [engine server-connector]
  (-> engine
      (assoc-in [:server-connectors (:id server-connector)] server-connector)))

(defmethod get-server-connector-by-id :ssoup/engine
  [engine connector-id]
  (get-in engine [:server-connectors connector-id]))

; -------------------------------------------------------
; IWithServices
; -------------------------------------------------------
(defmethod register-service* :ssoup/engine
  ; to select best candidates for given user, we keep a map of services keyed by classes-key,
  ; which are put in is-a hierarchy: then postfiltering will check for arg-types and apply permissions and scs-rules
  ([engine fndef]
    (register-service* engine fndef (:classes fndef)))
  ([engine
    {fname     :fname
     ; classes :classes
     level     :level                                       ; :ssoup-engine, :usernormal, :author-normal, :author-important, :user-important
     handler   :handler
     user-role :user-role
     flavor    :flavor
     arg-types :arg-types
     :as       fndef}
    classes]
    (when (nil? classes)
      (if-err-throw ":classes must be specified"))
    (when (nil? fname)
      (if-err-throw ":fname must be specified"))
    (go (let [fns (:fns engine)]
          (let [arg-type-list (vals (into (sorted-map) arg-types)) ; seq of arg types,ordered by arg name
                ; ensure arg-types are properly mapped to clojure
                engine (<! (fix-type-hierarchy* engine nil arg-type-list))
                {engine :engine classes-key :return-value} (get-classes-key* engine classes
                                                                             true false)
                ; dispatch-value (apply vector classes-key flavor arg-type-list)
                fndefs (get fns classes-key)
                engine (if fndefs
                         (if (some #(= fname (:fname %)) fndefs)
                           (do (debug fname " was already added with classes : " classes)
                               engine)
                           (do (debug "Adding " fname " with classes : " classes)
                               (assoc-in engine [:fns classes-key] (conj fndefs fndef))))
                         (let [_ (debug "Registering service " fname " with classes: " classes)
                               fndefs #{fndef}
                               engine (assoc-in engine [:fns classes-key] fndefs)]
                           engine))]
            (if (<= (count classes) 1)
              engine
              ; register the same service for all subsets of classes recursively
              (loop [engine engine
                     classes-subsets (combo/combinations (seq classes)
                                                         (dec (count classes)))]
                (if (not-empty classes-subsets)
                  (recur (<! (register-service* engine
                                                fndef
                                                (into #{} (first classes-subsets))))
                         (rest classes-subsets))
                  engine))))))))

; -------------------------------------------------------
; IWithPermissions
; -------------------------------------------------------
(defmethod register-permission* :ssoup/engine
  [engine {object-id :ssoup/object-id :as permission}]
  ; TODO: save the permissions to the :ssoup/permissions datasource
  )

(defmethod get-permissions :ssoup/engine [engine object-id]
  (go
    (let [permission-cache (:permission-cache engine)
          object-permissions (get @permission-cache object-id)]
      (if object-permissions
        object-permissions
        (let [object-permissions (<! (list-objects-with-property engine nil nil
                                                                 #{:ssoup/permissions}
                                                                 :ssoup/object-id
                                                                 object-id
                                                                 ["*" 1000]
                                                                 {}))]
          (swap! permission-cache assoc object-id object-permissions)
          object-permissions)))))

(defmethod library-use-permitted? :ssoup/engine [engine user library-id]
  (go
    (let [role (or (keyword (first (:roles user))) :ssoup/anonymous-user)
          library-permissions (<! (get-permissions engine library-id))
          h @(:h-ref engine)]
      (loop [library-permissions library-permissions]
        (if-let [{users :ssoup/users
                  roles :ssoup/roles} (first library-permissions)]
          (if (or (contains? users (:id user))
                  (some #(isa? h role %) (as-set roles)))
            true
            (recur (rest library-permissions)))
          false)))))

(defmethod service-call-permitted? :ssoup/engine [engine user fndef args]
  (go
    (let [role (or (keyword (first (:roles user))) :ssoup/anonymous-user)
          library-id (:library-id fndef)]
      (if (and library-id (<! (library-use-permitted? engine user library-id)))
        true                                                ; if permission is granted at library level then don't need further checks.
        (let [service-permissions (<! (get-permissions engine (:fname fndef)))
              h @(:h-ref engine)]
          (loop [service-permissions service-permissions]
            (if-let [{users           :ssoup/users
                      roles           :ssoup/roles
                      args-mapping-fn :ssoup/args-mapping-fn
                      mapped-args     :ssoup/mapped-args} (first service-permissions)]
              (if (or (contains? users (:id user))
                      (some #(isa? h role %) (as-set roles)))
                (if args-mapping-fn                         ; if mapping function not specified,all arguments assumed good
                  (= (apply args-mapping-fn args) mapped-args)
                  true)
                (recur (rest service-permissions)))
              false)))))))

; -------------------------------------------------------
; IWithScripts
; -------------------------------------------------------
(defmethod register-script* :ssoup/engine [engine script]
  (assoc-in engine [:scripts (:id script)] script))

(defmethod get-script :ssoup/engine [engine script-id]
  (get-in engine [:scripts script-id]))

; -------------------------------------------------------
; IServiceRuntime
; -------------------------------------------------------
(defn- get-classes-key* [engine classes generate-if-not-found build-isa-hierarchy]
  (let [classes-keys (get engine :classes-keys)]
    (trace "classes keys " classes-keys)
    (if-let [k (get classes-keys classes)]
      (retval engine nil k)
      (when generate-if-not-found
        (let [k (keyword (str "ssoup/" (uuid/make-random)))
              ; register the current class set of size n with a new key
              _ (debug "Registering key for class set " classes ": " k)
              engine (assoc-in engine [:classes-keys classes] k)
              ; then derive the new key from the keys of all subclasses of size n-1
              engine (if (or (not build-isa-hierarchy) (<= (count classes) 1))
                       engine
                       (reduce (fn [engine classes-subset]
                                 (let [sk (get-classes-key* engine (set classes-subset)
                                                            true build-isa-hierarchy)
                                       engine (:engine sk)
                                       classes-key (:return-value sk)
                                       _ (info "DERIVE " k classes-key)
                                       engine (derive* engine k classes-key)]
                                   engine))
                               engine
                               (combo/combinations (seq classes)
                                                   (dec (count classes)))))]
          (retval engine nil k))))))

; dispatch function for ssoup service handlers (implemented as multimethods)
#_(defn- dispatch [engine session classes flavor args]
    (let [user (get session :user)
          role (or (keyword (first (:roles user))) :ssoup/anonymous-user)
          sk (get-classes-key* engine classes true false)
          ; new keys can be generated but then the new engine state is
          ; discarded in dispatch: a new key will be generated for the same classes in
          ; the future.
          classes-key (:return-value sk)
          dispatch-value (apply vector
                                role
                                classes-key
                                flavor
                                (get-arg-type-keys engine args))]
      (trace "dv " dispatch-value)
      (trace "h " @(:h-ref engine))
      dispatch-value))

#_(defn init-get-candidates [h-ref]
    (debug "Initializing get-candidates")

    (defmulti get-candidates dispatch :hierarchy h-ref)

    (defmethod get-candidates :default [engine session classes flavor args]
      ; no candidates by default
      (let [user (get session :user)
            role (or (keyword (first (:roles user))) :ssoup/anonymous-user)
            arg-type-keys (get-arg-type-keys engine args)]
        (doall arg-type-keys)
        (debug "No local candidates found for
      user role " role "
      classes " classes " (" (:return-value (get-classes-key* engine classes false false)) ")
      flavor " flavor "
      arguments " args "
      of types " (str-pprint arg-type-keys)
               "     with super types
                " (apply str
                         (map (fn [arg-type-key] (str "Super types of " arg-type-key ": "
                                                      (ancestors (get engine :types) arg-type-key)
                                                      "\n        "))
                              arg-type-keys)))
        [])))

; TODO 22/08/2017:
; rewrite register-service* to:
; - keep a map from classes-key to a set of service definitions
; and rewrite get-candidates to:
; - check if there is a cached result..
; - else loop over all the service definitions with given classes-key and all ancestors
; -   for each service definition check 1) flavor 2) arg-types and 3) permissions
; - cache the results

(defn- stack-entry-satisfies-selector?
  "Checks whether a stack entry satisfies a scs selector"
  [stack-entry selector]
  (let [sel-fname (:fname selector)
        sel-classes (:classes selector)
        sel-arg-types (:arg-types selector)
        fndef (:fndef stack-entry)
        fn-classes (if (coll? (:classes fndef)) (:classes fndef) [(:classes fndef)])
        fn-arg-types (if (coll? (:arg-types fndef)) (:arg-types fndef) [(:arg-types fndef)])
        ret (and (or (nil? sel-fname)
                     (= sel-fname (:fname fndef)))
                 (or (nil? sel-classes)
                     (every? (fn [selcl] (some #(isa? % selcl) fn-classes)) sel-classes))
                 (or (nil? sel-arg-types)
                     ; every? identity  is like  apply and   but apply does not work on macros
                     (every? identity (map (fn [sel-arg-type fn-arg-type] (isa? fn-arg-type sel-arg-type)) sel-arg-types fn-arg-types))))]
    (trace "stack-entry-satisfies-selector? stack-entry: " stack-entry " selector: " selector " -> " ret)
    ret))

(defn- is-relevant-scs-rule?
  "Checks whether a given scs rule is relevant for the given dispatch value at the current stack state"
  ([call-stack scs-rule [user-role classes flavor & arg-types :as dispatch-value]]
   (let [next-stack-entry {:fndef {:classes classes, :arg-types arg-types}}
         stack (conj (into [] call-stack) next-stack-entry) ; calls from least to most recent.
         _ (trace "built stack : " stack)
         ret (is-relevant-scs-rule? call-stack (:selector scs-rule) 0 stack 0)]
     (trace "is-relevant-scs-rule? scs rule " scs-rule " stack : " stack " dispatch value : " dispatch-value " -> " ret)
     ret))
  ([call-stack selectors selector-index stack stack-index]
   (cond
     (= selector-index (count selectors))                   ; all selectors were satisfied
     (= stack-index (count stack))                          ; check that the end of the stack has been reached
     (>= stack-index (count stack)) false                   ; reached the end of the stack without satisfiying the selector
     :else (if (stack-entry-satisfies-selector? (nth stack stack-index) (nth selectors selector-index))
             (is-relevant-scs-rule? call-stack selectors (inc selector-index) stack (inc stack-index))
             (is-relevant-scs-rule? call-stack selectors selector-index stack (inc stack-index))))))

(defn- get-relevant-scs-rules
  "Returns the scs rules that are relevant for the given dispatch value at the current stack state"
  [call-stack scs-rules dispatch-value]
  (doall (filter #(is-relevant-scs-rule? call-stack % dispatch-value) scs-rules)))

(defn- fn-satisfies-scs-rule?
  "Checks whether the candidate service has the name and all the classes specified in the scs rule"
  [candidate scs-rule]
  (let [rule (:rule scs-rule)
        fname (:fname rule)
        classes (:classes rule)
        ret (and (or (nil? fname) (= fname (:fname candidate)))
                 (or (nil? classes) (every? (fn [rulcl] (some #(isa? % rulcl) (:classes candidate))) classes)))]
    (trace "fn-satisfies-scs-rule? " (select-keys candidate [:fname :classes]) " " fname "." classes " -> " ret)
    ret))

(defn- fn-satisfies-scs-rules? [candidate scs-rules]
  (let [ret (every? #(fn-satisfies-scs-rule? candidate %) scs-rules)]
    (trace "fn-satisfies-scs-rules? " (select-keys candidate [:fname :classes]) " " scs-rules " -> " ret)
    ret))

(defn- merge-candidate-sets [candidate-sets]
  (cond
    (empty? candidate-sets)
    #{}
    (> (count candidate-sets) 1)
    (apply clojure.set/intersection candidate-sets)
    :else
    (first candidate-sets)))

(defn- post-filter-candidates [candidates relevant-scs-rules]
  (let [candidates-set candidates]                          ; (merge-candidate-sets candidates)
    (if (> (count relevant-scs-rules) 0)
      ; of the relevant scs rules take the last one
      (clojure.set/select #(fn-satisfies-scs-rule? % (last relevant-scs-rules)) candidates-set)
      candidates-set)))

(defn- adjust-args [args arg-types]
  (into {}
        (map (fn [[arg-name arg-value]]
               (let [arg-type (get arg-types arg-name)]
                 ; if the service expects a set but the argument is not a set, wrap it in a set
                 ; this is useful becuase when fetching properties objects from triple stores
                 ; a single valued property is put in a clojure object (for conveninece) and not
                 ; in a clojure set of a single object.
                 (if (and (= arg-type :ssoup/set)
                          (not (set? arg-value)))
                   [arg-name #{arg-value}]
                   [arg-name arg-value])))
             args)))

(defn service-dominates
  [engine x y scs-rules]
  (let [h @(:h-ref engine)
        x-classes-key (get-classes-key* engine (:classes x) false false)
        y-classes-key (get-classes-key* engine (:classes y) false false)
        x-arg-type-list (into [] (vals (into (sorted-map) (:arg-types x)))) ; seq of arg types,ordered by arg name
        y-arg-type-list (into [] (vals (into (sorted-map) (:arg-types y))))]
    (and
      (isa? h x-classes-key y-classes-key)
      (isa? h (:flavor x) (:flavor y))
      (isa? h x-arg-type-list y-arg-type-list))))

(defmethod find-best-candidate :ssoup/engine [engine user classes flavor args scs-rules]
  ; This code is derived from ClojureScript find-and-cache-best-method for multi-method dispatching
  (go
    (let [h @(:h-ref engine)
          {classes-key :return-value} (get-classes-key* engine classes false false)
          all-classes-keys (clojure.set/union #{classes-key} (or (ancestors classes-key) #{}))
          all-arg-type-keys-combinations (get-all-arg-type-keys-combinations engine args)
          fns (reduce (fn [fns classes-key] (clojure.set/union fns (get-in engine [:fns classes-key])))
                      #{} all-classes-keys)]
      ;(debug "AC K " all-classes-keys)
      (loop [be nil
             fns fns]
        (if (empty? fns)
          be
          (let [{level             :level                   ; :ssoup-engine, :usernormal, :author-normal, :author-important, :user-important
                 service-flavor    :flavor
                 service-arg-types :arg-types
                 :as               fndef} (first fns)
                service-arg-type-list (into [] (vals (into (sorted-map) service-arg-types)))] ; seq of arg types,ordered by arg name])
            (recur
              (loop [be be
                     all-arg-type-keys-combinations all-arg-type-keys-combinations]
                (if (empty? all-arg-type-keys-combinations)
                  be
                  (let [arg-type-keys (into [] (first all-arg-type-keys-combinations))]
                    (if (and (isa? h flavor service-flavor)
                             (isa? h arg-type-keys service-arg-type-list))
                      (if (<! (service-call-permitted? engine user fndef args))
                        (let [be2 (if (or (nil? be) (service-dominates engine fndef be scs-rules))
                                    fndef
                                    (if (service-dominates engine be fndef scs-rules)
                                      be
                                      (throw (exception
                                               (str "Multiple services with classes '" classes
                                                    "' match and neither is preferred. First 2 matches: "
                                                    (:fname be) ", " (:fname fndef))))))]
                          (recur be2 (rest all-arg-type-keys-combinations)))
                        (do
                          (debug (str "Service call " (:fname fndef) " not permitted"))
                          (recur be (rest all-arg-type-keys-combinations))))
                      (recur be (rest all-arg-type-keys-combinations))))))
              (rest fns)))))))
  ; TODO: implement caching somehow
  #_(when best-entry
      (if (= @cached-hierarchy @hierarchy)
        (do
          (swap! method-cache assoc dispatch-val (second best-entry))
          (second best-entry))
        (do
          (reset-cache method-cache method-table cached-hierarchy hierarchy)
          (find-and-cache-best-candidates name dispatch-val hierarchy method-table prefer-table
                                          method-cache cached-hierarchy))))
  )

(defmethod exec-async :ssoup/engine
  [engine session
   {classes           :ssoup/classes
    flavor            :ssoup/flavor
    scs-rules         :ssoup/scs-rules
    ctx               :ssoup/ctx
    args              :ssoup/args
    ssoup-options     :ssoup/options
    ; remote engine ids to try if the local engine cannot execute the service. :ssoup/all for try all connected engines
    remote-engine-ids :ssoup/remote-engine-ids
    remote-engine-id  :ssoup/remote-engine-id
    :as               options}]
  (let [ret-chan (chan)]
    (go (let [user (get session :user)
              _ (debug "USER:    " (ssoup-clj-core.utils/str-pprint user))
              role (or (keyword (first (:roles user))) :ssoup/anonymous-user)
              classes (as-set classes)
              flavor (or flavor (get session :flavor) :ssoup/default-flavor)
              scs-rules (or scs-rules (get engine :author-scs-rules))
              call-stack (or (get-in ctx [:call-context :stack]) [])
              {engine :engine classes-key :return-value} (get-classes-key* engine classes
                                                                           true false)
              arg-type-keys (get-arg-type-keys engine args)
              engine (<! (fix-type-hierarchy* engine session arg-type-keys))
              dispatch-value (apply vector role
                                    classes-key flavor arg-type-keys)
              dispatch-value-str (apply vector role classes
                                        flavor arg-type-keys)
              _ (debug "-------------------------- exec-async " dispatch-value-str " --------------------------")
              relevant-scs-rules (get-relevant-scs-rules call-stack scs-rules dispatch-value)
              ;all-candidates (get-candidates engine session classes flavor args)
              ;_ (trace "All candidates " all-candidates)
              ;allowed-candidates (filter-candidates-by-permissions user all-candidates)
              ;_ (trace "Allowed candidates " allowed-candidates)
              ;final-candidates (post-filter-candidates allowed-candidates relevant-scs-rules)
              best-candidate (<! (find-best-candidate engine user classes flavor args relevant-scs-rules))]
          (let [ret-value
                (if-not best-candidate
                  (if (or remote-engine-id remote-engine-ids)
                    (do
                      (debug (str "Could not find local candidates for " dispatch-value-str ": trying remote SSOUP engines."))
                      (exec-async-remote engine session options))
                    (debug (str "Could not find local candidates for " dispatch-value-str ".")))
                  (let [fndef best-candidate
                        fname (:fname fndef)
                        handler (:handler fndef)]
                    (debug "Best candidate " (select-keys best-candidate [:fname :flavor :user-role :classes :arg-types]))
                    (trace "scs rules " scs-rules)
                    (debug "Dispatching " dispatch-value-str " to " fname)
                    (let [ctx (assoc-in ctx [:call-context :stack]
                                        (conj call-stack
                                              {:dispatch-value dispatch-value
                                               :fndef          (select-keys fndef [:fname :flavor :user-role :classes :arg-types])}))
                          arg-types (:arg-types fndef)
                          args (adjust-args args arg-types)]
                      ; handlers can return either a value or the special retval that
                      ; includes the return value and the updated engine and session
                      (try
                        (handler engine session ctx args ssoup-options)
                        (catch #?(:clj Exception) #?(:cljs js/Error) e
                                                                     (error "An error occurred executing handler " e)
                                                                     #?(:clj (.printStackTrace e))
                                                                     e)))))
                ;; TODO: expect as return value of the handler only instructions to
                ;; change the session / engine state and not the actual engine /
                ;; session objects, then apply the operations needed to change the
                ;; engine / session inside the transducer rv
                rv (fn [r]
                     (if (= :ssoup/return-value (get-type engine r))
                       r
                       (retval engine session r)))
                rvt (fn [reducing]
                      (fn
                        ([result]
                         (reducing result))
                        ([result r]
                         (trace "Reducing " (str-pprint (:return-value (rv r))))
                         (reducing result (rv r)))))]
            (if (channel? ret-value)
              (pipeline 1 ret-chan rvt ret-value)
              (do
                (trace "ret-val " ret-value)
                (put! ret-chan (rv ret-value))
                (close! ret-chan))))))
    ret-chan))

(defmethod exec-async-remote :ssoup/engine
  [engine session
   {remote-engine-id  :ssoup/remote-engine-id
    remote-engine-ids :ssoup/remote-engine-ids
    classes           :ssoup/classes flavor :ssoup/flavor
    scs-rules         :ssoup/scs-rules ctx :ssoup/ctx
    args              :ssoup/args ssoup-options :ssoup/options
    :as               options}]
  (let [user (:user session)]
    (if remote-engine-id
      ; if a specific remote engine id is provided, send the request there
      (go (try (let [remote-engine (or (get-remote-engine-by-id engine remote-engine-id)
                                       (throw (exception (str "No engine with id " remote-engine-id " was found"))))
                     remote-engine-connector (:connector-client remote-engine)
                     ctx {:engine engine :session session}
                     ret-value (if-err-throw (<! (connector/exec remote-engine-connector ctx user classes flavor
                                                                 args ssoup-options)))]
                 (retval engine session ret-value))
               (catch #?(:clj Exception)
                      #?(:cljs js/Error) e (debug "Exec-async engine " remote-engine-id ": " e) nil)))
      ; if no specific remote engine is provided, look for a connected engine able to execute the request
      (let [remote-engine-ids (or remote-engine-ids
                                  (into [] (keys (:remote-engines-by-id engine))))]
        (if (empty? remote-engine-ids)
          nil
          (go
            (loop [index 0
                   size (count remote-engine-ids)]
              (if (< index size)
                (let [remote-engine-id (get remote-engine-ids index)]
                  (debug "Exec-async engine " (inc index) "/" size ": " remote-engine-id)
                  (or (<! (exec-async-remote engine session (merge options
                                                                   {:ssoup/remote-engine-id remote-engine-id})))
                      (recur (inc index) size)))
                (let [msg (str "No remote engine function with classes [" classes "] flavor ["
                               flavor "] args types [" (doall (get-arg-type-keys engine args))
                               "] and user role [" (first (:roles user)) "] was found")]
                  (debug msg)
                  (exception msg))))))))))

#?(:clj
   (defmacro ssoup-exec-sync [engine session classes flavor scs-rules ctx args options]
     "Execute a ssoup service synchronously. Only callable from Clojure."
     `(let [ret# (exec-async ~engine ~session
                             {:ssoup/classes   ~classes
                              :ssoup/flavor    ~flavor
                              :ssoup/scs-rules ~scs-rules
                              :ssoup/ctx       ~ctx
                              :ssoup/args      ~args
                              :ssoup/options   ~options})]
        (if (channel? ret#)
          (if-err-throw (<!! ret#))
          ret#))))

; -------------------------------------------------------
; IScriptRuntime
; -------------------------------------------------------
(defn- exec-node [engine session ctx rg node-id options])

;(defn run-script [engine session script script-arguments]
;  (script engine session script-arguments))

(defn- update-latest-node-exec-time! [rg node-id]
  (swap! rg assoc-in [:latest-node-exec-times node-id] (t/now)))

(defn- get-latest-node-exec-time [rg node-id]
  (get-in @rg [:latest-node-exec-times node-id]))

(defn- update-latest-elapsed-time! [rg node-id]
  (swap! rg assoc-in [:latest-node-elapsed-time node-id] (t/in-millis (t/interval (get-latest-node-exec-time rg node-id) (t/now)))))

(defn- get-latest-node-elapsed-time [rg node-id]
  (get-in @rg [:latest-node-elapsed-time node-id]))

(defn- update-latest-node-output! [rg node-id ret]
  (swap! rg assoc-in [:latest-node-outputs node-id] ret))

(defn- get-latest-node-output [rg node-id]
  (get-in @rg [:latest-node-outputs node-id]))

(defn- resolve-args-and-options [engine session ctx rg args]
  (go
    (if (nil? args)
      {}
      (let [args (if (channel? args) (<! args) args)        ; if args is a channel, wait for it to be resolved to a value (must be an array).
            args (into (sorted-map) args)                   ; reordered args by arg name
            arg-keys (into [] (keys args))
            size (count args)
            script-arguments (:script-arguments ctx)
            ; if some arguments are channels, wait for them to be resolved to a value
            ; if some arguments are variable references, resolve them
            rargs (loop [i 0                                ; need to use a loop, for or map not working: core async stops at function boundaries
                         result {}]
                    (if (= i size)
                      result
                      (let [k (get arg-keys i)
                            v (get args k)
                            rv (cond
                                 (channel? v)
                                 v                          ; channels are waited for in the next loop

                                 (= :ssoup/from-var (get-type engine v))
                                 (get-in session [:vars (:var v)])

                                 (= :ssoup/from-node (get-type engine v))
                                 (if (= "latest" (:ssoup/mode v))
                                   (if-let [latest-node-value (get-latest-node-output rg (:ssoup/node-id v))]
                                     (gretval engine latest-node-value)
                                     (gretval engine (exec-node engine session ctx rg
                                                                (:ssoup/node-id v) {}))) ;
                                   ; returning the channel so to start all dependent nodes in parallel
                                   (gretval engine (exec-node engine session ctx rg
                                                              (:ssoup/node-id v)
                                                              {}))) ; in the next loop we wait for results

                                 (= :ssoup/from-script-argument (get-type engine v))
                                 (do
                                   (debug "Trying to resolve: " v " => " (get script-arguments (keyword (:ssoup/argument-name v))))
                                   (get script-arguments (keyword (:ssoup/argument-name v))))

                                 (= :ssoup/from-user-attribute (get-type engine v))
                                 (when session
                                   (get-in session [:user (keyword (:ssoup/attribute v))]))

                                 :else
                                 v)]
                        (recur (inc i) (assoc result k rv)))))
            rargs (loop [i 0                                ; need to use a loop, for or map not working: core async stops at function boundaries
                         result {}]
                    (if (= i size)
                      result
                      (let [k (get arg-keys i)
                            v (get rargs k)
                            rv (cond
                                 (channel? v) (<! v)
                                 :else v)]
                        (recur (inc i) (assoc result k rv)))))]
        rargs))))

(defn- compute-triggers-map [rg]
  (let [triggers-map (atom {})]
    (doseq [[node-id node] (:ssoup/body @rg)]
      (let [triggered-by (:ssoup/triggered-by node)
            triggered-by (as-set triggered-by)
            triggers (:ssoup/triggers node)
            triggers (as-set triggers)]
        (when triggered-by
          (doseq [triggering-node-id-or-set triggered-by]
            (when (keyword? triggering-node-id-or-set)
              (swap! triggers-map assoc triggering-node-id-or-set (clojure.set/union #{node-id} (or (get @triggers-map triggering-node-id-or-set) #{}))))
            (when (set? triggering-node-id-or-set)
              (doseq [triggering-node-id triggering-node-id-or-set]
                (swap! triggers-map assoc triggering-node-id (clojure.set/union #{{:type   :ssoup.trigger/with
                                                                                   :with   (disj triggering-node-id-or-set triggering-node-id)
                                                                                   :target node-id}}
                                                                                (or (get @triggers-map triggering-node-id) #{})))))))
        (when triggers
          (swap! triggers-map assoc node-id (clojure.set/union triggers (or (get @triggers-map node-id) #{}))))))
    (swap! rg assoc :triggers-map @triggers-map)))

(defn- compute-nodes-to-trigger [rg node-id node-output]
  (let [triggers-map (:triggers-map @rg)
        nodes-to-trigger (atom #{})]
    (doseq [trigger (get triggers-map node-id)]
      (debug "Trigger " node-id " " trigger)
      (when (keyword? trigger)
        (swap! nodes-to-trigger conj trigger))
      (when (map? trigger)
        (when (= :ssoup.trigger/with (:type trigger))
          (let [with-nodes (:with trigger)]                 ; the target is triggered only if all the other nodes have been executed at least one time
            (when (every? #(not (nil? (get-latest-node-output rg %))) with-nodes)
              (swap! nodes-to-trigger conj (:target trigger)))))
        (when (= :ssoup.trigger/on-outcome (:type trigger))
          (let [target (get (:targets trigger) node-output)]
            (swap! nodes-to-trigger clojure.set/union target)))))
    @nodes-to-trigger))

(defn- run-triggers [engine session ctx rg node-id ret options]
  (let [nodes-to-trigger (compute-nodes-to-trigger rg node-id ret)]
    (debug "Node " node-id " triggering nodes " nodes-to-trigger)
    (doseq [node-to-trigger nodes-to-trigger]
      (if (= :ssoup/script-ref (:type node-to-trigger))
        (let [script-id (:script-id node-to-trigger)
              script (get-script engine script-id)]
          (exec-script engine session ctx script (:script-arguments ctx) options))
        (exec-node engine session ctx rg node-to-trigger options)))))

(defn- exec-node [engine session ctx rg node-id options]
  (let [node (get (:ssoup/body @rg) node-id)
        ret-chan (chan)]
    (go
      (info "Processing node " node-id " for execution")
      (when (= :ssoup/call (get-type engine node))
        (let [rargs (<! (resolve-args-and-options engine session ctx rg (:ssoup/args node)))
              roptions (<! (resolve-args-and-options engine session ctx rg (:ssoup/options node)))
              node (merge {:ssoup/ctx ctx}
                          node
                          {:ssoup/args              rargs
                           :ssoup/options           roptions
                           :ssoup/remote-engine-id  (:ssoup/remote-engine-id node)
                           :ssoup/remote-engine-ids (:ssoup/remote-engine-ids node)})]
          (update-latest-node-exec-time! rg node-id)
          (info "Executing node " node-id "with args" rargs "and options" roptions)
          (let [out-chan (exec-async engine session node)]
            (loop [engine engine]                           ; while the node is sending output..
              (let [{engine  :engine
                     session :session
                     ret     :return-value
                     :as     out} (<! out-chan)]
                (when out
                  (info "Node " node-id " emitted output: " (str-pprint ret))
                  (update-latest-node-output! rg node-id out)
                  (let [engine (if (:store-result-in-var roptions)
                                 (set-var* engine session
                                           (:store-result-in-var roptions) ret)
                                 engine)]
                    ; trigger the execution of subsequent nodes and do not wait for completion
                    (run-triggers engine session ctx rg node-id ret roptions)
                    (put! ret-chan (retval engine session ret))
                    ; this recur appears to generate an infinite loop: why ?
                    ;(recur engine)
                    ))))
            (close! ret-chan)
            (update-latest-elapsed-time! rg node-id)
            (debug "Node " node-id " execution terminated")))))
    ret-chan))

(defn- ensure-body-is-map [script]
  (let [body (:ssoup/body script)]
    (if (and body (set? body))
      (assoc script :ssoup/body
                    (let [mk (map (fn [node]
                                    [(:id node) node])
                                  body)]
                      (into {} mk)))
      script)))

(defmethod exec-script :ssoup/engine [engine session ctx script script-arguments options]
  (info "Executing script " (:id script))
  (let [ctx (or (:ctx script) ctx {})
        ctx (assoc ctx :script-arguments script-arguments)
        start-nodes (:ssoup/start-nodes script)
        start-nodes (if (set? start-nodes) start-nodes #{start-nodes})
        script (ensure-body-is-map script)
        rg (atom script)]                                   ; script graph augmented with runtime info
    (compute-triggers-map rg)
    (debug "Triggers " (get @rg :triggers-map))
    (doseq [start-node start-nodes]
      (exec-node engine session ctx rg start-node options))))

; -------------------------------------------------------
; IWithSessions
; -------------------------------------------------------
(defmethod set-session* :ssoup/engine [engine session]
  (assoc-in engine [:sessions (:id session)] session))

(defmethod get-session :ssoup/engine [engine session-id]
  (get-in engine [:sessions session-id]))

; -------------------------------------------------------
; IWithAskContexts
; -------------------------------------------------------
(defmacro with-ask-contexts
  "Binds a new ask context for each binding and evaluates body in a try expression, and a
  finally clause that calls (delete-ask-context name) on binding in reverse order."
  [engine bindings & body]
  (assert-args
    (vector? bindings) "a vector for its bindings")
  (cond
    (= (count bindings) 0) `(do ~@body)
    (symbol? (bindings 0))
    (let [ask-ctx-symbol (bindings 0)
          ask-ctx-options (bindings 1)]
      `(let [~ask-ctx-symbol (new-ask-context engine ~ask-ctx-options)]
         (try
           (with-ask-contexts ~(subvec bindings 2) ~ask-ctx-options ~@body)
           (finally
             (delete-ask-context engine ~ask-ctx-symbol)))))
    :else (if-err-throw "with-ask-contexts only allows Symbols in bindings")))

(defn is-sync-context [ctx]
  (:sync ctx))

(defn mux [engine session ctx & args]
  (if (= (count args) 0)
    (let [ch (chan)]
      (put! ch [])
      ch)
    (if (is-sync-context ctx)
      (do
        ; sync context: exec asks in sequence
        (go (let [exec-params (first args)
                  ret (<! (exec-async engine session exec-params))
                  rest (<! (apply mux ctx (subvec (apply vector args) 1)))]
              (concat [ret] rest))))
      (do
        ; async context
        (async/map
          vector
          (for [exec-params args]
            (exec-async engine session exec-params)))))))

; -------------------------------------------------------
; IWithSessions
; -------------------------------------------------------
(defn new-session-fn* [engine additional-attributes options]
  (debug "Creating new session. Provided id: " (:session-id options))
  (let [session (merge {:id     (or (:session-id options)
                                    (uuid/make-random-str))
                        :engine engine
                        :user   anon-user
                        :flavor :ssoup/default-flavor
                        :vars   {}
                        :fns    {}}
                       additional-attributes)
        engine (set-session* engine session)]
    (retval engine session session)))

(defmethod new-session* :ssoup/engine [engine additional-attributes options]
  (new-session-fn* engine additional-attributes options))

; -------------------------------------------------------
; ISession
; -------------------------------------------------------
(defmethod init-session :ssoup/engine [engine session]
  session)

; -------------------------------------------------------
; IWithUser
; -------------------------------------------------------
(defn set-current-user-fn* [engine session user]
  (let [engine (reduce (fn [engine role]
                         (derive* engine role :ssoup/any-user))
                       engine (:roles user))
        session (assoc session :user user)
        engine (set-session* engine session)]
    (info "User changed to " user)
    (retval engine session nil)))

(defmethod set-current-user* :ssoup/engine [engine session user]
  (set-current-user-fn* engine session user))

(defmethod get-current-user :ssoup/engine [engine session]
  (:user session))

; -------------------------------------------------------
; IWithFlavor
; -------------------------------------------------------
(defmethod set-current-flavor :ssoup/engine [engine session flavor]
  (info "Flavor changed to " flavor)
  (retval engine (assoc session :flavor flavor) nil))

(defmethod get-current-flavor :ssoup/engine [engine session]
  (:flavor session))

;--------------
; Clojure exposed convenience functions
;--------------
(defn run-script
  ([engine session ctx script script-arguments]
   (run-script engine session ctx script script-arguments {}))
  ([engine session ctx script script-arguments options]
   (let [ctx (or (:ctx script) ctx {})]
     (exec-async engine session
                 {:ssoup/ctx     ctx
                  :ssoup/classes #{:ssoup/run-script}
                  :ssoup/flavor  :ssoup/default-flavor
                  :ssoup/args    {:script           script
                                  :script-arguments script-arguments}
                  :ssoup/options options}))))

(def default-context (merge rdf/schema-org-context
                            {:ssoup "http://ssoup.org/"
                             :n     "http://node.ssoup.org/"}))
