(ns ^:figwheel-always ssoup-clj-core.compiler
  (:require
    [ssoup-clj-core.types :as types]
    [clojure.core.async :as async :refer [<! chan >! put! close! pipe]]
    #?(:clj [clojure.core.async :refer [go <!!]])
    [clojure.core.async.impl.protocols :refer [Channel]]
    [clojure.set]
    [ssoup-clj-core.core :as ssoup :refer [with-ask-contexts mux]]
    [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)])
  #?(:cljs (:require-macros [cljs.core.async.macros :refer [go]])))

(defn dispatch-compile [flavor arg]
  [flavor (types/get-arg-type arg)])

(defmulti ssoup-compile dispatch-compile)

(defmethod ssoup-compile [:ssoup/default-flavor :ssoup/call] [flavor calldef]
  (list 'ssoup/exec-async {:classes (:classes calldef)
                           :flavor  flavor
                           :args    (:args calldef)}))

(defmethod ssoup-compile [:ssoup/default-flavor :ssoup/with-ask-context] [flavor calldef]
  (let [args (:args calldef)
        ask-ctx (:ask-ctx args)
        body (:body args)]
    (list 'ssoup/with-ask-contexts [(symbol (:name ask-ctx)) ask-ctx]
          (ssoup-compile flavor body))))

(defmethod ssoup-compile [:ssoup/default-flavor :ssoup/ask] [flavor sequencedef]
  (list 'ssoup/exec-async {:classes :ssoup/ask}))

(defmethod ssoup-compile [:ssoup/default-flavor :ssoup/array] [flavor sequencedef]
  (list 'apply 'do (doall (for [el sequencedef] (ssoup-compile flavor el)))))

(defn dispatch-eval [ctx arg]
  [(types/get-arg-type arg)])

(defmulti ssoup-eval dispatch-eval)

(defmethod ssoup-eval [:ssoup/call] [ctx calldef]
  (let [args (:args calldef)
        options (:options calldef)
        arg-keys (into [] (keys args))
        size (count args)
        ctx (if (:ctx calldef) (:ctx calldef) ctx)
        _ (debug "ctx " ctx)
        flavor (if (:flavor calldef) (:flavor calldef) (ssoup/get-current-flavor))]
    (go
      ; first resolve all the unresolved arguments
      (let [rargs
            (loop [index 0
                   resolved-args {}]
              (if (= index size)
                resolved-args
                (let [k (get arg-keys index)
                      v (get args k)
                      rv (if (= (:type v) :ssoup/call)
                           (<! (ssoup-eval flavor v))
                           v)]
                  (recur (inc index) (assoc resolved-args k rv)))))]
        ; then execute the service
        (<! (ssoup/exec-async {:ctx     ctx
                               :classes (:classes calldef)
                               :flavor  flavor
                               :args    rargs
                               :options options}))))))

(defmethod ssoup-eval [:ssoup/array] [ctx sequencedef]
  (debug "Evaluating array")
  (go
    (loop [i 0
           ret nil]
      (if (= i (count sequencedef))
        ret
        (let [r (<! (ssoup-eval ctx (get sequencedef i)))]
          (recur (inc i) r))))))

(defmethod ssoup-eval [:ssoup/script] [ctx script]
  (info "Executing script " (:name script))
  (let [ctx (if (:ctx script) (:ctx script) ctx)]
    (ssoup-eval ctx (:body script))))

(comment
  (defmethod ssoup-eval [:ssoup/default-flavor :ssoup/with-ask-context] [flavor calldef]
    (let [args (:args calldef)
          ask-ctx (:ask-ctx args)
          body (:body args)]
      (with-ask-contexts [(symbol (:name ask-ctx)) ask-ctx]
                         (ssoup-eval flavor body))))

  (defmethod ssoup-eval [:ssoup/command-line-theme :ssoup/ask] [flavor calldef]
    (let [args (:args calldef)
          options (:options calldef)]
      (ssoup/exec-async {:classes :ssoup/ask
                         :flavor  flavor
                         :args    args
                         :options options}))))
