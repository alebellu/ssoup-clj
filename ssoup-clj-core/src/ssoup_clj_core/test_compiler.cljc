(ns ^:figwheel-always ssoup-clj-core.test-compiler
  #?(:clj
     (:require
       [clojure.core.async :as async :refer [<! <!! chan go >! put!]]
       [clojure.core.async.impl.protocols :refer [Channel]]
       [clojure.set]
       [ssoup-clj-core.core :as s]
       [ssoup-clj-core.compiler :as scc]
       [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)]))
  #?(:cljs
     (:require
       [cljs.core.async :as async :refer [<! chan close! >! put!]]
       [cljs.core.async.impl.protocols :refer [Channel]]
       [cljs-http.client :as http]
       [cljs.js :refer [eval eval-str]]
       [clojure.set]
       [ssoup-clj-core.core :as s]
       [ssoup-clj-core.compiler :as scc]
       [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]))
  #?(:cljs
     (:require-macros
       [cljs.core.async.macros :refer [go]])))

(comment
  (defn section1-declarative
    "Verbose version of ssoup scripting: useful for automation."
    [ctx]
    (s/exec-async {:classes :ssoup/show
                   :ctx     ctx
                   :args    [[{:type        (s/array-type :sqo/plant)
                               :keys        [:type :name :size]
                               :total-count 1543
                               :page        1
                               :total-pages 10
                               :items       [{:type :sqo/plant, :uuid "P0001", :name "plant01", :size 1435}
                                             {:type :sqo/plant, :uuid "P0002", :name "plant02"}]}
                              {:type        (s/array-type :sqo/plant)
                               :keys        [:type :name :size]
                               :total-count 1543
                               :page        1
                               :total-pages 10
                               :items       [{:type :sqo/plant, :uuid "P0003", :name "plant03", :size 1435}
                                             {:type :sqo/plant, :uuid "P0004", :name "plant04"}]}]]}))
  (s/register! {:fname     "section1-declarative"
                :handler   section1-declarative
                :flavor    :ssoup/default-flavor
                :user-role :ssoup/any-user
                :classes   :sqo/section1-declarative
                :arg-types []})

  (defn main-menu-declarative
    "Toy Main menu, the declarative way"
    []
    {:type :ssoup/with-ask-context
     :args {:ask-ctx {:name "main-menu" :immediate true :mode :repeat}
            :body    {:type :ssoup/ask
                      :args {:ask-type {:type        :ssoup/enum
                                        :enum-values [{:key :section1 :label "Section 1"}
                                                      {:key :section2 :label "Section 2"}
                                                      {:key :section3 :label "Section 3"}]
                                        :triggers    {
                                                      :section1 {:type :sqo/section1-declarative}
                                                      :section2 {:type :sqo/section2-declarative}
                                                      :section3 {:type :sqo/section3-declarative}}}}}}})

  ; To start a figwheel repl:
  ; (use 'figwheel-sidecar.repl-api)
  ; (cljs-repl)

  ;(sc/ssoup-compile :ssoup/default-flavor (main-menu-declarative))
  (scc/ssoup-eval :ssoup/default-flavor (main-menu-declarative)))
;(js/eval (c/emit (ana/analyze (assoc (ana/empty-env) :context :expr) (println "hhihi"))))
;(ana/analyze (assoc (ana/empty-env) :context :expr) (println "hhihi"))

;(eval (sc/ssoup-compile :ssoup/default-flavor (main-menu-declarative)))

#?(:cljs
   (do
     ; in order to load dependencies in eval see https://github.com/swannodette/cljs-bootstrap/blob/master/src/browser/cljs_bootstrap/dev.cljs
     (def libs
       {'cljs.core.async        :cljs
        'cljs.core.async.macros :clj})

     (defn browser-load [{:keys [name macros]} cb]
       (if (contains? libs name)
         (go
           (let [path (str "/js/out/" (cljs.js/ns->relpath name)
                           "." (cljs.core/name (get libs name)))
                 src (<! (http/get path))]
             (cb {:lang :clj :source src}))))
       (cb nil))

     (defn test-eval []
       (eval-str (cljs.js/empty-state)
                 ;(scc/ssoup-compile :ssoup/default-flavor (main-menu-declarative))
                 ;'(cljs.core.async.macros/go (println (cljs.core.async/<! "hihihihi!!")))
                 "(ns aletest.core (:require-macros [cljs.core.async.macros :refer [go]]))\n(go (println \"ciao\"))"
                 'aletest.core
                 ;'(ssoup-clj-client.core/test)
                 {:eval cljs.js/js-eval
                  :load browser-load}
                 (fn [ret]
                   (info ret)))))

   ;(test-eval)
   )
