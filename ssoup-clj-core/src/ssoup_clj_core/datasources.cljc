(ns ssoup-clj-core.datasources
  (:require
    [ssoup-clj-core.types :refer [dispatch-on-type]]
    [ssoup-clj-core.state :as state]
    [ssoup-rdf.core :as rdf]))

; --------------------------------------------------------------------------------------
; SSOUP datasource: a repository for SSOUP objects.
; A SSOUP object is the equivalent of an aggregate in the Domain Driven Design
; sense (https://martinfowler.com/bliki/DDD_Aggregate.html).
; --------------------------------------------------------------------------------------

; defprotocol IDataSource
(defmulti init-ds #_[ds ctx]
          "Initialize the datasource" dispatch-on-type)
(defmulti tear-down #_[ds ctx]
          "Tear down the datasource" dispatch-on-type)
(defmulti get-model-rdf-context #_[ds]
          "Returns the context derived from the datasource rdf model if possible" dispatch-on-type)
(defmulti get-object #_[query-ds ctx object-id data-pattern options]
          "Retrieve a channel that will receive the object with the given id from a datasource.
          null is put in the channel if the object was not found" dispatch-on-type)
(defmulti get-object-with-meta #_[query-ds ctx object-id data-pattern options]
          "Retrieve a channel that will receive the object with the given id from a datasource, together with the attached metadata.
           null is put in the channel if the object was not found" dispatch-on-type)
(defmulti list-objects-with-property #_[query-ds ctx property-id property-value data-pattern
                                        options]
          "Retrieve a channel that will receive the objects with the given value for the specified property from a
          datasource. null is put in the channel if no objects were found" dispatch-on-type)
(defmulti contains-object #_[ds ctx object-id]
          "Return a channel that will receive a boolean indicating whether the datasource contains the object with the given id."
          dispatch-on-type)
(defmulti save-object #_[ds ctx rdf-ctx object options]
          "Save an object into the datasource"
          dispatch-on-type)
(defmulti load-object-from-file #_[ds ctx filename options]
          "Load an object from a file and save it into the datasource"
          dispatch-on-type)
(defmulti get-schema #_[query-ds ctx options]
          "Return a channel that will receive the full schema from a datasource: a map from type id to type schema definition.
           null is put in the channel if no schema information was not found" dispatch-on-type)
(defmulti get-type-schema #_[schema-ds ctx type options]
          "Return a channel that will receive the schema of the given type from the datasources checked as
          :schema based.
           null is put in the channel if no schema info was found" dispatch-on-type)
(defmulti get-super-types #_[schema-ds ctx type options]
          "Return a channel that will receive the super types of the given type from the datasources checked as
          :schema based.
           null is put in the channel if no schema info was found or if there are no supertypes"
          dispatch-on-type)

; defprotocol IRemotizableDataSource
(defmulti sync-ds #_[ds ctx objects-to-update objects-to-fetch]
          "Synchronize the local datasource with the changes specified in objects-to-update
           Returns the current value of all the objects specified in objects-to-fetch"
          dispatch-on-type)

; defprotocol IRemotizedDataSource
(defmulti do-sync #_[ds ctx]
          "Do the synchronization of the local datasource with the remote datasource"
          dispatch-on-type)


;-------------------------
; Helper function to save data on the current engine.
; TODO: refactor code so that functions are immutable and return the mutation to be applied to the engine.
;-------------------------
(defn current-engine-save-object [ds rdf-ctx object options]
  (let [engine (state/get-engine)
        ctx {:engine engine}
        object (if (string? object)
                 (rdf/from-rdf object (:ssoup/data-format options))
                 object)]
    (save-object ds ctx rdf-ctx object options)))
