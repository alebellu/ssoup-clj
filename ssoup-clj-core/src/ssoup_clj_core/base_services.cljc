(ns ssoup-clj-core.base-services
  (:require [ssoup-clj-core.core :as s]
            [ssoup-clj-core.pub-sub :as ps]
            [ssoup-clj-core.utils :refer [retval]]
            [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)]))

; -------------------------------------------------------
; Library functions implementation.
; -------------------------------------------------------


; -------------------------------------------------------
; Library registration.
; -------------------------------------------------------
(defn- init-library [])

(s/register-library!
  {:library-id      :ssoup/base-services
   :engine-type     :ssoup/engine
   :ssoup/services  [{:fname     :ssoup/set-current-flavor
                      :handler   (fn [engine session ctx args options]
                                   (let [flavor (:ssoup/flavor args)]
                                     (retval engine (assoc session :flavor flavor)
                                             nil)))
                      :flavor    :ssoup/default-flavor
                      :user-role :ssoup/any-user
                      :classes   #{:ssoup/set-current-flavor}
                      :arg-types {:ssoup/flavor :ssoup/keyword}}
                     {:fname     :ssoup/run-script
                      :handler   (fn [engine session ctx args options]
                                   (s/exec-script engine session ctx (:script args)
                                                  (:script-arguments args) options))
                      :flavor    :ssoup/default-flavor
                      :user-role :ssoup/any-user
                      :classes   #{:ssoup/run-script}
                      :arg-types {:ssoup/script           :ssoup/script
                                  :ssoup/script-arguments :rdfs/Resource}}
                     {:fname     :ssoup/get-object
                      :handler   (fn [engine session ctx args options]
                                   (s/get-object engine session ctx
                                                 (:ssoup/data-tags args)
                                                 (:ssoup/object-id args)
                                                 (:ssoup/data-pattern args)
                                                 options))
                      :flavor    :ssoup/default-flavor
                      :user-role :ssoup/any-user
                      :classes   #{:ssoup/get-object}
                      :arg-types {:ssoup/data-tags :ssoup/set
                                  :ssoup/object-id       :ssoup/keyword
                                  :ssoup/data-pattern    :ssoup/array}}
                     {:fname     :ssoup/list-objects-with-property
                      :handler   (fn [engine session ctx args options]
                                   (s/list-objects-with-property engine session ctx
                                                                 (:ssoup/data-tags args)
                                                                 (:ssoup/property-id args)
                                                                 (:ssoup/property-value args)
                                                                 (:ssoup/data-pattern args)
                                                                 options))
                      :flavor    :ssoup/default-flavor
                      :user-role :ssoup/any-user
                      :classes   #{:ssoup/list-objects-with-property}
                      :arg-types {:ssoup/data-tags :ssoup/set
                                  :ssoup/property-id     :ssoup/keyword
                                  :ssoup/property-value  :rdfs/Literal
                                  :ssoup/data-pattern    :ssoup/array}}
                     {:fname     :ssoup/get-type-schema
                      :handler   (fn [engine session ctx args options]
                                   (s/get-type-schema engine session ctx
                                                      (:ssoup/data-tags args)
                                                      (:ssoup/type args)
                                                      options))
                      :flavor    :ssoup/default-flavor
                      :user-role :ssoup/any-user
                      :classes   #{:ssoup/get-type-schema}
                      :arg-types {:ssoup/data-tags :ssoup/set
                                  :ssoup/type            :ssoup/keyword}}
                     {:fname     :ssoup/get-super-types
                      :handler   (fn [engine session ctx args options]
                                   (s/get-super-types engine session ctx
                                                      (:ssoup/data-tags args)
                                                      (:ssoup/type args)
                                                      options))
                      :flavor    :ssoup/default-flavor
                      :user-role :ssoup/any-user
                      :classes   #{:ssoup/get-super-types}
                      :arg-types {:ssoup/data-tags :ssoup/set
                                  :ssoup/type            :ssoup/keyword}}
                     {:fname     :ssoup/save-object
                      :handler   (fn [engine session ctx args options]
                                   (s/save-object engine session ctx
                                                  (:ssoup/data-tags args)
                                                  (:ssoup/object args)
                                                  options))
                      :flavor    :ssoup/default-flavor
                      :user-role :ssoup/any-user
                      :classes   #{:ssoup/save-object}
                      :arg-types {:ssoup/data-tags :ssoup/set
                                  :ssoup/object          :rdfs/Resource}}
                     {:fname     :ssoup/subscribe-to-object-updates
                      :handler   (fn [engine session ctx args options]
                                   (ps/subscribe-to-object-updates
                                     engine session ctx
                                     (:ssoup/data-tags args)
                                     (:ssoup/object-id args)
                                     (:ssoup/data-pattern args)
                                     options))
                      :flavor    :ssoup/default-flavor
                      :user-role :ssoup/any-user
                      :classes   #{:ssoup/subscribe-to-object-updates}
                      :arg-types {:ssoup/topic-id :xsd/string}}
                     {:fname     :ssoup/sync-datasource
                      :handler   (fn [engine session ctx args options]
                                   (s/sync-datasource engine session ctx
                                                      (:ssoup/datasource-name args)
                                                      (:ssoup/objects-to-update args)
                                                      (:objects-to-fetch args)
                                                      options))
                      :flavor    :ssoup/default-flavor
                      :user-role :ssoup/any-user
                      :classes   #{:ssoup/sync-datasource}
                      :arg-types {:ssoup/datasource-name   :xsd/string
                                  :ssoup/objects-to-update :ssoup/set
                                  :ssoup/objects-to-fetch  :ssoup/set}}]
   :init-library-fn init-library})
