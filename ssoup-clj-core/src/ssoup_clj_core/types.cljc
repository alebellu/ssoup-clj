(ns ^:figwheel-always ssoup-clj-core.types
  (:require
    [clojure.math.combinatorics :as combo]
    [taoensso.timbre :as timbre
             :refer [log trace debug info warn error fatal]]))

(defn dispatch-on-type [& args] (:type (first args)))

(defn ssoup-cast [obj type]
  (assoc obj :type type))

(defn derive* [engine tag parent]
  (when (nil? engine)
    (debug "derive* nil: " tag "  " parent))
  (if (and tag parent)
    (let [h (get engine :types)
          h (derive h tag parent)
          h-ref (:h-ref engine)]
      ; this is just used for multimethods dispatch:
      ; the value should always be in line with :types.
      (reset! h-ref h)
      (debug "Derive* " tag " -> " parent)
      (assoc engine :types h))
    engine))

(defn boolean? [v]
  (or (false? v) (true? v)))

(defn atomic? [engine type]
  (let [h (get engine :types)]
    (isa? h type :xsd/anyAtomicType)))

(defn get-type-keyword [type]
  (if (map? type)
    (:type type)
    type))

(defn get-item-type-keyword [type]
  (when (= :ssoup/array (get-type-keyword type))
    (if (map? type)
      (:item-type type)
      :rdfs/Resource)))

(def stype
  ; Returns an object representing a type (like the Java class Class)
  (memoize
    (fn [mapped-type]
      (let [type-key (keyword (str "ssoup/type<"
                                   (subs (str (get-type-keyword mapped-type)) 1)
                                   ">"))]
        {:type        :ssoup/type
         :type-key    type-key
         :base-type   :ssoup/type
         :mapped-type mapped-type}))))

(defn box [value type]
  "Wrap a value in a types object with a value property: this way typed nil is allowed."
  (or value
      {:rdf/type    type
       :ssoup/value nil}))

(defn unbox [value]
  "Unboxes a boxed value."
  (if (contains? value :ssoup/value)
    (:ssoup/value value)
    value))

(def array-type
  ; Returns an object representing an array type
  (memoize
    (fn [item-type]
      (let [type-key (keyword (str "ssoup/array<"
                                   (subs (str (get-type-keyword item-type)) 1)
                                   ">"))
            array-type {:type      :ssoup/type
                        :type-key  type-key
                        :base-type :ssoup/array
                        :item-type item-type}]
        array-type))))

(def enum-type
  ; Returns an object representing an enum type
  (memoize
    (fn [enum-values]
      (let [type-key (keyword (str "ssoup/enum<"
                                   (map :key enum-values)
                                   ">"))
            enum-type {:type        :ssoup/type
                       :type-key    type-key
                       :base-type   :ssoup/enum
                       :enum-values enum-values}]
        enum-type))))

(defn get-base-type [type]
  (when-let [base-type (:base-type type)]
    (cond
      (keyword? base-type) base-type
      (string? base-type) (keyword base-type))))

(defn array-count [data]
  (cond
    (:items data) (count (:items data))
    :else (count data)))

(defn array-nth [data n]
  (cond
    (:items data) (nth (:items data) n)
    :else (nth data n)))

(defn get-xsd-datatypes []
  [:xsd/anyType :xsd/anySimpleType :xsd/anyAtomicType
   :xsd/anyURI :xsd/base64Binary :xsd/hexBinary
   :xsd/string :xsd/boolean
   :xsd/time :xsd/date :xsd/dateTime :xsd/dateTimeStamp :xsd/duration :xsd/dayTimeDuration
   :xsd/gYear :xsd/gMonth :xsd/gDay :xsd/gYearMonth :xsd/gMonthDay :xsd/yearMonthDuration
   :xsd/float :xsd/double :xsd/precisionDecimal
   :xsd/decimal :xsd/integer
   :xsd/byte :xsd/short :xsd/int :xsd/long
   :xsd/unsignedByte :xsd/unsignedShort :xsd/unsignedInt :xsd/unsignedLong
   :xsd/positiveInteger :xsd/nonNegativeInteger :xsd/negativeInteger :xsd/nonPositiveInteger])

; xsd data types hierarchy https://www.w3.org/TR/2009/WD-xmlschema11-2-20091203/type-hierarchy-200901.longdesc.html
(defn add-xsd-type-hierarchy [h]
  (-> h
      (derive :rdfs/Datatype :rdfs/Literal)
      (derive :xsd/anySimpleType :xsd/anyType)
      (derive :xsd/anyAtomicType :xsd/anySimpleType)
      (derive :xsd/decimal :xsd/anyAtomicType)
      (derive :xsd/anyURI :xsd/anyAtomicType)
      (derive :xsd/hexBinary :xsd/anyAtomicType)
      (derive :xsd/base64Binary :xsd/anyAtomicType)
      (derive :xsd/boolean :xsd/anyAtomicType)
      (derive :xsd/date :xsd/anyAtomicType)
      (derive :xsd/dateTime :xsd/anyAtomicType)
      (derive :xsd/dateTimeStamp :xsd/dateTime)
      (derive :xsd/time :xsd/anyAtomicType)
      (derive :xsd/duration :xsd/anyAtomicType)
      (derive :xsd/dayTimeDuration :xsd/duration)
      (derive :xsd/yearMonthDuration :xsd/duration)
      (derive :xsd/gDay :xsd/anyAtomicType)
      (derive :xsd/gMonth :xsd/anyAtomicType)
      (derive :xsd/gMonthDay :xsd/anyAtomicType)
      (derive :xsd/gYear :xsd/anyAtomicType)
      (derive :xsd/gYearMonth :xsd/anyAtomicType)
      (derive :xsd/integer :xsd/decimal)
      (derive :xsd/long :xsd/integer)
      (derive :xsd/int :xsd/long)
      (derive :xsd/short :xsd/int)
      (derive :xsd/byte :xsd/short)
      (derive :xsd/nonNegativeInteger :xsd/integer)
      (derive :xsd/positiveInteger :xsd/nonNegativeInteger)
      (derive :xsd/unsignedLong :xsd/nonNegativeInteger)
      (derive :xsd/unsignedInt :xsd/unsignedLong)
      (derive :xsd/unsignedShort :xsd/unsignedInt)
      (derive :xsd/unsignedByte :xsd/unsignedShort)
      (derive :xsd/nonPositiveInteger :xsd/integer)
      (derive :xsd/negativeInteger :xsd/nonPositiveInteger)
      (derive :xsd/precisionDecimal :xsd/anyAtomicType)
      (derive :xsd/double :xsd/anyAtomicType)
      (derive :xsd/float :xsd/anyAtomicType)
      (derive :xsd/string :xsd/anyAtomicType)

      ; strictly speaking xsd types are not subclasses of rdfs:Literal, but we put
      ; them in hierachy anyway.
      ;(doall (map #(derive % :rdfs/Datatype) xsd-datatypes))
      (derive :xsd/anyType :rdfs/Datatype)
      ;(derive :rdfs/Datatype :rdfs/Resource)
      ;(derive :ssoup/keyword :rdfs/Resource))
      ))

(defn get-type [engine obj]
  (cond
    (nil? obj) nil
    (keyword? obj) :ssoup/keyword
    (string? obj) :xsd/string
    (integer? obj) :xsd/integer
    (boolean? obj) :xsd/boolean
    (number? obj) :xsd/decimal
    (vector? obj) :ssoup/array
    (set? obj) :ssoup/set
    (fn? obj) :ssoup/function
    (map? obj) (if-let [t (:rdf/type obj)]
                 (cond
                   (= :ssoup/type t) obj                    ; if the argument represents a type, return itself
                   (keyword? t) t
                   (map? t) t
                   (string? t) (keyword t)
                   (set? t) t) ; multiple rdf types defined
                 :rdfs/Resource)))

(defn get-arg-type [engine arg]
  (get-type engine arg))

(defn get-arg-types [engine args]
  (if (empty? args)
    []
    (do
      (for [[_ v] (sort args)]                              ; first sort arguments by key
        (get-arg-type engine v)))))                         ; a map without a :type attribute

(defn get-type-key-from-type [engine type]
  (if (keyword? type)
    type
    (if (set? type)
      (into #{} (map #(get-type-key-from-type engine %) type)) ; case when multiple rdf types defined
      (when-let [type-key (:type-key type)]
        (condp = (get-base-type type)
          :ssoup/array (derive* engine type-key :ssoup/array)
          :ssoup/enum (derive* engine type-key :ssoup/enum))
        type-key))))

(defn get-arg-type-keys [engine args]
  (doall
    (for [type (get-arg-types engine args)]
      (get-type-key-from-type engine type))))

(defn get-all-arg-type-keys-combinations [engine args]
  (let [arg-type-keys (get-arg-type-keys engine args)
        arg-type-keys (map (fn [k] (if (set? k) k #{k})) arg-type-keys)]
    (apply combo/cartesian-product arg-type-keys)))
