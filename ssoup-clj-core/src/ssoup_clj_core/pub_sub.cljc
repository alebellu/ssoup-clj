(ns ssoup-clj-core.pub-sub
  (:require
    [clojure.core.async :as async :refer [sub pub <! chan close! >! put!]]
    [ssoup-clj-core.types :refer [dispatch-on-type]]
    [ssoup-clj-core.core :as s]))

; defprotocol IPublishSubscribe
(defmulti subscribe #_[engine session ctx topic-id chan options]
          "Subscribe to the given topic" dispatch-on-type)
(defmulti publish #_[engine ctx topic-id data]
          "Publish the provided data to all clients that had subscribed
           to the given topic" dispatch-on-type)

; defprotocol IPublishSubscribeObjects
(defmulti subscribe-to-object-updates #_[engine session ctx data-tags object-id data-pattern options]
          "Subscribe a remote client to updates on the specified object for the given data pattern" dispatch-on-type)
(defmulti publish-object-update #_[engine ctx data-tags object-id data-pattern object]
          "Publish the provided object update to all clients that had subscribed
           to receive updates on the given object" dispatch-on-type)

; defprotocol IPublishSubscribeSchema
(defmulti subscribe-to-schema-updates #_[engine session ctx data-tags options]
          "Subscribe a remote client to updates on the schema" dispatch-on-type)
(defmulti publish-schema-update #_[engine ctx data-tags schema-update]
          "Publish the provided schema update to all clients that had subscribed
           to receive updates on the schema" dispatch-on-type)
