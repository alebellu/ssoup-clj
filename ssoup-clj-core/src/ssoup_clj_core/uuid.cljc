(ns ssoup-clj-core.uuid
  #?(:cljs (:require [goog.string.StringBuffer])))

; code from https://github.com/davesann/cljs-uuid
; incorporated verbatim to get rid of warnings about uuid redefinition

#?(:cljs
(defn make-random
  "Returns a new randomly generated (version 4) cljs.core/UUID,
  like: xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx
  as per http://www.ietf.org/rfc/rfc4122.txt.
  Usage:
  (UUIDv4)  =>  #uuid \"305e764d-b451-47ae-a90d-5db782ac1f2e\"
  (type (UUIDv4)) => cljs.core/UUID"
  []
  (letfn [(f [] (.toString (rand-int 16) 16))
          (g [] (.toString  (bit-or 0x8 (bit-and 0x3 (rand-int 15))) 16))]
    (uuid (.toString
             ;(.append
             (goog.string.StringBuffer.
               (f) (f) (f) (f) (f) (f) (f) (f) "-" (f) (f) (f) (f)
               "-4" (f) (f) (f) "-" (g) (f) (f) (f) "-"
               (f) (f) (f) (f) (f) (f) (f) (f) (f) (f) (f) (f)))))))

#?(:clj
(defn make-random
  "make a version 4 (random UUID as per http://www.ietf.org/rfc/rfc4122.txt"
  []
  (java.util.UUID/randomUUID)))

(defn make-random-str []
  (str (make-random)))

(defn make-random-keyword [namespace]
  (keyword namespace (make-random-str)))
