(ns ssoup-clj-core.state)

; -------------------------------------------------------
; Utilities to manage the engine state
; -------------------------------------------------------

(defonce engine-atom (atom nil))

(defn set-engine-state! [engine]
  (reset! engine-atom engine))

(defn get-engine []
  @engine-atom)
