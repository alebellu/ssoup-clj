(ns ^:figwheel-always ssoup-clj-core.remote-ds
  (:require
    [clojure.core.async :as async :refer [<! chan >! put! close! pipeline]]
    #?(:clj
    [clojure.core.async :refer [go <!!]])
    [ssoup-clj-core.core :as s]
    [ssoup-clj-core.datasources :as ds]
    [ssoup-clj-core.connector :as connector]
    [ssoup-clj-core.types :refer [dispatch-on-type]]
    [ssoup-rdf.core :as rdf]
    [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)]
    [ssoup-clj-core.utils :as utils :refer [gretval]]
    [ssoup-clj-core.state :as state]
    [ssoup-clj-core.uuid :as uuid])
  #?(:cljs (:require-macros [cljs.core.async.macros :refer [go]])))

;---------------------------------------------------------------------------
; Remotizable datasource implementation
;---------------------------------------------------------------------------

(defmethod ds/sync-ds :ssoup/datasource [ds ctx objects-to-update objects-to-fetch options]
  (go
    (when objects-to-update
      (loop [objects-to-update objects-to-update]
        (when-not (empty? objects-to-update)
          (let [object-to-update (first objects-to-update)]
            (<! (ds/save-object ds ctx nil object-to-update options))
            (recur (rest objects-to-update))))))
    (when objects-to-fetch
      (loop [objects-to-fetch objects-to-fetch
             objects-fetched {}]
        (when (empty? objects-to-fetch)
          objects-fetched
          (let [object-to-fetch (first objects-to-fetch)
                {object-id    :ssoup/object-id
                 data-pattern :ssoup/data-pattern} object-to-fetch
                object-fetched (<! (ds/get-object-with-meta ds ctx object-id data-pattern options))]
            (recur (rest objects-to-fetch) (conj objects-fetched object-fetched))))))))

;---------------------------------------------------------------------------
; Remotized datasource implementation
;---------------------------------------------------------------------------

(derive :ssoup-ds/remotized-datasource :ssoup/datasource)

(def sync-state (atom :synced))                             ; One of :syncing, :synced, :sync-error

(defn new-remote-datasource
  "A remote datasource abstracts the link to a datasource connected to a remote SSOUP
  engine.

  The local datastore can be kept in sync with the remote in 2 ways:
  - via a direct connection: the remote datastore will notify all replica whenever there
  is new data (:fetch-mode = :pub-sub).
  - via polling: the local datastore will periodically connect to the remote datastore
  to check for updates (:fetch-mode = :batch).

  Updates from the local datastore can be sent to the remote datastore in 2 ways:
  - right away: the local datastore will send the changes to the remote datastore whenever there
  is new data (:update-mode = :direct).
  - in batches: the local datastore will periodically connect to the remote datastore
  to send the updates (:update-mode = :batch).

  The local datasource keeps a copy of the objects retrieved from the remote datasource.
  The local data is structured like this:
  {:objects-to-fetch  #{{:id        ; id of the object to fetch
                         :timestamp ; the timestamp of the last known object state,or nil if unknown}}
   :objects-to-update #{set of object ids}
   :objects
    {:object-id {
    ; ----------------------------------------------------------------------------------------------
    ; :local-xyz properties represent a locally-modified version of the object,
    ; that is to be synchronized with the remote datasource, and the state of the synchronization itself.
    ; ----------------------------------------------------------------------------------------------
    :local-timestamp     ; The timestamp at which the object was created/updated/deleted locally
    :local-state         ; One of :created, :updated, :deleted
    :local-pattern       ; The pattern representing the data structure of the local object
    :local-data          ; The object modified locally
    :local-sync-conflict ; Boolean indicating if there is a conflict

    ; ----------------------------------------------------------------------------------------------
    ; :remote-xyz properties represent a copy of the remote object and the state of the copy itself.
    ; ----------------------------------------------------------------------------------------------
    :remote-timestamp  ; The timestamp at which the object was retrieved (or not found..) from the remote datastore
    :remote-pattern    ; The pattern representing the data structure of the object requested remotely
    :remote-state      ; One of :found, :not-found
    :remote-data       ; The object retrieved from remote datasource
    },
   :lists
   {
     :list-id {
       :pages...
     }
   }
  }
  "
  [ds-name tags remote-engine-id options]
  {:type             :ssoup-ds/remotized-datasource
   :name             ds-name
   :data-tags        tags
   :data             (atom {})
   :schema           (atom nil)
   :schema-update-channel (atom nil)
   :remote-engine-id remote-engine-id
   :options          options})

; -------------------------------------------------------
; IDataSource
; -------------------------------------------------------

(defmethod ds/init-ds :ssoup-ds/remotized-datasource [ds]
  (let [options (:options ds)]
    (when (or (= :batch (:update-mode options))
              (= :batch (:fetch-mode options)))
      (let [interval-handle (utils/set-interval #(ds/do-sync ds nil)
                                                (or (:sync-interval options)
                                                    ; sync every 5 seconds by default
                                                    5000))]
        (assoc ds :interval-handle interval-handle))))
  ds)

(defmethod ds/tear-down :ssoup-ds/remotized-datasource [ds]
  (when-let [interval-handle (:interval-handle ds)]
    (utils/clear-interval interval-handle)
    (dissoc ds :interval-handle)))

(defn- make-remote-meta [object-id object-with-meta]
  (when-not (= (:ssoup/not-changed object-with-meta))
    [object-id (if object-with-meta
                 {:remote-timestamp (:ssoup/timestamp object-with-meta)
                  :remote-pattern   (:ssoup/pattern object-with-meta)
                  :remote-state     :found
                  :remote-data      (:ssoup/data object-with-meta)}
                 {:remote-state :not-found})]))

(defmethod ds/do-sync :ssoup-ds/remotized-datasource [ds ctx]
  "Sends updates to the remote datasource and retrieves new info"
  (go
    (reset! sync-state :syncing)
    (let [remote-objects
          (<! (s/exec-async-remote (:engine ctx) (:session ctx)
                                   {:ssoup/remote-engine-id (:remote-engine-id ds)
                                    :ssoup/classes          #{:ssoup/sync-datasource}
                                    :ssoup/args             {:ssoup/objects-to-update (:objects-to-update @(:data ds))
                                                             :ssoup/objects-to-fetch  (:objects-to-fetch @(:data ds))}}))
          objects-with-meta (when remote-objects (into {} (map make-remote-meta remote-objects)))]
      (if objects-with-meta
        (do (swap! (:data ds) merge {:objects objects-with-meta})
            (reset! sync-state :synced))
        (reset! sync-state :sync-error)))))

(defn- forward-request-to-remote-engine [ds ctx class args options]
  (let [engine (state/get-engine)]
    (go (let [retval (<! (s/exec-async-remote engine (:session ctx)
                                              {:ssoup/remote-engine-id (:remote-engine-id ds)
                                               :ssoup/classes          #{class}
                                               :ssoup/flavor           :ssoup/default-flavor
                                               :ssoup/args             args
                                               :ssoup/options          options}))]
          (:return-value retval)))))

(defn on-object-update [ds object-with-meta]
  (swap! (:data ds) assoc-in) [:objects (:id object-with-meta)] object-with-meta)

(defn- subscribe-to-object-updates [ds ctx object-id data-pattern]
  (if-let [obj-chan (get-in @(:data ds) [:objects-to-fetch object-id data-pattern])]
    obj-chan
    (let [obj-chan (chan)]
      (swap! (:data ds) assoc-in [:objects-to-fetch object-id data-pattern] obj-chan)
      (when (= :pub-sub (:fetch-mode (:options ds)))
        (let [engine (state/get-engine)
              remote-engine-id (:remote-engine-id ds)
              remote-engine (s/get-remote-engine-by-id engine remote-engine-id)
              connector (:connector-client remote-engine)
              data-tags (:data-tags ds)]
          (connector/subscribe-to-object-updates connector ctx data-tags object-id data-pattern
                                          {:update-channel obj-chan
                                           :on-object-update (partial on-object-update ds)
                                           :send-initial-update true})))
      obj-chan)))

(defn on-schema-update [ds schema-update]
  (swap! (:schema ds) merge schema-update)
  (debug "schema-update " @(:schema ds)))

(defn- subscribe-to-schema-updates [ds ctx]
  (if-let [schema-chan @(:schema-update-channel ds)]
    schema-chan
    (let [schema-chan (chan)]
      (reset! (:schema-update-channel ds) schema-chan)
      (when (= :pub-sub (:fetch-mode (:options ds)))
        (let [engine (state/get-engine)
              remote-engine-id (:remote-engine-id ds)
              remote-engine (s/get-remote-engine-by-id engine remote-engine-id)
              connector (:connector-client remote-engine)
              data-tags (:data-tags ds)]
          (connector/subscribe-to-schema-updates connector ctx data-tags
                                                 {:update-channel schema-chan
                                                  :on-schema-update (partial on-schema-update ds)
                                                  :send-initial-update true})))
      schema-chan)))

(defmethod ds/get-object :ssoup-ds/remotized-datasource
  [ds ctx object-id data-pattern options]
  "Returns the object from the local datastore, if there is a record for it.
   Otherwise adds a new record for the object, which will be requested to the server."
  (let [object-with-meta (get-in @(:data ds) [:objects object-id data-pattern])]
    (if object-with-meta
      (go object-with-meta)
      (subscribe-to-object-updates ds ctx object-id data-pattern))))

(defmethod ds/get-schema :ssoup-ds/remotized-datasource
  [ds ctx options]
  "Returns the schema from the local datastore, if there is a record for it.
   Otherwise adds a new record for the schema, which will be requested to the server."
  (let [schema @(:schema ds)]
    (debug "schema " schema)
    (if schema
      (go schema)
      (subscribe-to-schema-updates ds ctx))))

(defmethod ds/list-objects-with-property :ssoup-ds/remotized-datasource
  [ds ctx property-id property-value data-pattern options]
  ; for now forward this kind of requests to the server
  (forward-request-to-remote-engine ds ctx :ssoup/list-objects-with-property
                                             {:ssoup/data-tags      (:data-tags ds)
                                              :ssoup/property-id    property-id
                                              :ssoup/property-value property-value
                                              :ssoup/data-pattern   data-pattern}
                                             options))

(defmethod ds/save-object :ssoup-ds/remotized-datasource [ds ctx rdf-ctx object options]
  (let [object-id (or (:id object)
                      (uuid/make-random-keyword :ssoup))
        object (assoc object :id object-id)]
    (when-not (contains? (:objects-to-update @(:data ds)) object-id)
      (debug "1111")
      (swap! (:data ds) update :objects-to-update (fn [objs-to-update]
                                                    (if objs-to-update
                                                      (conj objs-to-update object-id)
                                                      #{object-id}))))
    (when (= :direct (-> ds :options :update-mode))
      (debug "2222")
      (forward-request-to-remote-engine ds ctx :ssoup/save-object
                                        {:ssoup/data-tags (:data-tags ds)
                                         :ssoup/object    object}
                                        options))
    (subscribe-to-object-updates ds ctx (:id object) ["*" 1000])))

(defmethod ds/load-object-from-file :ssoup-ds/remotized-datasource [ds ctx filename options]
  (error "load-object-from-file currently not supported for remote datasources"))

(defmethod ds/get-type-schema :ssoup/datasource [schema-ds ctx type options]
  (go
    (let [schema (<! (ds/get-schema schema-ds ctx options))]
      {:rdf/context (get schema :rdf/context)
       :ssoup/schema (get-in schema [:ssoup/schema type])})))

(defmethod ds/get-super-types :ssoup-ds/remotized-datasource [schema-ds ctx type options]
  (go
    (let [type-schema (<! (ds/get-type-schema schema-ds ctx type options))]
      (get-in type-schema [:ssoup/schema :ssoup/super-types]))))
