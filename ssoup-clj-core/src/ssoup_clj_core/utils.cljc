(ns ssoup-clj-core.utils
  #?(:clj
     (import java.io.StringWriter
             (java.util.concurrent Executors TimeUnit ScheduledFuture)))
  (:require
    [clojure.string]
    [clojure.core.async :as async :refer [<! chan close! put! pipeline]]
    [ssoup-clj-core.types :refer [get-type]]
    #?(:clj
    [clojure.pprint :refer [pprint]])
    #?(:cljs [cljs.pprint :refer [pprint]])
    #?(:clj
    [clojure.core.async :refer [go]])
    [clojure.core.async.impl.protocols :refer [Channel]])
  #?(:cljs (:require-macros [cljs.core.async.macros :refer [go]])))

; -------------------------------------------------------
; Utility functions
; -------------------------------------------------------

(defn channel? [x]
  (and (not (nil? x))
       (satisfies? Channel x)))

(defn exception? [v]
  #?(:clj (instance? Throwable v))
  #?(:cljs (instance? js/Error v)))

(defn if-err-throw [v]
  (when (exception? v)
    (throw v))
  (when (= :ssoup/return-value (:rdf/type v))
    (when-let [e (:exception v)]
      (throw e)))
  v)

(defn exception [msg]
  #?(:clj (Exception. msg))
  #?(:cljs (js/Error. msg)))

(defn as-set [o]
  (if (or (nil? o) (set? o)) o #{o}))

(defn retval [engine session val]
  "Defines a compund value that includes the new immutable engine (possibly in a new
  state with respect to the beginning of the call) and a return value or an error."
  (let [rv {:rdf/type :ssoup/return-value
            :engine   engine
            :session  session}]
    (if (exception? val)
      (assoc rv :exception val)
      (assoc rv :return-value val))))

(defn gretval [engine retval]
  "Checks whether a returned value is of the special type :ssoup/return-value and
   return the actual return value in that case"
  (if (channel? retval)
    (let [ret-chan (chan)
          rv (fn [r]
               (if (= :ssoup/return-value (get-type engine r))
                 r
                 (gretval engine r)))
          rvt (fn [reducing]
                (fn [result r]
                  (reducing result (rv r))))]
      (pipeline 1 ret-chan rvt retval))
    (if (= :ssoup/return-value (get-type engine retval))
      (:return-value retval)
      retval)))

(defn async-err [msg]
  (go
    #?(:clj (Throwable. msg))
    #?(:cljs (js/Error. msg))))

#?(:clj
   (defmacro <? [ch]
     `(do (if-err-throw (~'<! ~ch)))))

#?(:clj
   (defmacro assert-args
     [& pairs]
     `(do (when-not ~(first pairs)
            (throw (IllegalArgumentException.
                     (str (first ~'&form) " requires " ~(second pairs) " in " ~'*ns* ":" (:line (meta ~'&form))))))
          ~(let [more (nnext pairs)]
             (when more
               (list* `assert-args more))))))

; argument-list->argument-map (adapted from Leinegen source code)
(defn argument-list->argument-map
  [args]
  (let [keys (map first (partition 2 args))
        unique-keys (set keys)]
    (if (= (count keys) (count unique-keys))
      (apply hash-map args)
      (let [duplicates (->> (frequencies keys)
                            (remove #(> 2 (val %)))
                            (map first))]
        (throw
          (#?(:clj IllegalArgumentException.) #?(:cljs js/Error)
                                              (str "Duplicate keys: "
                                                   (clojure.string/join ", " duplicates))))))))

(defn str-pprint [o]
  #?(:clj
     (let [w (StringWriter.)]
       (pprint o w)
       (.toString w)))
  #?(:cljs
     (with-out-str (pprint o))))

(defn set-interval [callback ms]
  #?(:clj (let [executor (Executors/newSingleThreadScheduledExecutor)]
            (.scheduleAtFixedRate executor callback 0 3 TimeUnit/MILLISECONDS)))
  #?(:cljs (.setInterval js/window ms callback)))

(defn clear-interval [interval-handle]
  #?(:clj (.cancel ^ScheduledFuture interval-handle true))
  #?(:cljs (.clearInterval interval-handle)))

(defn string-contains? [str sub-str]
  #?(:clj (.contains str sub-str))
  #?(:cljs ()))
