(ns ssoup-clj-core.modules
  (:require [clojure.string]
            [ssoup-clj-core.utils :as utils]))

; -------------------------------------------------------
; Module registration service
; -------------------------------------------------------

(defmulti register-module (fn [module-name version type options] type))
(defmulti get-modules (fn [module-name version type options] type))

; -------------------------------------------------------
; Module definition macro
; (adapted from Leinegen source code)
; -------------------------------------------------------

#?(:clj
  (defmacro defmodule
    "Defines a SSOUP module"
    [module-name version type & args]
    `(let [args# ~(utils/argument-list->argument-map args)]
       (ssoup-clj-core.modules/register-module ~(name module-name) ~version ~type args#))))
