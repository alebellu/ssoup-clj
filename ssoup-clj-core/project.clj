(defproject ssoup-clj-core "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.293"]
                 [org.clojure/core.cache "0.6.5"]
                 [org.clojure/core.memoize "0.5.8"]
                 [org.clojure/core.async "0.2.385"]
                 [com.taoensso/timbre "4.2.1"]
                 [clj-time "0.11.0"]
                 [com.andrewmcveigh/cljs-time "0.3.14"]
                 [ssoup-rdf "0.1.0-SNAPSHOT"]]

  :plugins [[lein-cljsbuild "1.1.4"]]

  :source-paths ["src"]

  :repositories [["releases" {:url "https://clojars.org/repo/"
    :username :env/CLOJARS_USERNAME
    :password :env/CLOJARS_PASSWORD}]])
