; Modules can either be included statically server side or dynamically client side
(ns ssoup-clj-client.modules
  #?(:clj
      (:require
        [taoensso.timbre :as timbre :refer (log trace debug info warn error fatal)]
        [ssoup-clj-core.modules :as sm]))
  #?(:cljs
     (:require
       [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]
       [ssoup-clj-core.modules :as sm])))

; -------------------------------------------------------
; Modules global registry
; -------------------------------------------------------

(def modules (atom {}))

; -------------------------------------------------------
; Module registration service
; -------------------------------------------------------

(defmethod sm/register-module :ssoup-clj-client
  [module-name version _ options]
  (info (str "Registering module " module-name ":" version))
  (swap! modules assoc module-name (assoc options :version version)))

; -------------------------------------------------------
; Dependencies
; -------------------------------------------------------

(defn compute-js-dependencies
  "Resolve all js dependencies and eliminates duplicates.
   Returns a sequence of urls, in the order in which they should be loaded."
  []
  (remove nil?
    (flatten
      (for [[module-name options] @modules]
        (for [dep (:dependencies options)]
          (let [depname (first dep)
                version (second dep)
                depoptions (nth dep 2)
                depjs (:js depoptions)]
            depjs))))))

(defn compute-css-dependencies
  "Resolve all css dependencies and eliminates duplicates.
   Returns a sequence of urls, in the order in which they should be loaded."
  []
  (remove nil?
    (flatten
      (for [[module-name options] @modules]
        [(for [dep (:dependencies options)]
               (let [depname (first dep)
                     version (second dep)
                     depoptions (nth dep 2)
                     css (:css depoptions)]
                 css))
             (:css options)]))))

(defn compute-scs-rules
  "Resolve all scs rules
   Returns a sequence of rules,in the order in which they should be considered."
  []
  (remove nil?
          (flatten
            (for [[module-name options] @modules]
              ((:scs-rules options))))))
