(ns ^:figwheel-always ssoup-clj-client.base-ui-services
  (:require [ssoup-clj-core.core :as s]
            [ssoup-clj-client.core :as sc]
            [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]))

; -------------------------------------------------------
; Library functions implementation.
; -------------------------------------------------------


; -------------------------------------------------------
; Library registration.
; -------------------------------------------------------
(defn- init-library [])

(s/register-library!
  {:library-id      :ssoup/base-ui-services
   :engine-type     :ssoup/ui-engine
   :dependencies [:ssoup/base-services]
   :ssoup/services  [{:fname     :ssoup/set-template
                      :handler   (fn [ui-engine ui-session ctx args options]
                                   (let [template-id (:ssoup/template-id args)
                                         template (sc/get-template ui-engine
                                                                template-id)
                                         template-body (:body template)
                                         container-id (if (:ssoup/is-root options)
                                                        :ssoup/root-container
                                                        (:ssoup/container options))]
                                     (sc/set-container-content ui-engine
                                                               ui-session
                                                               container-id template-body)
                                     true))
                      :flavor    :ssoup/default-theme
                      :user-role :ssoup/any-user
                      :classes   #{:ssoup/set-template}
                      :arg-types {:ssoup/template-id :ssoup/keyword}}]
   :init-library-fn init-library})
