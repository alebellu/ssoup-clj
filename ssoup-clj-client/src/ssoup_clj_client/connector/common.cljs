(ns ^:figwheel-always ssoup-clj-client.connector.common
  (:require [cljs.core.async :as async :refer [<! chan close! >!]]
            [cljs-http.client :as http]
            [ssoup-clj-core.core :as s]
            [ssoup-clj-core.connector :as connector]
            [ssoup-clj-core.pub-sub :as ps]
            [ssoup-clj-core.utils :refer [as-set]]
            [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]
            [ssoup-clj-core.state :as state])
  (:require-macros [cljs.core.async.macros :refer [go]]))

; -------------------------------------------------------
; IConnector
; -------------------------------------------------------
(derive :ssoup/connector-client :ssoup/connector)

(defmethod connector/exec :ssoup/websocket-cljs-connector-client
  [connector ctx user classes flavor args options]
  (connector/send-message-and-wait-for-response connector ctx nil
                                                :ssoup/exec-message
                                                {:classes (for [class classes] (str (namespace class) "/" (name class)))
                                                 :flavor  (str (namespace flavor) "/" (name flavor))
                                                 :args    (if (empty? args) [] args)
                                                 :options options}
                                                {}))

(defmethod connector/subscribe :ssoup/websocket-cljs-connector-client
  [connector ctx topic-id]
  (connector/send-or-enqueue-message connector ctx nil
                                     :ssoup/subscribe-message
                                     {:topic-id topic-id}
                                     {}))

(defmethod connector/topic-on-message :ssoup/connector-client [connector ctx topic-id message]
  (let [engine (state/get-engine)
        {session :session} ctx]
    (ps/publish engine ctx topic-id message)))

(defmethod connector/subscribe-to-object-updates :ssoup/connector-client
  [connector ctx data-tags object-id data-pattern options]
  (let [engine (state/get-engine)
        {session :session} ctx]
    (ps/subscribe-to-object-updates engine session ctx data-tags object-id data-pattern
                                    {:update-channel   (:update-channel options)
                                     :on-object-update (:on-object-update options)})
    (connector/send-or-enqueue-message connector ctx nil
                                       :ssoup/subscribe-to-object-updates-message
                                       {:ssoup/data-tags     data-tags
                                        :ssoup/object-id     object-id
                                        :ssoup/data-pattern  data-pattern
                                        :send-initial-update (:send-initial-update options)}
                                       {})))

(defmethod connector/on-object-update :ssoup/connector-client [connector ctx data-tags object-id data-pattern object]
  (let [engine (state/get-engine)
        {session :session} ctx]
    (ps/publish-object-update engine ctx data-tags object-id data-pattern object)))

(defmethod connector/subscribe-to-schema-updates :ssoup/connector-client
  [connector ctx data-tags options]
  (let [engine (state/get-engine)
        {session :session} ctx]
    (ps/subscribe-to-schema-updates engine session ctx data-tags
                                    {:update-channel   (:update-channel options)
                                     :on-schema-update (:on-schema-update options)})
    (connector/send-message connector ctx nil
                            :ssoup/subscribe-to-schema-updates-message
                            {:ssoup/data-tags     data-tags
                             :send-initial-update (:send-initial-update options)}
                            {})))

(defmethod connector/on-schema-update :ssoup/connector-client [connector ctx data-tags schema-update]
  (let [engine (state/get-engine)
        {session :session} ctx]
    (ps/publish-schema-update engine ctx data-tags schema-update)))

(defmethod connector/dispatch-message :ssoup/connector-client
  [connector ctx msg-type msg-body]
  (when (= :ssoup/topic-message msg-type)
    (let [topic-id (:topic-id ctx)]
      (connector/topic-on-message connector ctx topic-id msg-body)))
  (when (= :ssoup/object-update-message msg-type)
    (let [data-tags (:data-tags ctx)
          object-id (:object-id ctx)
          data-pattern (:data-pattern ctx)]
      (connector/on-object-update connector ctx data-tags object-id data-pattern msg-body)))
  (when (= :ssoup/schema-update-message msg-type)
    (let [data-tags (:data-tags ctx)]
      (connector/on-schema-update connector ctx data-tags msg-body))))

(defn new-connector-client []
  (let [connector-client (connector/new-connector)]
    (merge connector-client
           {:default-destination :ssoup-destination/server})))
