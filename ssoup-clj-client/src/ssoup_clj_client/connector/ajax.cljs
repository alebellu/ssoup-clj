(ns ^:figwheel-always ssoup-clj-client.connector.ajax
  (:require [cljs.core.async :as async :refer [<! chan close! >!]]
            [cljs-http.client :as http]
            [ssoup-clj-core.core :as s]
            [ssoup-clj-core.connector :as connector]
            [ssoup-clj-client.connector.common :as common]
            [ssoup-clj-core.utils :refer [as-set]]
            [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)])
  (:require-macros [cljs.core.async.macros :refer [go]]))

#_(defn- exec [connector user classes flavor args options]
    (go
      (debug "-> Ajax call to " classes " flavor " flavor " args " args)
      (let [exec-url (:exec-url connector)
            _ (debug "exec-url " exec-url)
            classes (as-set classes)
            resp (<! (http/post exec-url
                                {:transit-params {:classes (for [class classes] (str (namespace class) "/" (name class)))
                                                  :flavor  (str (namespace flavor) "/" (name flavor))
                                                  :args    (if (empty? args) [] args)
                                                  :options options}}))]
        (if (= (:status resp) 200)
          (do (debug "<- Ajax call to " classes " flavor " flavor " args " args ": " resp)
              (:body resp))
          (let [msg (str "Ajax: " (:body resp))]
            (debug "<-! Ajax call to " classes " flavor " flavor " args " args ": " resp)
            (js/Error. msg))))))

(defn get-current-user [options]
  (go
    (let [response (<! (http/get "user/current"))]
      (when (= 200 (:status response))
        (let [user (:body response)]
          (when-let [on-success (:on-success options)]
            (on-success user))))
      response)))

(defn login [user pass options]
  (go
    (let [response (<! (http/post "login" {:transit-params {:username user
                                                            :password pass}}))]
      (when (= 200 (:status response))
        (when-let [on-success (:on-success options)]
          (on-success user)))
      response)))

(defn logout [options]
  (http/post "logout"))

; -------------------------------------------------------
; IConnector
; -------------------------------------------------------
(derive :ssoup/ajax-connector-client :ssoup/connector-client)

(defmethod connector/init :ssoup/ajax-connector-client
  [connector ctx]
  (go))

(defmethod connector/tear-down :ssoup/ajax-connector-client
  [connector ctx])

(defmethod connector/get-current-user :ssoup/ajax-connector-client
  [connector ctx options]
  (get-current-user options))

(defmethod connector/login :ssoup/ajax-connector-client
  [connector ctx user password options]
  (login user password options))

(defmethod connector/logout :ssoup/ajax-connector-client
  [connector ctx options]
  (logout options))

(defmethod connector/subscribe :ssoup/ajax-connector-client
  [connector ctx topic-id]
  (warn "Subscribe not implemented for connector ssoup:ajax-connector-client"))

(defmethod connector/subscribe-to-object-updates :ssoup/ajax-connector-client
  [connector ctx topic-id]
  (warn "Subscribe-to-object-updates not implemented for connector ssoup:ajax-connector-client"))

(defmethod connector/send-message :ssoup/ajax-connector-client
  [connector ctx destination msg-type msg-body headers]
  (go
    (debug "-> Ajax call to " msg-type ", body " msg-body)
    (let [exec-url (:exec-url connector)
          resp (<! (http/post exec-url
                              {:transit-params msg-body}))]
      (if (= (:status resp) 200)
        (do (debug "<- Ajax call to " msg-type ", body " msg-body ": " resp)
            true)
        (let [msg (str "Ajax: " (:body resp))]
          (debug "<-! Ajax call to " msg-type ", body " msg-body ": " resp)
          (js/Error. msg)
          false)))))

(defn new-ajax-connector-client [exec-url]
  (let [connector-client (common/new-connector-client)]
    (merge connector-client
           {:type     :ssoup/ajax-connector-client
            :exec-url exec-url})))
