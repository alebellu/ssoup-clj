(ns ^:figwheel-always ssoup-clj-client.connector.websocket
  (:require [cljs.core.async :as async :refer [sub pub <! chan close! >! put!]]
            [cljs-http.client :as http]
            [cognitect.transit :as t]
            [goog.net.cookies]
            [ssoup-clj-core.core :as s]
            [ssoup-clj-core.connector :as connector]
            [ssoup-clj-client.connector.common :as common]
            [ssoup-clj-client.connector.ajax :as ajax]
            [ssoup-clj-core.utils :refer [as-set]]
            [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]
            [ssoup-clj-core.uuid :as uuid])
  (:require-macros [cljs.core.async.macros :refer [go]]))

; From https://github.com/pedestal/pedestal/tree/master/samples/jetty-web-sockets :
; w = new WebSocket("ws://localhost:8080/ws")
; w.onmessage = function(e) { console.log(e.data); }
; w.onclose = function(e) {  console.log("The connection to the server has closed."); }
; w.send("Hello from the Client!");

(defn- get-cookie
  "gets the value at the key (as string)"
  [k]
  (.get goog.net.cookies k))

(defonce ws-conn (atom nil))
(defonce ws-cid (atom nil))
(defonce call-map (atom {}))

(def check-connection-interval 1000)

(defn- ensure-websocket-connection [connector ctx])

(defn- send-string [msg])
(defn- send [msg-type msg-body headers])

(defn- onopen [connector w e]
  (reset! ws-conn w)
  (send :ssoup/open-request "" {}))

(defn- open-ack [ctx cid]
  (reset! ws-cid cid)
  (debug "open-ack signal received from server. Assigned cid: " cid)
  (when-let [engine (:engine ctx)]
    (s/on-notification engine {:id             :connection-status
                               :ssoup/severity :info
                               :ssoup/message  "Connection to the server established"}))
  :open-acknoledged)

(defn- onmessage [connector ctx e]
  (let [tr (t/reader :json)
        msg (t/read tr (.-data e))
        cid (-> msg :header :cid)
        call-id (-> msg :header :call-id)
        msg-type (-> msg :header :type)
        msg-body (:body msg)
        headers (:header msg)
        message-ctx headers]
    (if (= :ssoup/open-ack msg-type)
      (open-ack ctx cid)
      (do
        (when call-id
          (swap! call-map assoc call-id msg))
        (debug "Received from server: " msg)
        (connector/dispatch-message connector message-ctx msg-type msg-body)))))

(defn- onclose [connection ctx e]
  "e.code: https://developer.mozilla.org/en/docs/Web/API/CloseEvent
    1000: CLOSE_NORMAL
    1001: CLOSE_GOING_AWAY: The endpoint is going away, either because of a server
                            failure or because the browser is navigating away
                            from the page that opened the connection.
                            It also happens in case of timeout.
    1005: CLOSE_NO_STATUS : Indicates that no status code was provided even though one was expected.
                            Happens when the client closes the connection.
    1006: CLOSE_ABNORMAL  : Server or network crashed"
  (reset! ws-conn nil)
  (let [code (.-code e)
        engine (:engine ctx)]
    (debug "web socket connection closed: " code)
    (when engine
      (s/on-notification engine {:id             :connection-status
                                 :ssoup/severity :error
                                 :ssoup/message "Connection to the server closed"}))
    (when (= 1001 code)
      ; try to reconnect
      (ensure-websocket-connection connection ctx))
    (when (= 1006 code)
      (warn "Connection to the server terminated abnormally")
      ; try to reconnect
      (ensure-websocket-connection connection ctx))))

(defn- onerror [connector ctx e]
  (reset! ws-conn nil)
  (let [code (.-code e)
        engine (:engine ctx)]
    (debug "web socket connection error: " code)
    (when engine
      (s/on-notification engine {:id             :connection-status
                                 :ssoup/severity :error
                                 :ssoup/message "An socket connection error occurred"}))))

(defn- check-connection
  "This function checks the state of the web socket connection on the client.
   In case the connection is closing, a new connection is open, otherwise
   a new check is scheduled.
   This check is needed because the client does not always get a
   notification when the connection is closed, for example in an abnormal way
   on the server side"
  [connector ctx]
  (when-let [conn @ws-conn]
    (let [ready-state (.-readyState conn)]
      ; readyState
      ; CONNECTING	0	The connection is not yet open.
      ; OPEN        1	The connection is open and ready to communicate.
      ; CLOSING     2	The connection is in the process of closing.
      ; CLOSED      3	The connection is closed or couldn't be opened.
      #_(debug "ready state " ready-state)
      (if (or (= 2 ready-state)
              (= 3 ready-state))
        (do
          (reset! ws-conn nil)
          (ensure-websocket-connection connector ctx))
        (js/setTimeout (partial check-connection connector ctx)
                       check-connection-interval)))))

(defn- ensure-websocket-connection [connector ctx]
  (let [conn-chan (chan)]
    (if @ws-conn
      (put! conn-chan @ws-conn)
      (let [websocket-url (:websocket-url connector)
            w (window/WebSocket. websocket-url)]
        (debug "websocket-url " websocket-url)
        (set! (.-onopen w) (partial onopen connector w))
        (set! (.-onmessage w) (fn [e]
                                (when (= (onmessage connector ctx e)
                                         :open-acknoledged)
                                  (put! conn-chan w))))
        (set! (.-onclose w) (partial onclose connector ctx))
        (set! (.-onerror w) (partial onerror connector ctx))
        (js/setTimeout (partial check-connection connector ctx)
                       check-connection-interval)))
    conn-chan))

(defn- send-string [msg]
  (let [w @ws-conn]
    (.send w msg)))

(defn- send [msg-type msg-body headers]
  (let [w @ws-conn
        cid @ws-cid]
    (if w
      (let [tw (t/writer :json)
            headers (if cid (assoc headers :cid cid) headers)
            headers (assoc headers :type   msg-type)
            payload (t/write tw {:header headers
                                 :body   msg-body})]
        (.send w payload)
        true)
      ; no connection, could not send the message,return false
      false)))

(defn- send-and-catch-response [msg-type msg-body headers]
  (let [call-id (uuid/make-random-str)
        response-channel (chan)]
    (add-watch call-map call-id (fn [key a old-val new-val]
                                  (when-let [response (get new-val call-id)]
                                    (put! response-channel response)
                                    (swap! call-map dissoc call-id)
                                    (remove-watch call-map call-id))))
    (send msg-type msg-body (merge headers {:call-id call-id}))
    response-channel))

(defn- close-connection []
  (when @ws-conn
    (.close @ws-conn)
    (reset! ws-conn nil)))

; -------------------------------------------------------
; IConnector
; -------------------------------------------------------
(derive :ssoup/websocket-cljs-connector-client :ssoup/connector-client)

(defmethod connector/init :ssoup/websocket-cljs-connector-client
  [connector ctx]
  (ensure-websocket-connection connector ctx))

(defmethod connector/tear-down :ssoup/websocket-cljs-connector-client
  [connector ctx])

; use plain http calls for authorization

(defmethod connector/get-current-user :ssoup/websocket-cljs-connector-client
  [connector ctx options]
  (ajax/get-current-user options))

(defmethod connector/login :ssoup/websocket-cljs-connector-client
  [connector ctx user password options]
  (ajax/login user password
              (merge options
                     {:on-success (fn [user]
                                    ; drop the current websocket after login
                                    (close-connection))})))

(defmethod connector/logout :ssoup/websocket-cljs-connector-client
  [connector ctx options]
  (ajax/logout (merge options
                      {:on-success (fn [user]
                                     ; drop the websocket after logout
                                     (close-connection))})))

(defmethod connector/send-message :ssoup/websocket-cljs-connector-client
  [connector ctx destination msg-type msg-body headers]
  (go
    (let [w (<! (ensure-websocket-connection connector ctx))]
      (send msg-type msg-body headers))))

(defmethod connector/send-message-and-wait-for-response :ssoup/websocket-cljs-connector-client
  [connector ctx destination msg-type msg-body headers]
  (go
    (debug "-> Websocket call to " msg-type ", body " msg-body)
    (let [; cookie sid cannot be retrieved via javascript because it is a http only cookie.
          ;session-cookie (get-cookie "SID")
          w (<! (ensure-websocket-connection connector ctx))
          resp (<! (send-and-catch-response
                     msg-type
                     msg-body
                     headers))
          {resp-header :header
           resp-body   :body} resp]
      (if (= (:status resp-header) :ok)
        (do (debug "<- Websocket call to " msg-type ", body " msg-body ": " resp-body)
            resp-body)
        (let [msg (str "Websocket: " (:error-text resp-header))]
          (debug "<-! Websocket call to " msg-type ", body " msg-body ": " msg)
          (js/Error. msg))))))

(defn new-websocket-cljs-connector-client [options]
  (let [connector-client (common/new-connector-client)]
    (merge connector-client
           options
           {:type :ssoup/websocket-cljs-connector-client})))
