(ns ^:figwheel-always ssoup-clj-client.core
  (:require
    [cljs.core.async :as async :refer [<! chan close! put!]]
    [cljs.core.async.impl.protocols :refer [Channel]]
    [taoensso.timbre :as timbre :refer [log trace debug info warn error fatal]]
    [ssoup-clj-core.core :as s]
    [ssoup-clj-core.types :refer [get-type atomic? ssoup-cast box unbox dispatch-on-type]]
    [ssoup-clj-core.utils :refer [if-err-throw as-set retval gretval]]
    [ssoup-clj-core.uuid :as uuid]
    [ssoup-clj-core.pub-sub-impl]                           ; don't remove, needed to require multimethods implementation
    [ssoup-clj-client.connector.ajax :as ajax]
    [ssoup-clj-client.modules :as sm]
    [clojure.string :as str])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(enable-console-print!)

; Key concepts: Templates, Containers, Widgets (viewers and askers)
; Templates define layout + a set of containers in the layout: widgets can be attached to containers

; -------------------------------------------------------
; UI Engine protocols
; -------------------------------------------------------

; defprotocol IUIEngine

; defprotocol IWithAuthHandler
(defmulti register-auth-services* #_[ui-engine]
          "Register all the services related to authentication" dispatch-on-type)
(defmulti ensure-auth* #_[ui-engine]
          "Ensure that a user is logged in to the system.
          It must return the current user or a channel that will receive the main user
          info once the user has authenticated."
          dispatch-on-type)

; defprotocol IWithContainers
(defmulti register-container* #_[ui-engine container-id]
          "Register a container with the engine" dispatch-on-type)
(defmulti get-container #_[ui-engine container-id]
          "Register the container with the given id" dispatch-on-type)

; defprotocol IWithTemplates
(defmulti register-template* #_[ui-engine ctx theme template-id template-body]
          "Register a template" dispatch-on-type)
(defmulti template-registered #_[ui-engine ctx template-id template-body]
          "Callback on template registration" dispatch-on-type)
(defmulti get-template #_[ui-engine template-id]
          "Return the template with the given id" dispatch-on-type)

; defprotocol IWithViewers
(defmulti register-viewer* #_[ui-engine ctx library-id viewer-name obj-type additional-classes
                              viewer]
          "Register a viewer" dispatch-on-type)
(defmulti register-viewer* #_[ui-engine ctx theme library-id viewer-name additional-classes obj-type
                              library-id viewer]
          "Register a viewer" dispatch-on-type)
(defmulti deregister-viewer* #_[ui-engine name]
          "Deregister a viewer" dispatch-on-type)
(defmulti get-viewer #_[ui-engine name]
          "Return the viewer with the given name" dispatch-on-type)

; defprotocol IWithAskers
(defmulti register-asker*
          #_[ui-engine ctx library-id asker-name additional-classes obj-type asker]
          #_[ui-engine ctx theme library-id asker-name additional-classes obj-type asker]
          "Register an asker" dispatch-on-type)

; defprotocol IWithAskerUIs
(defmulti register-asker-ui* #_[ui-engine name asker-ui]
          "Register an asker ui" dispatch-on-type)
(defmulti deregister-asker-ui* #_[ui-engine name]
          "Deregister an asker ui" dispatch-on-type)
(defmulti get-asker-ui #_[ui-engine name]
          "Return the asker ui with the given name" dispatch-on-type)

; defprotocol IWithThemes
(defmulti register-theme* #_[ui-engine ctx theme options]
          "Register a theme" dispatch-on-type)
(defmulti register-theme-viewers* #_[ui-engine ctx theme library-id viewers]
          "Register the theme viewers" dispatch-on-type)
(defmulti register-theme-editors* #_[ui-engine ctx theme library-id editors]
          "Register the theme editors" dispatch-on-type)
(defmulti register-theme-selectors* #_[ui-engine ctx theme library-id selectors]
          "Register the theme selectors" dispatch-on-type)
(defmulti register-theme-containers* #_[ui-engine ctx theme containers]
          "Register the theme containers" dispatch-on-type)
(defmulti register-theme-templates* #_[ui-engine ctx theme templates]
          "Register the theme templates" dispatch-on-type)

; defprotocol IUIRuntime
(defmulti call-viewer #_[ui-engine ui-session viewer ctx obj options]
          "Show a viewer ui" dispatch-on-type)
(defmulti new-ask-context-ui #_[ui-engine ui-session asker-ui asker-name
                                send-answer cancel-handler option]
          "Returns a ui for the ask context" dispatch-on-type)
(defmulti call-asker #_[ui-engine ui-session asker ctx name obj-type options]
          "Show an asker ui" dispatch-on-type)
(defmulti error-message #_[ui-engine ui-session]
          "Return an error message panel" dispatch-on-type)

; -------------------------------------------------------
; Session related protocols
; -------------------------------------------------------

(derive :ssoup/ui-session :ssoup/session)

; defprotocol IUISession

; defprotocol IWithTheme
(defmulti get-current-theme #_[ui-engine ui-session]
          "Returns the current ui theme" dispatch-on-type)
(defmulti set-current-theme #_[ui-engine ui-session]
          "Sets the current ui theme" dispatch-on-type)
(defmulti set-container-content #_[ui-engine ui-session container-id content]
          "Sets the content of the specified container" dispatch-on-type)

; -------------------------------------------------------
; IWithAuthHandler
; -------------------------------------------------------
(defmethod register-auth-services* :ssoup/ui-engine [ui-engine]
  (let [classes #{:ssoup/require-authentication}]
    (s/register-service* ui-engine
                         {:library-id :ssoup/base-ui-services
                          :fname      :ssoup/require-authentication
                          :handler    (fn [ui-engine ui-session ctx args options]
                                        (ensure-auth* ui-engine))
                          :flavor     :ssoup/default-theme
                          :user-role  :ssoup/any-user
                          :classes    classes
                          :arg-types  {}})))

; -------------------------------------------------------
; IWithTemplates
; -------------------------------------------------------
(defmethod register-template* :ssoup/ui-engine [ui-engine ctx theme template-id
                                                template-body]
  (let [template-definition {:id   template-id
                             :body template-body}
        ui-engine (assoc-in ui-engine [:templates template-id] template-definition)]
    (debug "Registering template " template-id " in theme " theme)
    (template-registered ui-engine ctx template-id template-body)
    ui-engine))

(defmethod template-registered :ssoup/ui-engine [ui-engine ctx template-id template-body])

(defmethod get-template :ssoup/ui-engine [ui-engine template-id]
  (get-in ui-engine [:templates template-id]))

; -------------------------------------------------------
; IWithViewers
; -------------------------------------------------------
(defmethod register-viewer* :ssoup/ui-engine
  ([ui-engine ctx library-id viewer-name obj-type additional-classes viewer]
    (register-viewer* ui-engine ctx :ssoup/default-theme library-id viewer-name additional-classes
                      obj-type viewer))
  ([ui-engine ctx theme library-id viewer-name additional-classes obj-type viewer]
    (let [classes #{:ssoup/show}
          classes (if additional-classes
                    (clojure.set/union classes additional-classes)
                    classes)
          fndef {:library-id library-id
                 :fname      viewer-name
                 :flavor     theme
                 :user-role  :ssoup/any-user
                 :classes    classes
                 :arg-types  {:ssoup/object obj-type}}
          ctx (assoc ctx :fndef fndef)]
      (s/register-service* ui-engine
                           (assoc fndef
                             :handler (fn [ui-engine ui-session ctx args options]
                                        (call-viewer ui-engine ui-session viewer ctx
                                                     (:ssoup/object args) options)))))))

(defmethod deregister-viewer* :ssoup/ui-engine [ui-engine viewer-name])

(defmethod get-viewer :ssoup/ui-engine [ui-engine name]
  (get-in ui-engine [:viewers name]))

; -------------------------------------------------------
; IWithEditors
; -------------------------------------------------------
#_(defmethod register-editor* :ssoup/ui-engine
    ([ui-engine ctx editor-name additional-classes obj-type editor]
      (register-editor* ui-engine ctx :ssoup/default-theme editor-name additional-classes
                        obj-type editor))
    ([ui-engine ctx theme editor-name additional-classes obj-type editor]
      (let [classes #{:ssoup/edit}
            classes (if additional-classes
                      (clojure.set/union classes additional-classes)
                      classes)]
        (s/register-service* ui-engine
                             {:fname     editor-name
                              :handler   (fn [ui-engine ui-session ctx args options]
                                           (call-editor ui-engine ui-session editor ctx
                                                        (:ssoup/object args) options))
                              :flavor    theme
                              :user-role :ssoup/any-user
                              :classes   classes
                              :arg-types {:ssoup/object obj-type}}))))

#_(defmethod deregister-editor* :ssoup/ui-engine [ui-engine editor-name])

#_(defmethod get-editor :ssoup/ui-engine [ui-engine name]
    (get-in ui-engine [:editors name]))

; -------------------------------------------------------
; IWithAskers
; -------------------------------------------------------
(defprotocol IAsker
  (get-value [this])
  (send-value [this]))

(defrecord WrappedAsker
  [asker-ui out-channel state options]
  IAsker
  (get-value [_] @state)
  (send-value [_]
    (put! out-channel @state)))

(defn wrap-asker [asker ctx obj options]
  (let [state (atom obj)
        on-change (:on-change options)
        on-change-handler (fn [value]
                            (reset! state value)
                            (trace "on-change value: " value)
                            ; propagate the on-change event
                            (on-change value))
        new-options (merge options {:on-change on-change-handler})
        asker-ui (asker ctx state new-options)]
    (->WrappedAsker asker-ui (chan) state new-options)))

(defn register-asker*
  ([ui-engine ctx library-id asker-name additional-classes obj-type asker]
   (register-asker* ui-engine ctx :ssoup/default-theme library-id asker-name additional-classes
                    obj-type asker))
  ([ui-engine ctx theme library-id asker-name additional-classes obj-type asker]
   (register-asker* ui-engine ctx theme library-id asker-name additional-classes
                    obj-type asker {}))
  ([ui-engine ctx theme library-id asker-name additional-classes obj-type asker additional-asker-options]
   (let [classes (clojure.set/union additional-classes #{:ssoup/ask})]
     (go
       (let [fndef {:library-id library-id
                    :fname      asker-name
                    :flavor     theme
                    :user-role  :ssoup/any-user
                    :classes    classes
                    :arg-types  {:ssoup/object obj-type}}
             ctx (assoc ctx :fndef fndef)
             ui-engine (<! (s/register-service* ui-engine
                                                (assoc fndef
                                                  :handler (fn [ui-engine ui-session ctx args options]
                                                             (call-asker ui-engine ui-session asker
                                                                         ctx
                                                                         (str (uuid/make-random))
                                                                         (:ssoup/object args)
                                                                         (merge options
                                                                                additional-asker-options))))))]
         ui-engine)))))

(defn register-editor* [ui-engine ctx theme library-id editor-name additional-classes obj-type editor]
  (go
    (let [; service witb class :ssoup/ask-with-editor will use the editor ui but will not call save-object
          ui-engine (<! (register-asker* ui-engine ctx theme library-id editor-name
                                         (clojure.set/union additional-classes #{:ssoup/ask-with-editor})
                                         obj-type editor))
          ; service with class :ssoup/edit will call save-object
          ui-engine (<! (register-asker* ui-engine ctx theme library-id editor-name
                                         (clojure.set/union additional-classes #{:ssoup/edit})
                                         obj-type editor
                                         {:on-answer (fn [ctx value]
                                                       (go
                                                         (let [{rdf-ctx       :rdf-ctx
                                                                ui-engine     :engine
                                                                ui-session    :session
                                                                asker-options :asker-options} ctx
                                                               value (if (atomic? ui-engine obj-type)
                                                                       value
                                                                       (let [rdf-ctx (merge rdf-ctx (:rdf/context value))]
                                                                         (assoc value :rdf/context rdf-ctx)))]
                                                           (try
                                                             (if-err-throw (<! (s/exec-async ui-engine ui-session
                                                                                             {:ssoup/classes #{:ssoup/save-object}
                                                                                              :ssoup/ctx     ctx
                                                                                              :ssoup/args    {:ssoup/data-tags (:ssoup/data-tags asker-options)
                                                                                                              :ssoup/object    value}
                                                                                              :ssoup/options {}})))
                                                             (catch js/Error e
                                                               (error ui-engine ui-session "An error occurred saving object" value e))))))}))]
      ui-engine)))

(defn register-selector* [ui-engine ctx theme library-id selector-name additional-classes obj-type
                          selector]
  (register-asker* ui-engine ctx theme library-id selector-name
                   (clojure.set/union additional-classes #{:ssoup/ask-with-selector})
                   obj-type selector))

; -------------------------------------------------------
; IWithAskerUIs
; -------------------------------------------------------
(defmethod register-asker-ui* :ssoup/ui-engine [ui-engine name asker-ui]
  (retval (assoc-in ui-engine [:askers-ui name] asker-ui)
          nil
          asker-ui))

(defmethod deregister-asker-ui* :ssoup/ui-engine [ui-engine name]
  (assoc-in ui-engine [:askers-ui name] nil))

(defmethod get-asker-ui :ssoup/ui-engine [ui-engine name]
  (get-in ui-engine [:askers-ui name]))

; -------------------------------------------------------
; IWithThemes
; -------------------------------------------------------
(defmethod register-theme* :ssoup/ui-engine [ui-engine ui-session ctx theme options]
  (go (let [library-id (:library-id options)
            viewers (:viewers options)
            editors (:editors options)
            selectors (:selectors options)
            containers (:containers options)
            templates (:templates options)
            ui-engine (assoc-in ui-engine [:themes theme] options)
            ui-engine (<! (register-theme-viewers* ui-engine ctx theme library-id viewers))
            ui-engine (<! (register-theme-editors* ui-engine ctx theme library-id editors))
            ui-engine (<! (register-theme-selectors* ui-engine ctx theme library-id selectors))
            ui-engine (register-theme-containers* ui-engine ctx theme containers)
            ui-engine (register-theme-templates* ui-engine ctx theme templates)]
        (retval
          ui-engine
          ui-session
          true))))

(defmethod register-theme-viewers* :ssoup/ui-engine [ui-engine ctx theme library-id viewers]
  (go (loop [ui-engine ui-engine
             viewers viewers]
        (if (not-empty viewers)
          (recur (<! (apply register-viewer* ui-engine ctx theme library-id (first viewers)))
                 (rest viewers))
          ui-engine))))

(defmethod register-theme-editors* :ssoup/ui-engine [ui-engine ctx theme library-id editors]
  (go (loop [ui-engine ui-engine
             editors editors]
        (if (not-empty editors)
          (recur (<! (apply register-editor* ui-engine ctx theme library-id (first editors)))
                 (rest editors))
          ui-engine))))

(defmethod register-theme-selectors* :ssoup/ui-engine [ui-engine ctx theme library-id selectors]
  (go (loop [ui-engine ui-engine
             selectors selectors]
        (if (not-empty selectors)
          (recur (<! (apply register-selector* ui-engine ctx theme library-id (first selectors)))
                 (rest selectors))
          ui-engine))))

(defmethod register-theme-containers* :ssoup/ui-engine [ui-engine ctx theme containers]
  (reduce (fn [ui-engine container]
            (apply register-container* ui-engine container))
          ui-engine containers))

(defmethod register-theme-templates* :ssoup/ui-engine [ui-engine ctx theme templates]
  (reduce (fn [ui-engine template]
            (apply register-template* ui-engine ctx theme template))
          ui-engine templates))

; -------------------------------------------------------
; IUIRuntime
; -------------------------------------------------------

(defn run-in-container
  [ui-engine ui-session ctx class additional-classes args options]
  (let [container-id (keyword (str (uuid/make-random)))
        ; NOTE: this random container uuid will be lost after execution
        ; as the engine new state after registration of the container is not kept
        ui-engine (register-container* ui-engine container-id)]
    (go (let [classes #{class}
              classes (if additional-classes
                        (clojure.set/union classes additional-classes)
                        classes)
              ctx (assoc ctx :ssoup/container-id container-id)]
          (try (if-err-throw (<! (s/exec-async ui-engine ui-session
                                               {:ssoup/classes classes
                                                :ssoup/ctx     ctx
                                                :ssoup/args    args
                                                :ssoup/options (merge options
                                                                      {:ssoup/container container-id})})))
               (catch js/Error e
                 (set-container-content ui-engine container-id
                                        (error-message ui-engine ui-session
                                                       "An error occurred." e))))))
    (get-container ui-engine container-id)))

(defmethod call-asker :ssoup/ui-engine [ui-engine ui-session asker ctx name
                                        obj options]
  (go
    (let [ctx (assoc ctx :engine ui-engine :session ui-session)
          fndef (:fndef ctx)
          service-arg-types (:arg-types fndef)
          service-arg-type (:ssoup/object service-arg-types)
          on-change (:on-change options)
          ; ask-ctx (or (:ask-ctx ctx) (ssoup/new-ask-context (ssoup/get-engine) {:name "ask-context-default"}))
          ask-ctx-id (:ssoup/ask-ctx-id options)
          ask-ctx (or (:ssoup/ask-ctx ctx)
                      (when ask-ctx-id
                        (s/get-ask-context ui-engine ask-ctx-id)))
          create-new-ask-ctx (or (nil? ask-ctx)
                                 (:create-new-ask-context options))
          {ui-engine :engine
           ask-ctx   :return-value}
          (if create-new-ask-ctx
            (s/new-ask-context ui-engine ui-session options)
            (retval ui-engine ui-session ask-ctx))
          ctx (assoc ctx :ssoup/ask-ctx ask-ctx)
          existing-asker-ui (get-asker-ui ui-engine name)
          reuse-existing (and
                           (:ssoup/reuse-existing options)
                           (not (nil? existing-asker-ui)))
          wrapped-on-change (fn [value]
                              (when on-change
                                (apply on-change [value]))
                              (when (:ssoup/immediate options)
                                (apply (:send-answer ask-ctx) [])))
          on-key-up (fn [key]
                      (when (and (:ssoup/container options) (not (:ssoup/immediate options)))
                        (when (= key 13)                    ; carriage return
                          (apply (:send-answer ask-ctx) []))))
          obj-types (get-type ui-engine obj)
          obj-types (as-set obj-types)
          obj (unbox obj)
          {ui-engine     :engine
           wrapped-asker :return-value}
          (if reuse-existing
            ui-engine
            (let [schema (<! (s/get-types-schema ui-engine ui-session ctx
                                          #{:ssoup/schema} obj-types
                                          {}))
                  item-type (:ssoup/item-type options)
                  {item-schema :ssoup/schema}
                  (when item-type
                    (gretval ui-engine
                             (<! (s/get-type-schema ui-engine ui-session ctx
                                                    #{:ssoup/schema}
                                                    item-type
                                                    {}))))
                  wrapped-asker (wrap-asker asker
                                            ctx
                                            obj
                                            (merge options
                                                   {:on-change         wrapped-on-change
                                                    :on-key-up         on-key-up
                                                    :ssoup/schema      schema
                                                    :ssoup/item-schema item-schema}))]
              (register-asker-ui* ui-engine name wrapped-asker)))
          on-send-answer-handler (fn []
                                   (when (not (nil? (get-value wrapped-asker)))
                                     (when (:on-answer options)
                                       (let [ctx {:rdf-ctx       (get-in wrapped-asker [:options
                                                                                        :rdf/context])
                                                  :engine        ui-engine
                                                  :session       ui-session
                                                  :asker-options options}
                                             value (get-value wrapped-asker)]
                                         (apply (:on-answer options)
                                                [ctx value])))
                                     (send-value wrapped-asker)))
          on-cancel-handler (fn []
                              (when (:on-cancel options)
                                (apply (:on-cancel options) [])))]
      (apply (:ask ask-ctx) [ui-engine ui-session
                             (:asker-ui wrapped-asker) ctx name reuse-existing
                             create-new-ask-ctx (:ssoup/container options)
                             on-send-answer-handler on-cancel-handler])
      (retval ui-engine ui-session (<! (:out-channel wrapped-asker))))))

; -------------------------------------------------------
; Show and edit facades
; -------------------------------------------------------

(defn show
  ([ctx obj]
   (show ctx nil obj))
  ([ctx additional-classes obj]
   (show ctx additional-classes obj {}))
  ([ctx additional-classes obj options]
   (when obj
     (run-in-container (:engine ctx) (:session ctx) ctx
                       :ssoup/show
                       additional-classes
                       {:ssoup/object obj}
                       options))))

(defn ask
  "Ask means just ask for information to the user, but don't save back to the data source."
  ([ctx obj]
   (ask ctx (get-type (:engine ctx) obj) obj))
  ([ctx obj-type obj]
   (ask ctx obj-type obj nil {}))
  ([ctx obj-type obj additional-classes options]
   (let [obj (box obj obj-type)]
     (run-in-container (:engine ctx) (:session ctx) ctx
                       :ssoup/ask
                       additional-classes
                       {:ssoup/object obj}
                       options))))

(defn edit
  "Edit means ask and save back to the data source."
  ([ctx obj-type obj options]
   (let [ui-engine (:engine ctx)
         ui-session (:session ctx)]
     (ask ctx obj-type obj
          #{:ssoup/edit}
          options))))

; -------------------------------------------------------
; IWithTheme
; -------------------------------------------------------
(defmethod get-current-theme :ssoup/ui-engine [ui-engine ui-session]
  (s/get-current-flavor ui-engine ui-session))

(defmethod set-current-theme :ssoup/ui-engine [ui-engine ui-session theme]
  (s/set-current-flavor ui-engine ui-session theme))

; -------------------------------------------------------
; IWithAskContexts
; -------------------------------------------------------
(defmethod s/new-ask-context :ssoup/ui-engine [ui-engine ui-session options]
  (let [ask-ctx-id (or (:ssoup/ask-ctx-id options)
                       (uuid/make-random-str))
        send-answer-observers (atom {})
        send-answer (fn []
                      (doseq [[name observer] @send-answer-observers]
                        (observer)))
        cancel-observers (atom {})
        cancel-handler (fn []
                         (doseq [[name observer] @cancel-observers]
                           (observer)))]
    (debug "Creating ask context " ask-ctx-id)
    (let [ask-ctx {:id          ask-ctx-id
                   :ask         (fn [ui-engine ui-session
                                     asker-ui ctx asker-name
                                     reuse-existing
                                     create-ask-context-ui
                                     container-id
                                     on-send-answer
                                     on-cancel]
                                  (swap! send-answer-observers assoc asker-name
                                         on-send-answer)
                                  (swap! cancel-observers assoc asker-name
                                         on-cancel)
                                  (when-not reuse-existing
                                    (if create-ask-context-ui
                                      (let [ask-ctx-ui (new-ask-context-ui ui-engine
                                                                           ui-session
                                                                           asker-ui
                                                                           asker-name
                                                                           send-answer
                                                                           cancel-handler
                                                                           (assoc
                                                                             options
                                                                             :ssoup/ask-ctx-id
                                                                             ask-ctx-id))]
                                        (let [container-id (:ssoup/container options)]
                                          (set-container-content ui-engine ui-session
                                                                 container-id
                                                                 ask-ctx-ui)))
                                      (set-container-content ui-engine ui-session
                                                             container-id
                                                             asker-ui))))
                   :send-answer send-answer}]
      (retval
        (assoc-in ui-engine [:ask-ctxs ask-ctx-id] ask-ctx)
        ui-session
        ask-ctx))))

(defmethod s/get-ask-context :ssoup/ui-engine [ui-engine ui-session ask-ctx-id]
  (get-in ui-engine [:ask-ctxs ask-ctx-id]))

(defmethod s/delete-ask-context :ssoup/ui-engine [ui-engine ui-session ask-ctx])

; -------------------------------------------------------
; IWithSessions
; -------------------------------------------------------

(defn new-ui-session-fn* [ui-engine additional-attributes options]
  (s/new-session-fn* ui-engine (merge {:type  :ssoup/ui-session
                                       :theme nil}
                                      additional-attributes)
                     options))

(defmethod s/new-session* :ssoup/ui-engine [engine additional-attributes options]
  (new-ui-session-fn* engine additional-attributes options))

; -------------------------------------------------------
; User interface initialization
; -------------------------------------------------------

; show user specific landing element once the user is logged in
(defn user-logged-in [ui-session logged-in-user]
  (s/set-current-user* ui-session logged-in-user))

(defn user-logged-out [ui-session]
  (s/set-current-user* ui-session nil))

(defn get-server-engine-connector [engine]
  (let [remote-engine (s/get-remote-engine-by-id engine :ssoup/server-engine)
        remote-engine-connector (:connector-client remote-engine)]
    remote-engine-connector))
