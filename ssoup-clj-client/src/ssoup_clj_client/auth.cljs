(ns ^:figwheel-always ssoup-clj-client.auth
  (:require [cljs.core.async :as async :refer [<! chan close! >!]]
            [cljs-http.client :as http]
            [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]
            [ssoup-clj-core.core :as s]
            [ssoup-clj-core.connector :as connector]
            [ssoup-clj-client.core :as sc]
            [ssoup-clj-client.i18n :refer [msg]])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defn get-current-user [engine]
  (go (let [server-engine-connector (sc/get-server-engine-connector engine)
            ctx {:engine engine :session nil}
            resp (<! (connector/get-current-user server-engine-connector ctx {}))]
        (if (= (:status resp) 200)
          (let [user (:body resp)]
            user)
          nil))))

(defn login! [engine user pass success error]
  (cond
    (empty? user)
    (reset! error (msg :admin-required))
    (empty? pass)
    (reset! error (msg :admin-pass-required))
    :else
    (go (let [server-engine-connector (sc/get-server-engine-connector engine)
              ctx {:engine engine :session nil}
              resp (<! (connector/login server-engine-connector ctx user pass (:options engine)))]
          (if (= (:status resp) 200)
            (let [user (:body resp)]
              (success user))
            ((reset! error (:error resp))))))))

(defn logout! [engine success fail]
  (go (let [server-engine-connector (sc/get-server-engine-connector engine)
            ctx {:engine engine :session nil}
            resp (<! (connector/logout server-engine-connector ctx (:options engine)))]
        (if (= (:status resp) 200)
          (success)
          (fail (:error resp))))))
