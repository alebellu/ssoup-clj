(ns ^:figwheel-always ssoup-clj-client.utils
  (:require [cljs.core.async :as async :refer [<! chan close! >!]]
            [cljs-http.client :as http]
            [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defn protocol []
  (str js/window.location.protocol))

(defn host []
  (str js/window.location.host))

(defn pathname []
  (str js/window.location.pathname))

(defn path-parts []
  (clojure.string/split (pathname) "/"))
