(ns ^:figwheel-always ssoup-clj-client.i18n
  (:require [cljs.core.async :as async :refer [<! chan close! >!]]
            [cljs-http.client :as http]
            [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)])
  (:require-macros [cljs.core.async.macros :refer [go]]))

; this should use localization message map
(defn msg [key] (name key))
