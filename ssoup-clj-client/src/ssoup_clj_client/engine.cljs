(ns ^:figwheel-always ssoup-clj-client.engine
  (:require [clojure.core.async :as async :refer [<! chan >! put! close! pipeline]]
            [ssoup-clj-core.engine :as se]
            [ssoup-clj-core.utils :refer [retval]]
            [ssoup-clj-client.base-ui-services]
            [ssoup-clj-client.connector.ajax :as ajax]
            [ssoup-clj-client.connector.websocket :as websocket]
            [ssoup-clj-client.auth :as auth]
            [ssoup-clj-core.core :as s]
            [ssoup-clj-core.types :refer [derive*]]
            [ssoup-clj-client.core :as sc]
            [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]
            [ssoup-clj-core.connector :as connector])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(derive :ssoup/ui-engine :ssoup/engine)

(defn- init-ui-type-system [ui-engine]
  (let [user-type (get-in ui-engine [:options :ssoup/user-type])]
    (-> ui-engine
        (derive* :ssoup/default-theme :ssoup/default-flavor)
        (derive* :ssoup/editor :ssoup/asker)
        (derive* :ssoup/selector :ssoup/asker)
        #_(derive* user-type :rdfs/Resource))))

(defn init-engine-fn* [ui-engine]
  (go (let [libraries (or (:ssoup/libraries ui-engine) #{})
            libraries (conj libraries :ssoup/base-ui-services)
            ; all ui engines should expose the ui base library
            ui-engine (assoc ui-engine :ssoup/libraries libraries)
            ui-engine (<! (se/init-engine-fn* ui-engine))
            ui-engine (init-ui-type-system ui-engine)
            ui-engine (<! (sc/register-auth-services* ui-engine))
            ; create the main session used by the ui engine, with id :main-session
            {ui-session :session
             ui-engine :engine} (s/new-session* ui-engine {} {:session-id :main-session})
            user (<! (auth/get-current-user ui-engine))
            ; reflect the remote current user on the client local current user
            {ui-engine :engine}
            (if user (s/set-current-user* ui-engine ui-session user)
                     (retval ui-engine ui-session nil))
            ctx {:engine ui-engine}]
        (loop [remote-engines (:remote-engines ui-engine)]
          (when-not (empty? remote-engines)
            (let [remote-engine (first remote-engines)
                  connector-client (:connector-client remote-engine)]
              (<! (connector/init connector-client ctx))
              (recur (rest remote-engines)))))
        ui-engine)))

(defmethod s/init-engine* :ssoup/ui-engine [ui-engine]
  (init-engine-fn* ui-engine))

; TODO: choose ajax-connector or websocket-connector depending on a flag passed in via options argument
(defn new-ui-engine [name default-context options]
  (let [base-engine (se/new-engine name default-context options)
        server-exec-url (:server-exec-url options)
        ajax-connector (ajax/new-ajax-connector-client server-exec-url)
        websocket-url (:websocket-url options)
        websocket-connector (websocket/new-websocket-cljs-connector-client
                              {:websocket-url websocket-url})]
    (-> base-engine
        (assoc :themes {}
               :templates {}
               :ask-ctxs {}
               :askers-ui {})
        ; register remote ssoup engine
        (s/register-remote-engine* {:id :ssoup/server-engine
                                    :connector-client websocket-connector}))))
